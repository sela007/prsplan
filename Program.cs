﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace PRSPLAN
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] command)
        {

          
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (command.Count() == 1)
            {
                Form1 frm = new Form1(command[0]);

               // MessageBox.Show(command[0]);
                Application.Run(frm );
                 

            }
            else
            {
                Application.Run(new Form1());
            }

        }
    }
}
