﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;

using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PRSPLAN
{
    public partial class cSlike : UserControl
    {
        cDocument doc;
        public cSlike()
        {
            InitializeComponent();
        }
        public cSlike(cDocument d)
        {
            InitializeComponent();
            doc = d;

        }
        OpenFileDialog fo = new OpenFileDialog();
        private void cSlike_Load(object sender, EventArgs e)
        {
            refreshSlike();
            fo.Filter = "JPG|*.jpg";
            if (lstSlike.Items.Count > 0) lstSlike.SelectedIndex = 0;
        }
        private void refreshSlike()
        {
            if (doc == null) return;
            int i = 0;
            lstSlike.Items.Clear();
            if (System.IO.Directory.Exists(doc.folder + @"\slike") != true) return;
            if (doc.filePath == "") return;
            string[] files = System.IO.Directory.GetFiles(doc.folder + @"\slike","*.jpg");
            foreach (string f in files)
            {
                i++;
                lstSlike.Items.Add(System.IO.Path.GetFileName(f));
            }

            //foreach (Bitmap b in doc.slike)
            //{
            //    i++;
            //    lstSlike.Items.Add("Slika " + i.ToString());
            //}
        }

        private void lstSlike_SelectedIndexChanged(object sender, EventArgs e)
        {
            //string fpath = System.IO.Path.GetDirectoryName(doc.filePath) + @"\slike\" + lstSlike.Items[lstSlike.SelectedIndex];

            if (lstSlike.Items.Count == 0) return;

            if (lstSlike.SelectedItem == null) return;

            //slika.BackgroundImage = doc.slike[lstSlike.SelectedIndex];
            slika.BackgroundImage = Image.FromFile(doc.folder + @"\slike\" + lstSlike.SelectedItem.ToString());
           // slika.BackgroundImage = new Bitmap(doc.folder + @"\slike\" + lstSlike.SelectedItem.ToString ());
             
        }


        private void btnDodaj_Click(object sender, EventArgs e)
        {
            fo.Multiselect = true;
            if (doc.filePath == "") { MessageBox.Show("Prvo spremi dokument"); return; }
            if (fo.ShowDialog() == DialogResult.Cancel) return;
            

            if (System.IO.Directory.Exists(doc.folder + @"\" + "slike") != true)
            {
                System.IO.Directory.CreateDirectory(doc.folder + @"\" + "slike");
            }
            foreach (string f in fo.FileNames)
            {
                 
            string fn=System.IO.Path.Combine ( doc.folder ,"slike" ,System.IO.Path.GetFileNameWithoutExtension(f));
            Bitmap b = new Bitmap(f);

            Bitmap newBitmap = new Bitmap(b.Width / 3, b.Height / 3);
            using (Graphics gr = Graphics.FromImage(newBitmap ))
            {
                gr.SmoothingMode = SmoothingMode.HighQuality;
                gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gr.PixelOffsetMode = PixelOffsetMode.HighQuality;
                gr.DrawImage( b, new Rectangle(0, 0, newBitmap.Width  ,newBitmap.Height  ));
            }

            //System.IO.File.Copy(f, fn);
           newBitmap.Save(fn + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg );
           newBitmap.Dispose();
           b.Dispose();
                //doc.slike.Add(b);

            }
            refreshSlike();
            lstSlike.SelectedIndex = lstSlike.Items.Count - 1;

        }

        private void btnBrisi_Click(object sender, EventArgs e)
        {
            if (lstSlike.SelectedIndex == -1) return;
            
            //doc.slike.RemoveAt(lstSlike.SelectedIndex);
            //lstSlike.Items.RemoveAt(lstSlike.SelectedIndex);
            //slika.BackgroundImage = null; 
            try
            {
                slika.BackgroundImage.Dispose ();
                slika.BackgroundImage = null;
                GC.Collect();
                System.IO.File.Delete(System.IO.Path.Combine(doc.folder + @"\slike\", lstSlike.SelectedItem.ToString()));
            }
           catch (Exception ex) { MessageBox.Show(ex.Message); }
            refreshSlike();

        }
    }
}
