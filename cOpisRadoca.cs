﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PRSPLAN
{
    public partial class cOpisRadoca : UserControl
    {
        public cOpisRadoca()
        {
            InitializeComponent();
        }

        private void cOpisRadoca_Load(object sender, EventArgs e)
        {
           // ucitajCvs(dgMaterijal, "mat.txt");
           // ucitajCvs(dgDemMaterijal, "mat.txt");
            dg1.Columns[0].Width = 70;
            dg1.Columns[1].Width = this.Width / 2;

        }

        public void ucitajCvs(string cvsFile)
        {
            dg1.Rows.Clear();
            string[] lines = System.IO.File.ReadAllLines(cvsFile , Encoding.Default);
            foreach (string line in lines)
            {
                if (line == "") break;
                string[] cols = line.Split(new char[] { '|' });
                int i = dg1.Rows.Add();
                dg1.Rows[i].Cells[0].Value = cols[0];
                dg1.Rows[i].Cells[1].Value = cols[1];
                dg1.Rows[i].Cells[2].Value = cols[2];
                dg1.Rows[i].Cells[3].Value = "";
                dg1.Rows[i].Cells[4].Value = "Dodaj";
                dg1.Rows[i].Cells[5].Value = "Briši";
            }
        }

        private void txtFazeRada_KeyDown(object sender, KeyEventArgs e)
        {

           
        }

        private void txtFazeRada_KeyUp(object sender, KeyEventArgs e)
        {

            TextBox tb =(TextBox )sender;
            if (e.KeyCode == Keys.Enter)
            {
               tb.Text +=  NextLine (tb.Text )+". " ;
               tb.SelectionStart = tb.Text.Length  ;
            }

        }

        private int LinesCount(string text) {
            string[] str = text.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None);
            return str.Count();
        }

        private int NextLine(string text)
        {
            string[] str = text.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None);
            int num=0;
            int lastNum=0;

            foreach (string s in str)
            {
                int y = 0;
                string str1 = s;
                y = str1.IndexOf(".");
                if (y > 0)
                {
                    string str2 = str1.Substring(0, y);
                    if (Int32.TryParse(str2, out num))
                    {
                        lastNum = num;
                    }
                }
            }


            return lastNum+1;

        }

        private void txtFazeRada_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtFazeRada_Click(object sender, EventArgs e)
        {
            TextBox tb =(TextBox ) sender;
            if (tb.Text == "") { tb.Text = "1. "; tb.SelectionStart = tb.Text.Length ; }

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgFazeRada_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void dgFazeRada_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex == -1) return;
            ((DataGridView)sender).Rows[e.RowIndex].Selected = true;
            DataGridView dg = (DataGridView)sender;
            if (e.ColumnIndex == 4)
            {
                if (dg.SelectedRows[0].Cells[3].Value == null) { dg.SelectedRows[0].Cells[3].Value = "1"; return; }
                if (dg.SelectedRows[0].Cells[3].Value.ToString() == "")
                { dg.SelectedRows[0].Cells[3].Value = "1"; return; }

                int i = -1;
                int.TryParse(dg.SelectedRows[0].Cells[3].Value.ToString(), out i);
                i++;
                dg.SelectedRows[0].Cells[3].Value = i;
            }
            if (e.ColumnIndex == 5)
            {
                if (dg.SelectedRows[0].Cells[3].Value == null) { dg.SelectedRows[0].Cells[3].Value = ""; return; }
                if (dg.SelectedRows[0].Cells[3].Value.ToString() == "")
                { return; }

                int i = -1;
                int.TryParse(dg.SelectedRows[0].Cells[3].Value.ToString(), out i);
                i--;
                if (i == 0)
                {
                    dg.SelectedRows[0].Cells[3].Value = "";
                }
                else
                {
                    dg.SelectedRows[0].Cells[3].Value = i;
                }
            }

        }

        private void cOpisRadoca_Resize(object sender, EventArgs e)
        {
            dg1.Columns[0].Width = 70;
            dg1.Columns[1].Width = this.Width / 2;

        }

        
    }
}
