﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PRSPLAN
{
    class cTask
    {
        public enum Action { AddElement, AddPoint, DeleteElement , DeletePoint, Edit }
        public Action action;
        public object element;
        public cTask(object ielement, Action iaction) 
        {
            action = iaction;
            element = ielement;
        }
    }
}
