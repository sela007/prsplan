﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
namespace PRSPLAN
{
    [Serializable]
    public class cZaglavlje
    {
        public List<cZaglavljeItem> items = new List<cZaglavljeItem>();
        public int width=250;
        public int rowHeight=20;
        //[NonSerialized]
        public Color color=Color.FromArgb(100,100,100)  ;
        //[NonSerialized ]
        public Font textFont = new Font("MS SERIF", 10);
        //[NonSerialized ]
        public Font titleFont= new Font("MS SERIF", 7 );
        public void AddRow(string title) 
        {
            cZaglavljeItem czi= new cZaglavljeItem();
            czi.title = title;
            items.Add(czi);
        }

        public cZaglavlje() 
        {
             
        }

         
        public void drawon(Bitmap b,Graphics g)
        {
          //using(  Graphics g = Graphics.FromImage(b))
          //{
            int i = 0;
            foreach (cZaglavljeItem s in items)
            {
                int zh = rowHeight * items.Count;
                int zt = b.Height - zh;


                Rectangle rec = new Rectangle(b.Width - width, zt + (rowHeight * i), width, b.Height - rowHeight);
                Rectangle okvir = new Rectangle(0, 0, b.Width - 1, b.Height - 1);


                Pen p = new Pen(new SolidBrush(color), 2);

                p.Width = 1;
                g.DrawRectangle(p, rec);
                g.DrawRectangle(p, okvir);

                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;

                g.DrawString(s.title, titleFont, new SolidBrush(color), rec);
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit ;

                g.DrawString(s.text, textFont, new SolidBrush(color), new PointF(rec.X + 50, zt + (rowHeight * i+5)));
                i++;
            }
          //}


        }

       
    }
    [Serializable]
    public class cZaglavljeItem
    {
       public string text;
       public string title;
    }
}
