﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PRSPLAN
{
    static  class Contacts
    {
        public static  List<cPerson> Persons=new List<cPerson >();
        public static void Add(string name, string mail)
        {
            cPerson p = new cPerson();
            p.Name = name;
            p.Mail = mail;
            Persons.Add(p);
        }

        private static string fPath =System.Windows.Forms.Application.StartupPath +  @"\contacts.csv";
        public static void Save()
        {
            System.IO.StreamWriter sw = new System.IO.StreamWriter(fPath);
            foreach (cPerson P in Persons) {
                sw.WriteLine(P.Name + "," + P.Mail);
                
            }
            sw.Close();
            sw.Dispose();

        }
        public static void Load()
        {
             Persons.Clear();

             if (System.IO.File.Exists(fPath))
             {
                 string[] lines = System.IO.File.ReadAllLines(fPath);
                 foreach (string line in lines)
                 {
                     //  string[] fields = line.Split(new char[] { "," });
                     string[] fields = line.Split(new Char[] { ',' });

                     if (fields.Count() > 1)
                     {
                         Add(fields[0], fields[1]);
                     }

                 }
             }

        }
    }

    class cPerson 
    
    {
       public  string Name;
       public  string Mail;
       
       

    }


}
