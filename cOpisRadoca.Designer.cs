﻿namespace PRSPLAN
{
    partial class cOpisRadoca
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtFazeRada = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dg1 = new System.Windows.Forms.DataGridView();
            this.Sifra = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Opis = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Jedinica = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Kom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Dodaj = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Oduzmi = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dg1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtFazeRada
            // 
            this.txtFazeRada.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFazeRada.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtFazeRada.Location = new System.Drawing.Point(3, 21);
            this.txtFazeRada.Multiline = true;
            this.txtFazeRada.Name = "txtFazeRada";
            this.txtFazeRada.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtFazeRada.Size = new System.Drawing.Size(480, 432);
            this.txtFazeRada.TabIndex = 0;
            this.txtFazeRada.Tag = "FAZERADA";
            this.txtFazeRada.Click += new System.EventHandler(this.txtFazeRada_Click);
            this.txtFazeRada.TextChanged += new System.EventHandler(this.txtFazeRada_TextChanged);
            this.txtFazeRada.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFazeRada_KeyDown);
            this.txtFazeRada.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtFazeRada_KeyUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.Color.DimGray;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Faze rada";
            // 
            // dg1
            // 
            this.dg1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dg1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Sifra,
            this.Opis,
            this.Jedinica,
            this.Kom,
            this.Dodaj,
            this.Oduzmi});
            this.dg1.GridColor = System.Drawing.Color.Silver;
            this.dg1.Location = new System.Drawing.Point(3, 21);
            this.dg1.MultiSelect = false;
            this.dg1.Name = "dg1";
            this.dg1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg1.Size = new System.Drawing.Size(480, 430);
            this.dg1.TabIndex = 3;
            this.dg1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgFazeRada_CellClick);
            this.dg1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dg1.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgFazeRada_CellMouseDown);
            // 
            // Sifra
            // 
            this.Sifra.HeaderText = "Sifra";
            this.Sifra.Name = "Sifra";
            // 
            // Opis
            // 
            this.Opis.HeaderText = "Opis";
            this.Opis.Name = "Opis";
            // 
            // Jedinica
            // 
            this.Jedinica.HeaderText = "Jedinica";
            this.Jedinica.Name = "Jedinica";
            // 
            // Kom
            // 
            this.Kom.HeaderText = "Kom";
            this.Kom.Name = "Kom";
            // 
            // Dodaj
            // 
            this.Dodaj.HeaderText = "Dodaj";
            this.Dodaj.Name = "Dodaj";
            this.Dodaj.Text = "Dodaj";
            // 
            // Oduzmi
            // 
            this.Oduzmi.HeaderText = "Oduzmi";
            this.Oduzmi.Name = "Oduzmi";
            this.Oduzmi.Text = "Oduzmi";
            // 
            // cOpisRadoca
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.dg1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtFazeRada);
            this.Name = "cOpisRadoca";
            this.Size = new System.Drawing.Size(486, 457);
            this.Load += new System.EventHandler(this.cOpisRadoca_Load);
            this.Resize += new System.EventHandler(this.cOpisRadoca_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.dg1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox txtFazeRada;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sifra;
        private System.Windows.Forms.DataGridViewTextBoxColumn Opis;
        private System.Windows.Forms.DataGridViewTextBoxColumn Jedinica;
        private System.Windows.Forms.DataGridViewTextBoxColumn Kom;
        private System.Windows.Forms.DataGridViewButtonColumn Dodaj;
        private System.Windows.Forms.DataGridViewButtonColumn Oduzmi;
        public System.Windows.Forms.DataGridView dg1;
        public System.Windows.Forms.Label label1;

    }
}
