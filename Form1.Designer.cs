﻿namespace PRSPLAN
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.toolbar = new System.Windows.Forms.ToolStrip();
            this.btnSelect = new System.Windows.Forms.ToolStripButton();
            this.btnMove = new System.Windows.Forms.ToolStripButton();
            this.btnEraser = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnText = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.btnRoad = new System.Windows.Forms.ToolStripButton();
            this.btnRec = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnLine = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.btnDemontiraniK = new System.Windows.Forms.ToolStripButton();
            this.btnNoviKabel = new System.Windows.Forms.ToolStripButton();
            this.btnPostojeciK = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPKDemontirani = new System.Windows.Forms.ToolStripButton();
            this.btnPKNovi = new System.Windows.Forms.ToolStripButton();
            this.btnPKPostojeci = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.btnIzvod = new System.Windows.Forms.ToolStripButton();
            this.btnKutija = new System.Windows.Forms.ToolStripButton();
            this.btnEStup = new System.Windows.Forms.ToolStripButton();
            this.btnStup = new System.Windows.Forms.ToolStripButton();
            this.btnStupNovi = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Nacrt = new System.Windows.Forms.Panel();
            this.plan = new System.Windows.Forms.PictureBox();
            this.trMill = new System.Windows.Forms.TrackBar();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.datotekaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menubtnOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.menubtnSave = new System.Windows.Forms.ToolStripMenuItem();
            this.spremiKaoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.velicinaDokumentaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.izlazToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnNew = new System.Windows.Forms.ToolStripButton();
            this.btnOpen = new System.Windows.Forms.ToolStripButton();
            this.btnSave = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.btnSaveAll = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.btnExportJPG = new System.Windows.Forms.ToolStripButton();
            this.btnPrint = new System.Windows.Forms.ToolStripButton();
            this.btnSendMail = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.btUndo = new System.Windows.Forms.ToolStripButton();
            this.btnRedo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.btnDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.btnZaglavlje = new System.Windows.Forms.ToolStripButton();
            this.pd1 = new System.Windows.Forms.PrintDialog();
            this.EditStrip = new System.Windows.Forms.ToolStrip();
            this.btnNacrt = new System.Windows.Forms.ToolStripButton();
            this.btnFazeRada = new System.Windows.Forms.ToolStripButton();
            this.btnMaterijal = new System.Windows.Forms.ToolStripButton();
            this.btnDemMaterijal = new System.Windows.Forms.ToolStripButton();
            this.btnSlike = new System.Windows.Forms.ToolStripButton();
            this.lblCordinates = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.lbl1 = new System.Windows.Forms.ToolStripLabel();
            this.roadCbSize = new System.Windows.Forms.ToolStripComboBox();
            this.lbl2 = new System.Windows.Forms.ToolStripLabel();
            this.texttext = new System.Windows.Forms.ToolStripTextBox();
            this.lbl3 = new System.Windows.Forms.ToolStripLabel();
            this.texttext2 = new System.Windows.Forms.ToolStripTextBox();
            this.lbl4 = new System.Windows.Forms.ToolStripLabel();
            this.textsize = new System.Windows.Forms.ToolStripComboBox();
            this.btnColor = new System.Windows.Forms.ToolStripButton();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.chSkala = new System.Windows.Forms.CheckBox();
            this.drawPanel = new System.Windows.Forms.Panel();
            this.toolbar.SuspendLayout();
            this.panel1.SuspendLayout();
            this.Nacrt.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.plan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trMill)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.EditStrip.SuspendLayout();
            this.drawPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolbar
            // 
            this.toolbar.AutoSize = false;
            this.toolbar.BackColor = System.Drawing.SystemColors.Control;
            this.toolbar.Dock = System.Windows.Forms.DockStyle.None;
            this.toolbar.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolbar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnSelect,
            this.btnMove,
            this.btnEraser,
            this.toolStripSeparator1,
            this.btnText,
            this.toolStripSeparator8,
            this.btnRoad,
            this.btnRec,
            this.toolStripSeparator2,
            this.btnLine,
            this.toolStripSeparator12,
            this.btnDemontiraniK,
            this.btnNoviKabel,
            this.btnPostojeciK,
            this.toolStripSeparator11,
            this.btnPKDemontirani,
            this.btnPKNovi,
            this.btnPKPostojeci,
            this.toolStripSeparator7,
            this.btnIzvod,
            this.btnKutija,
            this.btnEStup,
            this.btnStup,
            this.btnStupNovi,
            this.toolStripSeparator3});
            this.toolbar.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
            this.toolbar.Location = new System.Drawing.Point(0, 0);
            this.toolbar.Name = "toolbar";
            this.toolbar.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolbar.Size = new System.Drawing.Size(43, 666);
            this.toolbar.TabIndex = 4;
            this.toolbar.Text = "toolStrip1";
            this.toolbar.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolbar_ItemClicked);
            // 
            // btnSelect
            // 
            this.btnSelect.Checked = true;
            this.btnSelect.CheckState = System.Windows.Forms.CheckState.Checked;
            this.btnSelect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSelect.Image = ((System.Drawing.Image)(resources.GetObject("btnSelect.Image")));
            this.btnSelect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(41, 28);
            this.btnSelect.Tag = "Select";
            this.btnSelect.Text = "Oznaci";
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // btnMove
            // 
            this.btnMove.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnMove.Image = ((System.Drawing.Image)(resources.GetObject("btnMove.Image")));
            this.btnMove.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnMove.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnMove.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnMove.Name = "btnMove";
            this.btnMove.Size = new System.Drawing.Size(41, 36);
            this.btnMove.Tag = "Move";
            this.btnMove.Visible = false;
            // 
            // btnEraser
            // 
            this.btnEraser.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEraser.Image = ((System.Drawing.Image)(resources.GetObject("btnEraser.Image")));
            this.btnEraser.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEraser.Name = "btnEraser";
            this.btnEraser.Size = new System.Drawing.Size(41, 28);
            this.btnEraser.Tag = "Eraser";
            this.btnEraser.Text = "Brisanje";
            this.btnEraser.Click += new System.EventHandler(this.btnEraser_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(41, 6);
            // 
            // btnText
            // 
            this.btnText.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnText.Image = ((System.Drawing.Image)(resources.GetObject("btnText.Image")));
            this.btnText.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnText.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnText.Name = "btnText";
            this.btnText.Size = new System.Drawing.Size(41, 20);
            this.btnText.Tag = "LineText";
            this.btnText.Text = "Text";
            this.btnText.Click += new System.EventHandler(this.btnText_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(41, 6);
            // 
            // btnRoad
            // 
            this.btnRoad.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnRoad.Image = ((System.Drawing.Image)(resources.GetObject("btnRoad.Image")));
            this.btnRoad.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnRoad.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRoad.Name = "btnRoad";
            this.btnRoad.Size = new System.Drawing.Size(41, 20);
            this.btnRoad.Tag = "Road";
            this.btnRoad.Text = "Cesta";
            // 
            // btnRec
            // 
            this.btnRec.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnRec.Image = ((System.Drawing.Image)(resources.GetObject("btnRec.Image")));
            this.btnRec.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnRec.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRec.Name = "btnRec";
            this.btnRec.Size = new System.Drawing.Size(41, 20);
            this.btnRec.Tag = "Rec";
            this.btnRec.Text = "Objekt";
            this.btnRec.Click += new System.EventHandler(this.btnRec_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(41, 6);
            // 
            // btnLine
            // 
            this.btnLine.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnLine.Image = ((System.Drawing.Image)(resources.GetObject("btnLine.Image")));
            this.btnLine.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnLine.Name = "btnLine";
            this.btnLine.Size = new System.Drawing.Size(41, 28);
            this.btnLine.Tag = "Line";
            this.btnLine.Text = "Line";
            this.btnLine.Click += new System.EventHandler(this.btnLine_Click);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(41, 6);
            // 
            // btnDemontiraniK
            // 
            this.btnDemontiraniK.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDemontiraniK.Image = ((System.Drawing.Image)(resources.GetObject("btnDemontiraniK.Image")));
            this.btnDemontiraniK.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDemontiraniK.Name = "btnDemontiraniK";
            this.btnDemontiraniK.Size = new System.Drawing.Size(41, 28);
            this.btnDemontiraniK.Tag = "KDemontirani";
            this.btnDemontiraniK.Text = "Novi kabel";
            // 
            // btnNoviKabel
            // 
            this.btnNoviKabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnNoviKabel.Image = ((System.Drawing.Image)(resources.GetObject("btnNoviKabel.Image")));
            this.btnNoviKabel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNoviKabel.Name = "btnNoviKabel";
            this.btnNoviKabel.Size = new System.Drawing.Size(41, 28);
            this.btnNoviKabel.Tag = "KNovi";
            this.btnNoviKabel.Text = "Demontirani kabel";
            // 
            // btnPostojeciK
            // 
            this.btnPostojeciK.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPostojeciK.Image = ((System.Drawing.Image)(resources.GetObject("btnPostojeciK.Image")));
            this.btnPostojeciK.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPostojeciK.Name = "btnPostojeciK";
            this.btnPostojeciK.Size = new System.Drawing.Size(41, 28);
            this.btnPostojeciK.Tag = "KPostojeci";
            this.btnPostojeciK.Text = "Postojeci kabel";
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(41, 6);
            // 
            // btnPKDemontirani
            // 
            this.btnPKDemontirani.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPKDemontirani.Image = ((System.Drawing.Image)(resources.GetObject("btnPKDemontirani.Image")));
            this.btnPKDemontirani.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPKDemontirani.Name = "btnPKDemontirani";
            this.btnPKDemontirani.Size = new System.Drawing.Size(41, 28);
            this.btnPKDemontirani.Tag = "PKDemontirani";
            this.btnPKDemontirani.Text = "Novi kabel";
            // 
            // btnPKNovi
            // 
            this.btnPKNovi.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPKNovi.Image = ((System.Drawing.Image)(resources.GetObject("btnPKNovi.Image")));
            this.btnPKNovi.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPKNovi.Name = "btnPKNovi";
            this.btnPKNovi.Size = new System.Drawing.Size(41, 28);
            this.btnPKNovi.Tag = "PKNovi";
            this.btnPKNovi.Text = "Demontirani kabel";
            // 
            // btnPKPostojeci
            // 
            this.btnPKPostojeci.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPKPostojeci.Image = ((System.Drawing.Image)(resources.GetObject("btnPKPostojeci.Image")));
            this.btnPKPostojeci.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPKPostojeci.Name = "btnPKPostojeci";
            this.btnPKPostojeci.Size = new System.Drawing.Size(41, 28);
            this.btnPKPostojeci.Tag = "PKPostojeci";
            this.btnPKPostojeci.Text = "Postojeci kabel";
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(41, 6);
            // 
            // btnIzvod
            // 
            this.btnIzvod.BackColor = System.Drawing.Color.Transparent;
            this.btnIzvod.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnIzvod.Image = ((System.Drawing.Image)(resources.GetObject("btnIzvod.Image")));
            this.btnIzvod.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnIzvod.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnIzvod.Name = "btnIzvod";
            this.btnIzvod.Size = new System.Drawing.Size(41, 24);
            this.btnIzvod.Tag = "Izvod";
            this.btnIzvod.Text = "Izvod";
            // 
            // btnKutija
            // 
            this.btnKutija.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnKutija.Image = ((System.Drawing.Image)(resources.GetObject("btnKutija.Image")));
            this.btnKutija.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnKutija.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnKutija.Name = "btnKutija";
            this.btnKutija.Size = new System.Drawing.Size(41, 24);
            this.btnKutija.Tag = "Kutija";
            this.btnKutija.Text = "Kutija";
            this.btnKutija.Visible = false;
            // 
            // btnEStup
            // 
            this.btnEStup.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEStup.Image = ((System.Drawing.Image)(resources.GetObject("btnEStup.Image")));
            this.btnEStup.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnEStup.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEStup.Name = "btnEStup";
            this.btnEStup.Size = new System.Drawing.Size(41, 24);
            this.btnEStup.Tag = "EStup";
            this.btnEStup.Text = "Stup (elektra)";
            // 
            // btnStup
            // 
            this.btnStup.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnStup.Image = ((System.Drawing.Image)(resources.GetObject("btnStup.Image")));
            this.btnStup.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnStup.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnStup.Name = "btnStup";
            this.btnStup.Size = new System.Drawing.Size(41, 24);
            this.btnStup.Tag = "Stup";
            this.btnStup.Text = "Stup";
            // 
            // btnStupNovi
            // 
            this.btnStupNovi.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnStupNovi.Image = ((System.Drawing.Image)(resources.GetObject("btnStupNovi.Image")));
            this.btnStupNovi.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnStupNovi.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnStupNovi.Name = "btnStupNovi";
            this.btnStupNovi.Size = new System.Drawing.Size(41, 24);
            this.btnStupNovi.Tag = "StupNovi";
            this.btnStupNovi.Text = "StupNovi";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(41, 6);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.Nacrt);
            this.panel1.Location = new System.Drawing.Point(0, 77);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(786, 635);
            this.panel1.TabIndex = 5;
            // 
            // Nacrt
            // 
            this.Nacrt.Controls.Add(this.drawPanel);
            this.Nacrt.Controls.Add(this.toolbar);
            this.Nacrt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Nacrt.Location = new System.Drawing.Point(0, 0);
            this.Nacrt.Name = "Nacrt";
            this.Nacrt.Size = new System.Drawing.Size(786, 635);
            this.Nacrt.TabIndex = 5;
            // 
            // plan
            // 
            this.plan.BackColor = System.Drawing.Color.White;
            this.plan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.plan.Cursor = System.Windows.Forms.Cursors.UpArrow;
            this.plan.Location = new System.Drawing.Point(3, 3);
            this.plan.Name = "plan";
            this.plan.Size = new System.Drawing.Size(378, 375);
            this.plan.TabIndex = 0;
            this.plan.TabStop = false;
            this.plan.Click += new System.EventHandler(this.plan_Click);
            this.plan.MouseDown += new System.Windows.Forms.MouseEventHandler(this.plan_MouseDown);
            this.plan.MouseLeave += new System.EventHandler(this.plan_MouseLeave);
            this.plan.MouseMove += new System.Windows.Forms.MouseEventHandler(this.plan_MouseMove);
            this.plan.MouseUp += new System.Windows.Forms.MouseEventHandler(this.plan_MouseUp);
            // 
            // trMill
            // 
            this.trMill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.trMill.AutoSize = false;
            this.trMill.Location = new System.Drawing.Point(193, 718);
            this.trMill.Minimum = 1;
            this.trMill.Name = "trMill";
            this.trMill.Size = new System.Drawing.Size(93, 21);
            this.trMill.TabIndex = 7;
            this.trMill.Value = 1;
            this.trMill.Visible = false;
            this.trMill.Scroll += new System.EventHandler(this.trMill_Scroll);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.datotekaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(786, 24);
            this.menuStrip1.TabIndex = 8;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // datotekaToolStripMenuItem
            // 
            this.datotekaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menubtnOpen,
            this.menubtnSave,
            this.spremiKaoToolStripMenuItem1,
            this.velicinaDokumentaToolStripMenuItem,
            this.izlazToolStripMenuItem});
            this.datotekaToolStripMenuItem.Name = "datotekaToolStripMenuItem";
            this.datotekaToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.datotekaToolStripMenuItem.Text = "Datoteka";
            this.datotekaToolStripMenuItem.Click += new System.EventHandler(this.datotekaToolStripMenuItem_Click);
            // 
            // menubtnOpen
            // 
            this.menubtnOpen.Name = "menubtnOpen";
            this.menubtnOpen.Size = new System.Drawing.Size(165, 22);
            this.menubtnOpen.Text = "Otvori";
            this.menubtnOpen.Click += new System.EventHandler(this.menubtnOpen_Click);
            // 
            // menubtnSave
            // 
            this.menubtnSave.Name = "menubtnSave";
            this.menubtnSave.Size = new System.Drawing.Size(165, 22);
            this.menubtnSave.Text = "Spremi";
            this.menubtnSave.Click += new System.EventHandler(this.menubtnSave_Click);
            // 
            // spremiKaoToolStripMenuItem1
            // 
            this.spremiKaoToolStripMenuItem1.Name = "spremiKaoToolStripMenuItem1";
            this.spremiKaoToolStripMenuItem1.Size = new System.Drawing.Size(165, 22);
            this.spremiKaoToolStripMenuItem1.Text = "Spremi kao...";
            // 
            // velicinaDokumentaToolStripMenuItem
            // 
            this.velicinaDokumentaToolStripMenuItem.Name = "velicinaDokumentaToolStripMenuItem";
            this.velicinaDokumentaToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.velicinaDokumentaToolStripMenuItem.Text = "Velicina nacrta";
            this.velicinaDokumentaToolStripMenuItem.Click += new System.EventHandler(this.velicinaDokumentaToolStripMenuItem_Click);
            // 
            // izlazToolStripMenuItem
            // 
            this.izlazToolStripMenuItem.Name = "izlazToolStripMenuItem";
            this.izlazToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.izlazToolStripMenuItem.Text = "Izlaz";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNew,
            this.btnOpen,
            this.btnSave,
            this.toolStripSeparator13,
            this.btnSaveAll,
            this.toolStripSeparator9,
            this.btnExportJPG,
            this.btnPrint,
            this.btnSendMail,
            this.toolStripSeparator4,
            this.btUndo,
            this.btnRedo,
            this.toolStripSeparator5,
            this.btnDelete,
            this.toolStripSeparator6,
            this.btnZaglavlje});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(786, 25);
            this.toolStrip1.TabIndex = 9;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnNew
            // 
            this.btnNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnNew.Image = ((System.Drawing.Image)(resources.GetObject("btnNew.Image")));
            this.btnNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(23, 22);
            this.btnNew.Text = "Novi dokument";
            this.btnNew.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // btnOpen
            // 
            this.btnOpen.Image = ((System.Drawing.Image)(resources.GetObject("btnOpen.Image")));
            this.btnOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(57, 22);
            this.btnOpen.Text = "Otvori";
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // btnSave
            // 
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(59, 22);
            this.btnSave.Text = "Spremi";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(6, 25);
            // 
            // btnSaveAll
            // 
            this.btnSaveAll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSaveAll.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveAll.Image")));
            this.btnSaveAll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSaveAll.Name = "btnSaveAll";
            this.btnSaveAll.Size = new System.Drawing.Size(23, 22);
            this.btnSaveAll.Text = "toolStripButton1";
            this.btnSaveAll.ToolTipText = "Spremi sve";
            this.btnSaveAll.Click += new System.EventHandler(this.btnSaveAll_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(6, 25);
            // 
            // btnExportJPG
            // 
            this.btnExportJPG.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnExportJPG.Image = ((System.Drawing.Image)(resources.GetObject("btnExportJPG.Image")));
            this.btnExportJPG.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnExportJPG.Name = "btnExportJPG";
            this.btnExportJPG.Size = new System.Drawing.Size(23, 22);
            this.btnExportJPG.Text = "Izvezi sliku";
            this.btnExportJPG.Click += new System.EventHandler(this.btnExportJPG_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrint.Image = ((System.Drawing.Image)(resources.GetObject("btnPrint.Image")));
            this.btnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(23, 22);
            this.btnPrint.Text = "Printaj";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnSendMail
            // 
            this.btnSendMail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSendMail.Image = ((System.Drawing.Image)(resources.GetObject("btnSendMail.Image")));
            this.btnSendMail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSendMail.Name = "btnSendMail";
            this.btnSendMail.Size = new System.Drawing.Size(23, 22);
            this.btnSendMail.Text = "Pošalji mail";
            this.btnSendMail.Click += new System.EventHandler(this.btnSendMail_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // btUndo
            // 
            this.btUndo.Image = ((System.Drawing.Image)(resources.GetObject("btUndo.Image")));
            this.btUndo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btUndo.Name = "btUndo";
            this.btUndo.Size = new System.Drawing.Size(52, 22);
            this.btUndo.Text = "Undo";
            this.btUndo.Click += new System.EventHandler(this.btUndo_Click);
            // 
            // btnRedo
            // 
            this.btnRedo.Image = ((System.Drawing.Image)(resources.GetObject("btnRedo.Image")));
            this.btnRedo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRedo.Name = "btnRedo";
            this.btnRedo.Size = new System.Drawing.Size(52, 22);
            this.btnRedo.Text = "Redo";
            this.btnRedo.Click += new System.EventHandler(this.btnRedo_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // btnDelete
            // 
            this.btnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(23, 22);
            this.btnDelete.Text = "Briši selektirano";
            this.btnDelete.Visible = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // btnZaglavlje
            // 
            this.btnZaglavlje.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnZaglavlje.Image = ((System.Drawing.Image)(resources.GetObject("btnZaglavlje.Image")));
            this.btnZaglavlje.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnZaglavlje.Name = "btnZaglavlje";
            this.btnZaglavlje.Size = new System.Drawing.Size(54, 22);
            this.btnZaglavlje.Text = "Zaglavlje";
            this.btnZaglavlje.Click += new System.EventHandler(this.btnZaglavlje_Click);
            // 
            // pd1
            // 
            this.pd1.UseEXDialog = true;
            // 
            // EditStrip
            // 
            this.EditStrip.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.EditStrip.AutoSize = false;
            this.EditStrip.BackColor = System.Drawing.SystemColors.Control;
            this.EditStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.EditStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNacrt,
            this.btnFazeRada,
            this.btnMaterijal,
            this.btnDemMaterijal,
            this.btnSlike,
            this.lblCordinates,
            this.toolStripSeparator10,
            this.lbl1,
            this.roadCbSize,
            this.lbl2,
            this.texttext,
            this.lbl3,
            this.texttext2,
            this.lbl4,
            this.textsize,
            this.btnColor});
            this.EditStrip.Location = new System.Drawing.Point(0, 49);
            this.EditStrip.Name = "EditStrip";
            this.EditStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.EditStrip.Size = new System.Drawing.Size(786, 25);
            this.EditStrip.TabIndex = 10;
            this.EditStrip.Text = "toolStrip2";
            this.EditStrip.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.EditStrip_ItemClicked);
            // 
            // btnNacrt
            // 
            this.btnNacrt.Checked = true;
            this.btnNacrt.CheckState = System.Windows.Forms.CheckState.Checked;
            this.btnNacrt.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnNacrt.Image = ((System.Drawing.Image)(resources.GetObject("btnNacrt.Image")));
            this.btnNacrt.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNacrt.Name = "btnNacrt";
            this.btnNacrt.Size = new System.Drawing.Size(37, 22);
            this.btnNacrt.Tag = "NACRT";
            this.btnNacrt.Text = "Nacrt";
            this.btnNacrt.Click += new System.EventHandler(this.btnFazeRada_Click);
            // 
            // btnFazeRada
            // 
            this.btnFazeRada.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnFazeRada.Image = ((System.Drawing.Image)(resources.GetObject("btnFazeRada.Image")));
            this.btnFazeRada.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnFazeRada.Name = "btnFazeRada";
            this.btnFazeRada.Size = new System.Drawing.Size(59, 22);
            this.btnFazeRada.Tag = "FAZERADA";
            this.btnFazeRada.Text = "Faze rada";
            this.btnFazeRada.Click += new System.EventHandler(this.btnFazeRada_Click);
            // 
            // btnMaterijal
            // 
            this.btnMaterijal.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnMaterijal.Image = ((System.Drawing.Image)(resources.GetObject("btnMaterijal.Image")));
            this.btnMaterijal.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnMaterijal.Name = "btnMaterijal";
            this.btnMaterijal.Size = new System.Drawing.Size(95, 22);
            this.btnMaterijal.Tag = "MATERIJAL";
            this.btnMaterijal.Text = "Utrošeni materijal";
            this.btnMaterijal.Click += new System.EventHandler(this.btnFazeRada_Click);
            // 
            // btnDemMaterijal
            // 
            this.btnDemMaterijal.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnDemMaterijal.Image = ((System.Drawing.Image)(resources.GetObject("btnDemMaterijal.Image")));
            this.btnDemMaterijal.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDemMaterijal.Name = "btnDemMaterijal";
            this.btnDemMaterijal.Size = new System.Drawing.Size(112, 22);
            this.btnDemMaterijal.Tag = "DEMMATERIJAL";
            this.btnDemMaterijal.Text = "Demontirani materijal";
            this.btnDemMaterijal.Click += new System.EventHandler(this.btnFazeRada_Click);
            // 
            // btnSlike
            // 
            this.btnSlike.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnSlike.Image = ((System.Drawing.Image)(resources.GetObject("btnSlike.Image")));
            this.btnSlike.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSlike.Name = "btnSlike";
            this.btnSlike.Size = new System.Drawing.Size(32, 22);
            this.btnSlike.Tag = "SLIKE";
            this.btnSlike.Text = "Slike";
            this.btnSlike.Click += new System.EventHandler(this.btnFazeRada_Click);
            // 
            // lblCordinates
            // 
            this.lblCordinates.Name = "lblCordinates";
            this.lblCordinates.Size = new System.Drawing.Size(35, 22);
            this.lblCordinates.Text = "00,00";
            this.lblCordinates.Visible = false;
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(6, 25);
            // 
            // lbl1
            // 
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(37, 22);
            this.lbl1.Text = "Širina:";
            // 
            // roadCbSize
            // 
            this.roadCbSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.roadCbSize.Items.AddRange(new object[] {
            "10",
            "20",
            "30",
            "40",
            "50",
            "60",
            "70",
            "80",
            "90",
            "100"});
            this.roadCbSize.Name = "roadCbSize";
            this.roadCbSize.Size = new System.Drawing.Size(75, 25);
            this.roadCbSize.SelectedIndexChanged += new System.EventHandler(this.roadCbSize_SelectedIndexChanged);
            this.roadCbSize.Click += new System.EventHandler(this.roadCbSize_Click);
            // 
            // lbl2
            // 
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(42, 22);
            this.lbl2.Text = "Text 1:";
            // 
            // texttext
            // 
            this.texttext.Name = "texttext";
            this.texttext.Size = new System.Drawing.Size(100, 25);
            this.texttext.KeyUp += new System.Windows.Forms.KeyEventHandler(this.texttext_KeyUp);
            this.texttext.Click += new System.EventHandler(this.texttext_Click);
            this.texttext.TextChanged += new System.EventHandler(this.texttext_TextChanged);
            // 
            // lbl3
            // 
            this.lbl3.Name = "lbl3";
            this.lbl3.Size = new System.Drawing.Size(42, 22);
            this.lbl3.Text = "Text 2:";
            // 
            // texttext2
            // 
            this.texttext2.Name = "texttext2";
            this.texttext2.Size = new System.Drawing.Size(100, 25);
            this.texttext2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.texttext2_KeyUp);
            this.texttext2.Click += new System.EventHandler(this.texttext2_Click);
            this.texttext2.TextChanged += new System.EventHandler(this.texttext2_TextChanged);
            // 
            // lbl4
            // 
            this.lbl4.Name = "lbl4";
            this.lbl4.Size = new System.Drawing.Size(46, 13);
            this.lbl4.Text = "Velicina:";
            // 
            // textsize
            // 
            this.textsize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.textsize.Items.AddRange(new object[] {
            "6",
            "7",
            "8",
            "9",
            "10",
            "12",
            "14",
            "16",
            "20",
            "24",
            "30"});
            this.textsize.Name = "textsize";
            this.textsize.Size = new System.Drawing.Size(75, 21);
            this.textsize.SelectedIndexChanged += new System.EventHandler(this.textsize_SelectedIndexChanged);
            this.textsize.Click += new System.EventHandler(this.textsize_Click);
            // 
            // btnColor
            // 
            this.btnColor.BackColor = System.Drawing.Color.SlateGray;
            this.btnColor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.None;
            this.btnColor.Image = ((System.Drawing.Image)(resources.GetObject("btnColor.Image")));
            this.btnColor.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnColor.Name = "btnColor";
            this.btnColor.Size = new System.Drawing.Size(23, 4);
            this.btnColor.Text = "toolStripButton3";
            this.btnColor.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(786, 742);
            this.shapeContainer1.TabIndex = 11;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape1
            // 
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 351;
            this.lineShape1.X2 = 471;
            this.lineShape1.Y1 = 310;
            this.lineShape1.Y2 = 318;
            // 
            // chSkala
            // 
            this.chSkala.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chSkala.AutoSize = true;
            this.chSkala.Checked = global::PRSPLAN.Properties.Settings.Default.chKoordinatnaMreza;
            this.chSkala.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chSkala.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::PRSPLAN.Properties.Settings.Default, "chKoordinatnaMreza", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.chSkala.Location = new System.Drawing.Point(46, 722);
            this.chSkala.Name = "chSkala";
            this.chSkala.Size = new System.Drawing.Size(113, 17);
            this.chSkala.TabIndex = 3;
            this.chSkala.Text = "koordinatna mreža";
            this.chSkala.UseVisualStyleBackColor = true;
            this.chSkala.Visible = false;
            this.chSkala.CheckedChanged += new System.EventHandler(this.chSkala_CheckedChanged);
            // 
            // drawPanel
            // 
            this.drawPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.drawPanel.AutoScroll = true;
            this.drawPanel.Controls.Add(this.plan);
            this.drawPanel.Location = new System.Drawing.Point(46, 3);
            this.drawPanel.Name = "drawPanel";
            this.drawPanel.Size = new System.Drawing.Size(737, 629);
            this.drawPanel.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(786, 742);
            this.Controls.Add(this.EditStrip);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.trMill);
            this.Controls.Add(this.chSkala);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.shapeContainer1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "prsIMS";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyUp);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.toolbar.ResumeLayout(false);
            this.toolbar.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.Nacrt.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.plan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trMill)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.EditStrip.ResumeLayout(false);
            this.EditStrip.PerformLayout();
            this.drawPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox plan;
        private System.Windows.Forms.CheckBox chSkala;
        private System.Windows.Forms.ToolStrip toolbar;
        private System.Windows.Forms.ToolStripButton btnLine;
        private System.Windows.Forms.ToolStripButton btnRec;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnNoviKabel;
        private System.Windows.Forms.ToolStripButton btnPostojeciK;
        private System.Windows.Forms.ToolStripButton btnDemontiraniK;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TrackBar trMill;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton btnIzvod;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem datotekaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menubtnOpen;
        private System.Windows.Forms.ToolStripMenuItem menubtnSave;
        private System.Windows.Forms.ToolStripMenuItem spremiKaoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem izlazToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton btnSelect;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton btnEraser;
        private System.Windows.Forms.ToolStripButton btnRoad;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnSave;
        private System.Windows.Forms.ToolStripButton btnExportJPG;
        private System.Windows.Forms.ToolStripButton btnPrint;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton btUndo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton btnDelete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton btnZaglavlje;
        private System.Windows.Forms.PrintDialog pd1;
        private System.Windows.Forms.ToolStripButton btnPKNovi;
        private System.Windows.Forms.ToolStripButton btnPKPostojeci;
        private System.Windows.Forms.ToolStripButton btnPKDemontirani;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripButton btnEStup;
        private System.Windows.Forms.ToolStripButton btnStup;
        private System.Windows.Forms.ToolStripButton btnKutija;
        private System.Windows.Forms.ToolStripButton btnText;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStrip EditStrip;
        private System.Windows.Forms.ToolStripLabel lbl1;
        private System.Windows.Forms.ToolStripComboBox roadCbSize;
        private System.Windows.Forms.ToolStripTextBox texttext;
        private System.Windows.Forms.ToolStripButton btnNew;
        private System.Windows.Forms.ToolStripButton btnSendMail;
        private System.Windows.Forms.ToolStripTextBox texttext2;
        private System.Windows.Forms.ToolStripComboBox textsize;
        private System.Windows.Forms.ToolStripButton btnColor;
        private System.Windows.Forms.ToolStripButton btnOpen;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripButton btnRedo;
        private System.Windows.Forms.ToolStripButton btnStupNovi;
        private System.Windows.Forms.ToolStripLabel lbl2;
        private System.Windows.Forms.ToolStripLabel lbl3;
        private System.Windows.Forms.ToolStripLabel lbl4;
        private System.Windows.Forms.ToolStripLabel lblCordinates;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripButton btnNacrt;
        private System.Windows.Forms.ToolStripButton btnFazeRada;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripButton btnSaveAll;
        private System.Windows.Forms.ToolStripButton btnSlike;
        private System.Windows.Forms.Panel Nacrt;
        private System.Windows.Forms.ToolStripMenuItem velicinaDokumentaToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton btnMaterijal;
        private System.Windows.Forms.ToolStripButton btnDemMaterijal;
        private System.Windows.Forms.ToolStripButton btnMove;
        private System.Windows.Forms.Panel drawPanel;
    }
}

