﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace PRSPLAN
{
   public  class cSelector
    {
     
      public  cRec rectangle = new cRec(new Point(0, 0), new Point(0, 0),MyColors.selector );
      private bool _enabled = false;
      public bool enabled
      {
          get { return _enabled; }
          set
          {
              _enabled = value;
              if (value == false)
              {
                  rectangle.point1 = new Point(0, 0);
                  rectangle.point3 = new Point(0, 0);
              }
          }
      }

      public  void setRectangle(Point p1, Point p3)
   {
           
       rectangle.point1 = p1;
       rectangle.point3 = p3;
       
   }
       
       public  void drawon(Bitmap b, Graphics g ) 
      {
           Pen p=new Pen(new SolidBrush(MyColors.selector ));
           p.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
           g.DrawRectangle(p, rectangle.GetRectangle());

      }
    }


}
