﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace PRSPLAN
{
    [Serializable]
    public  class cPoint
    {
        public delegate void PointChangedEventHandler();
        public event PointChangedEventHandler PointChanged;
        public bool selected = false;

        private int _x = 0;
        private int _y = 0;
        
       public  int X {
           get { return _x; }
           set {
               if (_x != value) { _x = value; if (PointChanged != null) { PointChanged(); }; }

           }
       }
       public int Y {
           get { return _y; }
           set
           {
               if (_y != value) { _y= value; if (PointChanged != null) { PointChanged(); }; }

           }
       }
 
        public cPoint(int ix, int iy)
        {
            X = ix;
            Y = iy;
        }
 

      public cPoint(System.Drawing.Point p)
        {
            X = p.X ;
            Y = p.Y ;
        }
      
        public Point ToPoint()
      {
          return new Point(X, Y);

      }

        public void drawCode(Graphics g )
        {
            Color color = MyColors.getColorByCode(0, MyColors.codePoint);
            Rectangle rec = new Rectangle(X - MyColors.koefPoint, Y - MyColors.koefPoint, MyColors.koefPoint * 2, MyColors.koefPoint * 2);
            g.DrawLine(new Pen(new SolidBrush(color  )), new Point(X, Y), new Point(X+1, Y+1));
           // g.FillEllipse(new SolidBrush(color), rec); 
        }
    }
}
