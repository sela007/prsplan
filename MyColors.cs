﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace PRSPLAN
{
   public static  class MyColors
    {
       public static Color selector = Color.Pink;
       public static Color road = Color.Gainsboro;
       public static Color net = Color.FromArgb(250, 250, 250);
       public static Color selectedColor = Color.HotPink;
       public static Color net10 = Color.FromArgb(220, 220, 220);

       /*
       public static Color bgRec = Color.FromArgb(0, 0, 1);
       public static Color bgLine = Color.FromArgb(0, 0, 2);
       public static Color bgLineText = Color.FromArgb(0, 0, 3);
       public static Color bgSymbol = Color.FromArgb(0, 0, 4);
       public static Color bgPoint = Color.FromArgb(0, 0, 5);
       */

       public static byte codeRoad = 1;
       public static byte codeRec = 2;
       public static byte codeLine = 3;
       public static byte codeSymbol = 4;
       public static byte codeLineText = 5;
       public static byte codePoint = 6;
       public static byte codeZaglavlje = 7;

       public static int koefLine = 7;
       public static int koefPoint = 0;

       public static Color getColorByCode(int elementCode, byte elementType)
       {
           Color c=Color.FromArgb(0, elementCode, elementType);
           return c;
       }

       public static elementCodeInfo getCodeByColor(Color c)
       {
           elementCodeInfo ei = new elementCodeInfo();
           ei.elementCode = c.G;
           ei.elementType= c.B;
           return ei;
       }

       const int RGBMAX = 255;

       public static Color InvertColor(Color ColourToInvert)
       {
           return Color.FromArgb(RGBMAX - ColourToInvert.R,
             RGBMAX - ColourToInvert.G, RGBMAX - ColourToInvert.B);
       }
    }


   public class elementCodeInfo
   {
       public int elementCode;
       public byte elementType;

       public string ToString()
       {

           return elementType.ToString();
           //switch (elementType)
           //{
           //    case 1: return "ROAD"; 
                 
           //    case 2: return "REC"; 
                   
           //    case 3: return "LINE";
                  
           //    case 4: return "SYMBOL"; 
                  
           //     case 5: return "LINETEXT"; 
                   
           //     case 6: return "POINT"; 
                   
           //} 
           //return "GFDGFD";
       }
   }
}
