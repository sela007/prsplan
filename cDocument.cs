﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Drawing;

namespace PRSPLAN
{
    [Serializable]
    public  class cDocument
    {
        public string name { get; set; }
        public string filePath = "";
        public string folder = "";
        public string fazeRada = "";
        public string materijal = "";
        public string demMaterijal = "";

        public  cZaglavlje zaglavlje = new cZaglavlje();
        public  List<cLine> lines = new List<cLine>();
        public  List<cSymbol> symbols = new List<cSymbol>();
        public  List<cRec> recs = new List<cRec>();
        public  List<cRoad> roads = new List<cRoad>();
        public  List<cText> texts = new List<cText>();
        public  List<cLineText> linetexts = new List<cLineText>();

       
        public List<object> codeElements = new List<object>();
        public Bitmap codeImage;

        public System.Drawing.Size _paperSize = new System.Drawing.Size(16, 25);
        public System.Drawing.Size paperSizePix = new System.Drawing.Size();
        public System.Drawing.Size paperSize { get { return _paperSize; } set {

            Bitmap b = new Bitmap(System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width, System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height );
               
                double pixelW = -1;
                double pixelH = -1;
                using (Graphics g=Graphics.FromImage(b) )
                {
                    pixelW = value.Width  * g.DpiY / 2.54d;
                    pixelH = value.Height  * g.DpiY / 2.54d;
                }
                paperSizePix.Width =(int) pixelW;
                paperSizePix.Height =(int) pixelH;

 
            _paperSize = value; } }
        public Bitmap image; 
        public Font font = new Font("ARIAL", 10);

      
        public List<Bitmap> slike = new List<Bitmap >();

        public cDocument(Size documentSize)
        {
            paperSize = documentSize;
            image = new Bitmap(paperSizePix.Height  ,paperSizePix.Width  );
            slike = new List<Bitmap>();
            codeImage = new Bitmap(paperSizePix.Height, paperSizePix.Width);
            
        }
        public void ChangeSize(int w, int h)
        {
            paperSize = new Size(w, h);
            image = new Bitmap(paperSizePix.Width, paperSizePix.Height);
            codeImage = new Bitmap(paperSizePix.Width, paperSizePix.Height);
        }

        public void saveFazeRada(string filepath)
        {
            using (StreamWriter outfile = new StreamWriter(filepath ))
            {
                outfile.Write(fazeRada );
                outfile.Write(materijal  );
                outfile.Write(demMaterijal);


            }
        }
        public void save(string fpath)
        {
            try
            {
               // List<MemoryStream> ms = new List<MemoryStream>();

//                Stream str = new MemoryStream();
                filePath = fpath;
                folder = System.IO.Path.GetDirectoryName(fpath);
                using (Stream stream = File.Open(fpath , FileMode.Create))
                {
                    BinaryFormatter bin = new BinaryFormatter();
                    bin.Serialize( stream, this);
                    
                }
                filePath = fpath;
                folder = System.IO.Path.GetDirectoryName(fpath);
            }
            catch (IOException)
            {
            }
            
        }
        private int UndoIndex = -1;
        private List<cDocument> undolist = new List<cDocument>();

        public cDocument  GetPrevoiusState()
        {
            UndoIndex -= 1;
            return undolist[UndoIndex];

        }

        public void SaveCurrentState()
        {
            undolist.Add(Clone());
            UndoIndex = undolist.Count-1;
        }

        public cDocument Clone( )
        {
            cDocument doc = new cDocument(paperSize);
            using (MemoryStream stream = new MemoryStream())
            {
                BinaryFormatter bin = new BinaryFormatter();
                bin.Serialize(stream, this);
                stream.Position = 0;
                doc = (cDocument)bin.Deserialize(stream);
            }
            return doc;
        }


        public void UpdateImage()
        {
            drawon(image);

        }

        public Bitmap getImage()
        {
            Bitmap b = new Bitmap(paperSizePix.Height , paperSizePix.Width );
            drawon(b,false );
            return b;
        }

        public void fazeRadaDrawOn(Bitmap b)
        {
            int hFont = 12;
            
            Font f = new Font("ARIAL", 12);


            using (Graphics g = Graphics.FromImage(b))
            {

            }

        }

        public void drawon(Bitmap b, bool transparent = true)
        {
           
            using (Graphics g = Graphics.FromImage(b))
            {

                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                if (transparent)
                {
                    //g.Clear(Color.Transparent);
                }
                else
                {
                    g.Clear(Color.White);
                }

                Pen p1 = new Pen(Brushes.Red, 2);
                Pen p2 = new Pen(Brushes.LightGreen, 2);
                Pen p3 = new Pen(Brushes.Blue, 2);

                byte lw = 20;
                Font f = new Font("arial", 7);

                g.DrawString("NOVI KABEL", f, Brushes.Black, new PointF(35, 3));
                g.DrawString("DEMONTIRANI KABEL", f, Brushes.Black, new PointF(35, 18));
                g.DrawString("POSTOJEĆI KABEL", f, Brushes.Black, new PointF(35, 33));


                g.DrawLine(p1, 10, 10, 10 + lw, 10);
                g.DrawLine(p2, 10, 25, 10 + lw, 25);
                g.DrawLine(p3, 10, 40, 10 + lw, 40);

                /*
                foreach (cRoad el in roads) { el.drawon(b, g); }
                foreach (cLine el in lines) { el.drawon(b, g); if (el.selected) { el.drawMarkerOn(b, g); } }
                foreach (cRec el in recs) { el.drawon(b, g); if (el.selected) { el.drawMarkerOn(b, g); } }
                foreach (cSymbol el in symbols) { el.drawon(b, g); if (el.selected) { el.drawMarkerOn(b, g); } }
                foreach (cText el in texts) { el.drawon(b, g); }
                foreach (cLineText el in linetexts) { el.drawon(b, g); if (el.selected) { el.drawMarkerOn(b, g); } }
                */
                foreach (cRoad el in roads) { if (el.selected == false) { el.drawon(b, g,false); } }
               
                foreach (cRec el in recs) { if (el.selected == false) { el.drawon(b, g); } }
                foreach (cLine el in lines) { if (el.selected == false) { el.drawon(b, g); } }
               
                foreach (cSymbol el in symbols) { if (el.selected == false) { el.drawon(b, g); } }
                foreach (cText el in texts) { if (el.selected == false) { el.drawon(b, g); } }
                foreach (cLineText el in linetexts) { if (el.selected == false) { el.drawon(b, g); } }
            
                zaglavlje.drawon(b, g);

                //g.DrawString("PREKVRŠJE SVJETLOVOD", font, Brushes.LightGray    , new PointF(5, paperSizePix.Width   - 20));
                //g.DrawImageUnscaled (Properties.Resources.prsv2, new Point(5, paperSizePix.Width - 25));

                Rectangle rec = new Rectangle(5, paperSizePix.Width - 10 - Properties.Resources.PRSICTLOGO.Height, Properties.Resources.PRSICTLOGO.Width , Properties.Resources.PRSICTLOGO.Height );

                g.DrawImage(Properties.Resources.PRSICTLOGO, rec);


            }
        }

        public  void elementsAdd(object element)
        {
            if (element == null) return;
            System.Type t = element.GetType();
            if (t == typeof(cRoad))
            { cRoad el = (cRoad)element; this.roads.Add(el); }

            if (t == typeof(cRec))
            { cRec el = (cRec)element; this.recs.Add(el); }

            if (t == typeof(cSymbol))
            { cSymbol el = (cSymbol)element; this.symbols.Add(el); }

            if (t == typeof(cLine))
            { cLine el = (cLine)element; this.lines.Add(el); }

            if (t == typeof(cLineText))
            { cLineText el = (cLineText)element; this.linetexts.Add(el);}

            if (t == typeof(cText))
            { cText el = (cText)element; this.texts.Add(el); }

        }

        public void elementsRemove(object element, cPoint cp = null)
        {
            if (element == null) { return; }

            System.Type t = element.GetType();

            if (t == typeof(cRec))
            { var el = (cRec)element; recs.Remove(el); return; }

            if (t == typeof(cSymbol))
            { var el = (cSymbol)element; symbols.Remove(el); return; }

            if (t == typeof(cLine))
            { var el = (cLine)element; lines.Remove(el); return; }

            if (t == typeof(cLineText))
            { var el = (cLineText)element; linetexts.Remove(el); return; }

            if (t == typeof(cText))
            { var el = (cText)element; texts.Remove(el); return; }

            if (t == typeof(cRoad))
            {
                var r = (cRoad)element;

                if (cp != null) { r.points.Remove(cp); return; }
                else { roads.Remove(r); return; }

            }



        }

        public void updateCodeImage()
        {
            if (codeImage == null) return;
            using (Graphics g = Graphics.FromImage(codeImage))
            {
                g.Clear(Color.Black);

                for (int i = 0; i <= roads.Count - 1; i++)
                        {
                            roads[i].drawCode(g, i);
                        }

                for (int i = 0; i <= recs.Count - 1; i++)
                        {
                            recs[i].drawCode(g, i);
                        }

                for (int i = 0; i <= lines.Count - 1; i++)
                        {
                            lines[i].drawCode(g, i);
                        }


                 for (int i = 0; i <= symbols.Count - 1; i++)
                        {
                            symbols[i].drawCode(g, i);
                        }

                for (int i = 0; i <= linetexts.Count - 1; i++)
                        {
                            linetexts [i].drawCode(g, i);
                        }



                for (int i = 0; i <= roads.Count - 1; i++)
                {
                    foreach (cPoint p in roads[i].points) { p.drawCode(g); }
                }

                for (int i = 0; i <= recs.Count - 1; i++)
                {
                    recs[i].pointref1.drawCode(g);
                    recs[i].pointref2.drawCode(g);
                    recs[i].pointref3.drawCode(g);
                    recs[i].pointref4.drawCode(g);
                }

                for (int i = 0; i <= lines.Count - 1; i++)
                {
                    lines[i].pointref1.drawCode(g);
                    lines[i].pointref2.drawCode(g);
                }


                for (int i = 0; i <= symbols.Count - 1; i++)
                {
                    symbols[i].pointref.drawCode(g);
                }

                for (int i = 0; i <= linetexts.Count - 1; i++)
                {
                    linetexts[i].pointref1.drawCode(g);
                    linetexts[i].pointref2.drawCode(g);
                }
               
            }


        }
        
     
    }
}
