﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;

namespace PRSPLAN
{
    public partial class frmMail : Form
    {
        public frmMail()
        {
            InitializeComponent();
        }

      private delegate Bitmap BitmapParam ();

      private Func<Bitmap> GetImage;
      private cDocument document;
      private string htmlText = "";

        public frmMail(Func <Bitmap > p,string naslov,string text) 
        {
            InitializeComponent();
            GetImage = p;
            txtNaslov.Text = naslov;
            htmlText  = text;
        }

        
        public frmMail(cDocument doc, string naslov,string text) 
        {
            InitializeComponent();
            document = doc;
            txtNaslov.Text = naslov;
            htmlText  = text;
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            btnSend.Enabled = false;
            lblSlanje.Visible = true;

                try
                {
                    System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();

                    for (int i = 0; i <= lstContacts.Items.Count - 1; i++)
                    {
                        if (lstContacts.GetSelected(i)==true )
                            message.To.Add(Contacts.Persons[i].Mail);
                    }


                    if (message.To.Count == 0) { MessageBox.Show("Primatelj nije oznacen"); return; }


                    if (Directory.Exists(settings.directorySent) == false)
                        Directory.CreateDirectory(settings.directorySent);


                    //    if (System.IO.File.Exists(fPath)) { System.IO.File.Delete(fPath); }

                    //too  CreateJpg();
                    message.Subject = txtNaslov.Text;
                    message.From = new System.Net.Mail.MailAddress(txtSender.Text);

                    if (txtText.Text.Trim() != "")
                    {
                      
                        string[] lines = txtText.Text.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                        foreach (string line in lines)
                        {
                            message.Body += line + "<br>";
                        }
                        message.Body += "<br>";
                    }
                    message.Body += htmlText ;
                    
                    message.IsBodyHtml = true;

                    System.IO.MemoryStream msNacrt = new System.IO.MemoryStream();
                      
                    if (chNacrt.Checked)
                    {
                        
                            Bitmap b = document.getImage();
                            b.Save(msNacrt, ImageFormat.Png);
                            msNacrt.Position = 0;
                            Attachment attNacrt = new Attachment(msNacrt, "nacrt.png");
                            string fname = txtNaslov.Text + ".png";
                            if (fname == ".png") fname = "nacrt.png";
                            attNacrt.ContentDisposition.FileName = fname;
                         
                            message.Attachments.Add(attNacrt);
                        

                    }

                   // MemoryStream[] streams = new MemoryStream[document.slike.Count];
     

                    if (chSlike.Checked)
                    {
                       int i = 0;
                       string[] files = System.IO.Directory.GetFiles(document.folder + @"\slike", "*.jpg");
                       foreach (string f in files)
                       {
                             i++;
                             Attachment a = new Attachment(f);
                             a.ContentDisposition.FileName = "Slika " + i.ToString() + ".jpg";
                             message.Attachments.Add(a);
                       }
                        //foreach (Bitmap b in document.slike)
                        //{
                        //    i++;
                        //    streams[i - 1] = new MemoryStream();
                             
                        //    b.Save(streams  [i-1], ImageFormat.Jpeg );
                        //     streams[i - 1].Position = 0;
                        //    Attachment a = new Attachment(streams[i - 1], "Slika" + i.ToString() + ".jpg");
                        //    a.ContentDisposition.FileName = "Slika " + i.ToString() + ".jpg";
                        //    message.Attachments.Add(a);

                        //}
                    }


                    if (chProjekt.Checked)
                    {
                        string fPath = GetPrsFileName();
                        document.save(fPath);
                        Attachment att = new Attachment(fPath);
                        string fname = txtNaslov.Text + ".prs";
                        if (fname == ".prs") fname = "projekt.prs";
                        att.ContentDisposition.FileName = fname;
                        message.Attachments.Add(att);
                    }

                    //   ContentType content = data.ContentType;
                    //  content.MediaType = System.Net.Mime.MediaTypeNames.Text.Plain;

                    System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp.gmail.com");

                    smtp.Credentials = new NetworkCredential(txtUsername.Text , txtPassword.Text );
                    smtp.Port = 587;

                    int ip = 0;
                    if (int.TryParse(txtPort.Text, out ip)) smtp.Port = ip;

                    smtp.EnableSsl = true;

                  // smtp.UseDefaultCredentials = false;
                   
                    smtp.Timeout = (60 * 5 * 1000);
                   // smtp.Send(message);

                    smtp.SendCompleted += new System.Net.Mail.SendCompletedEventHandler(mailPoslan);
                    //smtp.SendAsync(message, message );
                    smtp.Send(message);
                    //MessageBox.Show("Mail je uspješno poslan!");

                   // message.Dispose();
                    //foreach (MemoryStream m in streams )
                    //{
                    //    m.Flush();
                    //    m.Dispose();
                    //}

                    msNacrt.Flush();
                    msNacrt.Dispose();
                    
                    //this.Close();


                }
                catch (Exception ex)
                {
                     
                    MessageBox.Show(ex.Message);
                    btnSend.Enabled = true;
                    lblSlanje.Visible = false;
                }
             
           

        }

        private void mailPoslan(object sender,AsyncCompletedEventArgs e)
        {
            
            lblSlanje.Visible = false;
            

            MessageBox.Show("Mail je uspješno poslan!");
            btnSend.Enabled = true;
           

        }


        private void frmMail_Load(object sender, EventArgs e)
        {
            lstContacts.Items.Clear();
            Contacts.Load();
            foreach(cPerson p in Contacts.Persons )
            {
                lstContacts.Items.Add(p.Name );
            }
        }

        private void CreateJpg()
        {


            /*too 
            ImageCodecInfo myImageCodecInfo;
           
            System.Drawing.Imaging. Encoder myEncoder;
            EncoderParameter myEncoderParameter;
            EncoderParameters myEncoderParameters;
            */
            // Create a Bitmap object based on a BMP file.

             
           //GetImage().s;
            string fPath = Application.StartupPath + @"\picx.png";

            // Get an ImageCodecInfo object that represents the JPEG codec.
           //too myImageCodecInfo =  GetEncoderInfo("image/jpeg");

            // Create an Encoder object based on the GUID 

            // for the Quality parameter category.
         //too   myEncoder =System.Drawing.Imaging. Encoder.Quality;

            // Create an EncoderParameters object. 

            // An EncoderParameters object has an array of EncoderParameter 

            // objects. In this case, there is only one 

            // EncoderParameter object in the array.
        //too    myEncoderParameters = new EncoderParameters(1);

       /*     // Save the bitmap as a JPEG file with quality level 25.
            myEncoderParameter = new EncoderParameter(myEncoder, 25L);
            myEncoderParameters.Param[0] = myEncoderParameter;
            myBitmap.Save("Shapes025.jpg", myImageCodecInfo, myEncoderParameters);

            // Save the bitmap as a JPEG file with quality level 50.
            myEncoderParameter = new EncoderParameter(myEncoder, 50L);
            myEncoderParameters.Param[0] = myEncoderParameter;
            myBitmap.Save("Shapes050.jpg", myImageCodecInfo, myEncoderParameters);
            */
            // Save the bitmap as a JPEG file with quality level 75.

            
           //tooo myEncoderParameter = new EncoderParameter(myEncoder,1500L);
           //tooo myEncoderParameters.Param[0] = myEncoderParameter;
           // myBitmap.Save(fPath, myImageCodecInfo, myEncoderParameters);
            Bitmap b = new Bitmap(100, 100);

             

              b=  (Bitmap )  GetImage().Clone();

            b.Save(fPath, System.Drawing.Imaging.ImageFormat.Png);
            b.Dispose();
          
        }

        private ImageCodecInfo GetEncoderInfo(string mimeType)
        {
            
            ImageCodecInfo[] encoders;
            encoders=System.Drawing.Imaging.ImageCodecInfo.GetImageDecoders ();
            
            foreach(ImageCodecInfo cod in encoders ){
                if(cod.MimeType==mimeType ){
                     return cod;
                }
                
            }
            return null;
        }

        private void btnAddContact_Click(object sender, EventArgs e)
        {
            frmAddContact frm = new frmAddContact();
            if (frm.ShowDialog() != System.Windows.Forms.DialogResult.Cancel)
            {
                lstContacts.Items.Add(frm.txtName.Text);
                Contacts.Add(frm.txtName.Text, frm.txtMail.Text);
                Contacts.Save();
            }
            frm.Dispose();

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void btnDeleteContact_Click(object sender, EventArgs e)
        {
            if (lstContacts.SelectedItems.Count == 0) { return; }
            int i = lstContacts.SelectedIndex;
            Contacts.Persons.RemoveAt(i);
            lstContacts.Items.RemoveAt(i);
            Contacts.Save();

        }

        private string GetPngFileName()
        {
            int i=0;
            string fpath = settings.directorySent + @"\pic" + i.ToString() + ".png";

            while (File.Exists (fpath ))            
            {
                i++;
                fpath = settings.directorySent + @"\pic" + i.ToString() + ".png";
            }
            return fpath ;
        }

        private string GetPrsFileName()
        {
            int i = 0;
            string fpath = settings.directorySent + @"\projekt" + i.ToString() + ".prs";

            while (File.Exists(fpath))
            {
                i++;
                fpath = settings.directorySent + @"\projekt" + i.ToString() + ".prs";
            }
            return fpath;
        }

        private void btnEditContact_Click(object sender, EventArgs e)
        {
            frmAddContact frm = new frmAddContact();
            int ind = lstContacts.SelectedIndex;
            if (ind == -1) return;

            frm.txtMail.Text  = Contacts.Persons[ind].Mail;
            frm.txtName.Text  = Contacts.Persons[ind].Name;

            if (frm.ShowDialog() != System.Windows.Forms.DialogResult.Cancel)
            {
                lstContacts.Items[ind] = frm.txtName.Text;
                Contacts.Persons[ind].Mail  = frm.txtMail.Text;
                Contacts.Persons[ind].Name   = frm.txtName.Text;

                Contacts.Save();
            }
            frm.Dispose();
        }

        OpenFileDialog fo = new OpenFileDialog();
        private void btnDodaj_Click(object sender, EventArgs e)
        { 
            fo.Multiselect = true;
            if (fo.ShowDialog() == System.Windows.Forms.DialogResult.Cancel) return;
            

            foreach (string s in fo.FileNames)
                lstDodaci.Items.Add(s);




        }

        private void btnDodaciBrisi_Click(object sender, EventArgs e)
        {
            if (lstDodaci.SelectedIndex == -1) return;
            lstDodaci.Items.RemoveAt(lstDodaci.SelectedIndex);

        }

        
        private void lstContacts_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
