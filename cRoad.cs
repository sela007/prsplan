﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing ;
namespace PRSPLAN
{

    [Serializable]
    public class cRoad
    {
        public bool selected = false;

        public  List<cPoint> points = new List<cPoint>();
        public int width = 90;
        public void drawon(Bitmap b,Graphics g)
        {

            Point[] ps = GetPoints();
            if (points.Count < 2) { return; }
           // using (Graphics g = Graphics.FromImage(b))
           // {
                Pen lp = new Pen(new SolidBrush(MyColors.road));
                lp.Width = width;
                g.DrawLines(lp, ps);
                //g.DrawCurve(lp, points.ToArray());

                Pen cp = new Pen(new SolidBrush(Color.DarkGray));
                int i = 0;
                foreach (Point p in ps)
                {
                    i++;
                    if (i != 1) { g.DrawEllipse(cp, p.X - 3, p.Y - 3, 6, 6); }
                    //g.FillEllipse( new SolidBrush(Color.White) ,p.X-3,p.Y-3,6,6);
                }
           // }
        }

        private Point[] GetPoints() 
        {
            Point[] ps = new Point[ points.Count  ];
            int i = -1;
            foreach (cPoint p in points) 
            {
                i++;
                ps[i] = new Point(p.X, p.Y);
            }

            return ps;
        }

    }
}
