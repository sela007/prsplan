﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing ;

namespace PRSPLAN
{
    [Serializable]
    public  class cLine
    {
        public int x1 = 0;
        public int y1 = 0;
        public int x2 = 0;
        public int y2 = 0;
        public int width = 2;
        public bool selected = false;

        public cPoint pointref1 = new cPoint(0, 0);
        public Point point1
        {
            get { return new Point(pointref1.X, pointref1.Y); }
            set { pointref1.X = value.X; pointref1.Y = value.Y; }
        }

        public cPoint pointref2 = new cPoint(0, 0);
        public Point point2
        {
            get { return new Point(pointref2.X, pointref2.Y); }
            set { pointref2.X = value.X; pointref2.Y = value.Y; }
        }

        public Color color;
        public System.Drawing.Drawing2D.DashStyle dashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
  
        public cLine(Point p1,Point p2)
        {
            point1 = p1;
            point2 = p2;
            color = Color.Black;
        }
        public cLine(Point p1, Point p2,Color c)
        {
            point1 = p1;
            point2 = p2;
            color = c;
        }

          public void drawon(Bitmap b,Graphics g) 
        {

           // using (Graphics g = Graphics.FromImage(b))
            //{
                Pen lp = new Pen(new SolidBrush(color));
                lp.Width = width;
                lp.DashStyle = dashStyle;
                g.DrawLine(lp, point1, point2);
            //}


        }
    }
}
