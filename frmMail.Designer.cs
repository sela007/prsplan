﻿namespace PRSPLAN
{
    partial class frmMail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMail));
            this.txtText = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lstDodaci = new System.Windows.Forms.ListBox();
            this.btnDodaciBrisi = new System.Windows.Forms.Button();
            this.btnDodaj = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNaslov = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lstContacts = new System.Windows.Forms.ListBox();
            this.btnEditContact = new System.Windows.Forms.Button();
            this.btnDeleteContact = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnAddContact = new System.Windows.Forms.Button();
            this.btnSend = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblSlanje = new System.Windows.Forms.Label();
            this.txtSender = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.txtServer = new System.Windows.Forms.ComboBox();
            this.txtPort = new System.Windows.Forms.ComboBox();
            this.chSlike = new System.Windows.Forms.CheckBox();
            this.chNacrt = new System.Windows.Forms.CheckBox();
            this.chProjekt = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtText
            // 
            this.txtText.Location = new System.Drawing.Point(6, 96);
            this.txtText.Multiline = true;
            this.txtText.Name = "txtText";
            this.txtText.Size = new System.Drawing.Size(356, 128);
            this.txtText.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 203);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tekst";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chSlike);
            this.groupBox1.Controls.Add(this.txtText);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.lstDodaci);
            this.groupBox1.Controls.Add(this.btnDodaciBrisi);
            this.groupBox1.Controls.Add(this.btnDodaj);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtNaslov);
            this.groupBox1.Controls.Add(this.chNacrt);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.chProjekt);
            this.groupBox1.Location = new System.Drawing.Point(170, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(368, 235);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pošalji";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(63, 112);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Dodaci";
            // 
            // lstDodaci
            // 
            this.lstDodaci.FormattingEnabled = true;
            this.lstDodaci.Location = new System.Drawing.Point(66, 128);
            this.lstDodaci.Name = "lstDodaci";
            this.lstDodaci.Size = new System.Drawing.Size(296, 56);
            this.lstDodaci.TabIndex = 7;
            // 
            // btnDodaciBrisi
            // 
            this.btnDodaciBrisi.Location = new System.Drawing.Point(10, 155);
            this.btnDodaciBrisi.Name = "btnDodaciBrisi";
            this.btnDodaciBrisi.Size = new System.Drawing.Size(50, 23);
            this.btnDodaciBrisi.TabIndex = 6;
            this.btnDodaciBrisi.Text = "Briši";
            this.btnDodaciBrisi.UseVisualStyleBackColor = true;
            this.btnDodaciBrisi.Click += new System.EventHandler(this.btnDodaciBrisi_Click);
            // 
            // btnDodaj
            // 
            this.btnDodaj.Location = new System.Drawing.Point(10, 128);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(50, 23);
            this.btnDodaj.TabIndex = 6;
            this.btnDodaj.Text = "Dodaj";
            this.btnDodaj.UseVisualStyleBackColor = true;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Naslov";
            // 
            // txtNaslov
            // 
            this.txtNaslov.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtNaslov.Location = new System.Drawing.Point(66, 68);
            this.txtNaslov.Name = "txtNaslov";
            this.txtNaslov.Size = new System.Drawing.Size(296, 22);
            this.txtNaslov.TabIndex = 4;
            this.txtNaslov.Text = "bez naslova";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Mail";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lstContacts);
            this.groupBox2.Controls.Add(this.btnEditContact);
            this.groupBox2.Controls.Add(this.btnDeleteContact);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.btnAddContact);
            this.groupBox2.Location = new System.Drawing.Point(6, 9);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(158, 235);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Primatelj";
            // 
            // lstContacts
            // 
            this.lstContacts.FormattingEnabled = true;
            this.lstContacts.Location = new System.Drawing.Point(6, 51);
            this.lstContacts.Name = "lstContacts";
            this.lstContacts.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lstContacts.Size = new System.Drawing.Size(141, 173);
            this.lstContacts.TabIndex = 9;
            this.lstContacts.SelectedIndexChanged += new System.EventHandler(this.lstContacts_SelectedIndexChanged);
            // 
            // btnEditContact
            // 
            this.btnEditContact.Location = new System.Drawing.Point(97, 19);
            this.btnEditContact.Name = "btnEditContact";
            this.btnEditContact.Size = new System.Drawing.Size(50, 23);
            this.btnEditContact.TabIndex = 7;
            this.btnEditContact.Text = "Uredi";
            this.btnEditContact.UseVisualStyleBackColor = true;
            this.btnEditContact.Click += new System.EventHandler(this.btnEditContact_Click);
            // 
            // btnDeleteContact
            // 
            this.btnDeleteContact.Location = new System.Drawing.Point(52, 19);
            this.btnDeleteContact.Name = "btnDeleteContact";
            this.btnDeleteContact.Size = new System.Drawing.Size(39, 23);
            this.btnDeleteContact.TabIndex = 1;
            this.btnDeleteContact.Text = "Briši";
            this.btnDeleteContact.UseVisualStyleBackColor = true;
            this.btnDeleteContact.Click += new System.EventHandler(this.btnDeleteContact_Click);
            // 
            // button1
            // 
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(57, 19);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(34, 23);
            this.button1.TabIndex = 1;
            this.button1.UseVisualStyleBackColor = true;
            // 
            // btnAddContact
            // 
            this.btnAddContact.Location = new System.Drawing.Point(6, 19);
            this.btnAddContact.Name = "btnAddContact";
            this.btnAddContact.Size = new System.Drawing.Size(45, 23);
            this.btnAddContact.TabIndex = 1;
            this.btnAddContact.Text = "Dodaj";
            this.btnAddContact.UseVisualStyleBackColor = true;
            this.btnAddContact.Click += new System.EventHandler(this.btnAddContact_Click);
            // 
            // btnSend
            // 
            this.btnSend.Image = ((System.Drawing.Image)(resources.GetObject("btnSend.Image")));
            this.btnSend.Location = new System.Drawing.Point(319, 396);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(112, 38);
            this.btnSend.TabIndex = 6;
            this.btnSend.Text = "Pošalji";
            this.btnSend.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.Location = new System.Drawing.Point(437, 396);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(101, 38);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Zatvori";
            this.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Smtp server";
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(253, 75);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(26, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Port";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtSender);
            this.groupBox3.Controls.Add(this.txtPassword);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.txtUsername);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.txtServer);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.txtPort);
            this.groupBox3.Location = new System.Drawing.Point(6, 252);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(525, 138);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Pošiljatelj";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(226, 49);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Password";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(25, 49);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "Username";
            // 
            // lblSlanje
            // 
            this.lblSlanje.AutoSize = true;
            this.lblSlanje.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblSlanje.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblSlanje.Location = new System.Drawing.Point(8, 404);
            this.lblSlanje.Name = "lblSlanje";
            this.lblSlanje.Size = new System.Drawing.Size(142, 20);
            this.lblSlanje.TabIndex = 11;
            this.lblSlanje.Text = "Slanje u tijeku....";
            this.lblSlanje.Visible = false;
            // 
            // txtSender
            // 
            this.txtSender.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::PRSPLAN.Properties.Settings.Default, "mailPosiljatelj", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txtSender.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtSender.Location = new System.Drawing.Point(84, 19);
            this.txtSender.Name = "txtSender";
            this.txtSender.Size = new System.Drawing.Size(328, 22);
            this.txtSender.TabIndex = 4;
            this.txtSender.Text = global::PRSPLAN.Properties.Settings.Default.mailPosiljatelj;
            // 
            // txtPassword
            // 
            this.txtPassword.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::PRSPLAN.Properties.Settings.Default, "mailPassword", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txtPassword.Location = new System.Drawing.Point(285, 46);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(127, 20);
            this.txtPassword.TabIndex = 9;
            this.txtPassword.Text = global::PRSPLAN.Properties.Settings.Default.mailPassword;
            // 
            // txtUsername
            // 
            this.txtUsername.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::PRSPLAN.Properties.Settings.Default, "mailUsername", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txtUsername.Location = new System.Drawing.Point(84, 46);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(127, 20);
            this.txtUsername.TabIndex = 9;
            this.txtUsername.Text = global::PRSPLAN.Properties.Settings.Default.mailUsername;
            // 
            // txtServer
            // 
            this.txtServer.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::PRSPLAN.Properties.Settings.Default, "smtpServer", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txtServer.FormattingEnabled = true;
            this.txtServer.Items.AddRange(new object[] {
            "smtp.gmail.com",
            "mail.htnet.hr",
            "smtp.t-com.hr",
            "mail.iskon.hr",
            "mail.vip.hr",
            "mail.optinet.hr"});
            this.txtServer.Location = new System.Drawing.Point(84, 72);
            this.txtServer.Name = "txtServer";
            this.txtServer.Size = new System.Drawing.Size(127, 21);
            this.txtServer.TabIndex = 7;
            this.txtServer.Text = global::PRSPLAN.Properties.Settings.Default.smtpServer;
            // 
            // txtPort
            // 
            this.txtPort.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::PRSPLAN.Properties.Settings.Default, "mailPort", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txtPort.FormattingEnabled = true;
            this.txtPort.Location = new System.Drawing.Point(285, 72);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(127, 21);
            this.txtPort.TabIndex = 7;
            this.txtPort.Text = global::PRSPLAN.Properties.Settings.Default.mailPort;
            // 
            // chSlike
            // 
            this.chSlike.AutoSize = true;
            this.chSlike.Checked = global::PRSPLAN.Properties.Settings.Default.chSendSlike;
            this.chSlike.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chSlike.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::PRSPLAN.Properties.Settings.Default, "chSendSlike", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.chSlike.Location = new System.Drawing.Point(66, 19);
            this.chSlike.Name = "chSlike";
            this.chSlike.Size = new System.Drawing.Size(49, 17);
            this.chSlike.TabIndex = 9;
            this.chSlike.Text = "Slike";
            this.chSlike.UseVisualStyleBackColor = true;
            // 
            // chNacrt
            // 
            this.chNacrt.AutoSize = true;
            this.chNacrt.Checked = global::PRSPLAN.Properties.Settings.Default.chSendImage;
            this.chNacrt.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chNacrt.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::PRSPLAN.Properties.Settings.Default, "chSendImage", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.chNacrt.Location = new System.Drawing.Point(9, 19);
            this.chNacrt.Name = "chNacrt";
            this.chNacrt.Size = new System.Drawing.Size(52, 17);
            this.chNacrt.TabIndex = 2;
            this.chNacrt.Text = "Nacrt";
            this.chNacrt.UseVisualStyleBackColor = true;
            // 
            // chProjekt
            // 
            this.chProjekt.AutoSize = true;
            this.chProjekt.Checked = global::PRSPLAN.Properties.Settings.Default.chSendProjekt;
            this.chProjekt.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::PRSPLAN.Properties.Settings.Default, "chSendProjekt", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.chProjekt.Location = new System.Drawing.Point(121, 19);
            this.chProjekt.Name = "chProjekt";
            this.chProjekt.Size = new System.Drawing.Size(59, 17);
            this.chProjekt.TabIndex = 2;
            this.chProjekt.Text = "Projekt";
            this.chProjekt.UseVisualStyleBackColor = true;
            this.chProjekt.Visible = false;
            // 
            // frmMail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(540, 439);
            this.Controls.Add(this.lblSlanje);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmMail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Slanje pošte";
            this.Load += new System.EventHandler(this.frmMail_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chNacrt;
        private System.Windows.Forms.CheckBox chProjekt;
        private System.Windows.Forms.TextBox txtText;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnDeleteContact;
        private System.Windows.Forms.Button btnAddContact;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSender;
        private System.Windows.Forms.TextBox txtNaslov;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnEditContact;
        private System.Windows.Forms.ComboBox txtServer;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox lstContacts;
        private System.Windows.Forms.Button btnDodaj;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListBox lstDodaci;
        private System.Windows.Forms.Button btnDodaciBrisi;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.CheckBox chSlike;
        private System.Windows.Forms.ComboBox txtPort;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblSlanje;
    }
}