﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PRSPLAN
{
    public partial class DokSize : Form
    {
        public DokSize()
        {
            InitializeComponent();
        }

        public int docWidth = 21;
        public int docHeight = 29;

        private void btnOK_Click(object sender, EventArgs e)
        {
            docWidth = Convert.ToInt16( txtWidth.Text);
            docHeight = Convert.ToInt16(txtHeight.Text);
            DialogResult = System.Windows.Forms.DialogResult.OK;

        }
    }
}
