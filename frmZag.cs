﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PRSPLAN
{
    public partial class frmZag : Form
    {
       cZaglavlje zaglavlje;
 
        public frmZag()
        {
            InitializeComponent();
            
        }

        public frmZag(cZaglavlje zag)
        {
            InitializeComponent();
            zaglavlje = zag;
        }
        
        private void frmZag_Load(object sender, EventArgs e)
        {
         if(zaglavlje.items.Count > 0){T1.Text=zaglavlje.items[0].title ;}
         if(zaglavlje.items.Count > 1){T2.Text=zaglavlje.items[1].title ;}
         if(zaglavlje.items.Count > 2){T3.Text=zaglavlje.items[2].title ;}
         if(zaglavlje.items.Count > 3){T4.Text=zaglavlje.items[3].title ;}
         if(zaglavlje.items.Count > 4){T5.Text=zaglavlje.items[4].title ;}
         if(zaglavlje.items.Count > 5){T6.Text=zaglavlje.items[5].title ;}
         if(zaglavlje.items.Count > 6){T7.Text=zaglavlje.items[6].title ;}

         if(zaglavlje.items.Count > 0){TXT1.Text=zaglavlje.items[0].text ;}
         if(zaglavlje.items.Count > 1){TXT2.Text=zaglavlje.items[1].text ;}
         if(zaglavlje.items.Count > 2){TXT3.Text=zaglavlje.items[2].text ;}
         if(zaglavlje.items.Count > 3){TXT4.Text=zaglavlje.items[3].text ;}
         if(zaglavlje.items.Count > 4){TXT5.Text=zaglavlje.items[4].text ;}
         if(zaglavlje.items.Count > 5){TXT6.Text=zaglavlje.items[5].text ;}
         if(zaglavlje.items.Count > 6){TXT7.Text=zaglavlje.items[6].text ;}
  
        if(TXT4.Text.Trim()=="")
        {TXT4.Text = DateTime.Now.ToString("dd.MM.yyyy");}
        
         }
        
        
        delegate void TwoPar(TextBox t1,TextBox t2);
 

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void btnOK_Click_1(object sender, EventArgs e)
        {
            int res = 0;

            if (TXT1.Text != "")
            {
                if (Int32.TryParse(TXT1.Text, out res) == false | TXT1.Text.Length != 8)
                {
                    MessageBox.Show("Pogrešan unos (WWMS)", "");
                    return;
                }
            }

            zaglavlje.items.Clear();

            TwoPar CheckItemss = (it1, it2) =>
            {
                if (it1.Text.Trim() != "" )
                {
                    cZaglavljeItem cz = new cZaglavljeItem();
                    cz.text = it2.Text;
                    cz.title = it1.Text;
                    zaglavlje.items.Add(cz);
                }
            };


            CheckItemss(T1, TXT1);
            CheckItemss(T2, TXT2);
            CheckItemss(T3, TXT3);
            CheckItemss(T4, TXT4);
            CheckItemss(T5, TXT5);
            CheckItemss(T6, TXT6);
            CheckItemss(T7, TXT7);
            this.Close();
        }

        private void frmZag_Shown(object sender, EventArgs e)
        {
            TXT1.Focus();
            TXT1.SelectAll();
        }

        
    }
}
