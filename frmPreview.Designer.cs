﻿namespace PRSPLAN
{
    partial class frmPreview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PPC1 = new System.Windows.Forms.PrintPreviewControl();
            this.PD1 = new System.Windows.Forms.PrintDialog();
            this.btnPrint = new System.Windows.Forms.Button();
            this.lv1 = new System.Windows.Forms.ListView();
            this.SuspendLayout();
            // 
            // PPC1
            // 
            this.PPC1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.PPC1.Location = new System.Drawing.Point(2, 1);
            this.PPC1.Name = "PPC1";
            this.PPC1.Size = new System.Drawing.Size(462, 291);
            this.PPC1.TabIndex = 0;
            // 
            // PD1
            // 
            this.PD1.UseEXDialog = true;
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPrint.Location = new System.Drawing.Point(12, 315);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 23);
            this.btnPrint.TabIndex = 1;
            this.btnPrint.Text = "print";
            this.btnPrint.UseVisualStyleBackColor = true;
            // 
            // lv1
            // 
            this.lv1.Location = new System.Drawing.Point(206, 316);
            this.lv1.Name = "lv1";
            this.lv1.Size = new System.Drawing.Size(54, 34);
            this.lv1.TabIndex = 2;
            this.lv1.UseCompatibleStateImageBehavior = false;
            // 
            // frmPreview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(465, 350);
            this.Controls.Add(this.lv1);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.PPC1);
            this.Name = "frmPreview";
            this.Text = "frmPreview";
            this.Load += new System.EventHandler(this.frmPreview_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PrintPreviewControl PPC1;
        private System.Windows.Forms.PrintDialog PD1;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.ListView lv1;
    }
}