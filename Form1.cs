﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Drawing.Printing;

namespace PRSPLAN
{
    public partial class Form1 : Form
    {
        private byte mill = 3;
        private Bitmap picscale = new Bitmap(4 * 10, 4 * 10);
        private List<object> elements = new List<object>();
        private bool ctrlIsPressed = false;

        private int _pointerx = 0;
        private int  pointerx
        {
            get { return _pointerx; } 
            set {if (value !=_pointerx )
            {
                _pointerx = value;
                //GetHoveredObjects();
                //processPosition();
            } } }
        private int  _pointery = 0;
        private int pointery
        {
            get { return _pointery; }
            set
            {
                if (value != _pointery)
                {
                    _pointery = value;
                    //GetHoveredObjects();
                    //processPosition();
                }
            }
        }

        private Point  cpoint1 =new Point(0,0);
        private Point cpoint2 = new Point(0, 0);
        private Bitmap bmp;
        private Bitmap bmpFocus;
        private Pen cpn = new Pen(new SolidBrush(Color.FromArgb(100, 100, 100)));
        private Pen lpn = new Pen(new SolidBrush(Color.FromArgb(210, 210, 210)));


        public cDocument document = new cDocument(new Size(25, 16));

        private List<cTask> tasklist = new List<cTask>();

        private List<cPoint> selectedPoints = null;
        private List<object> connectedSelectedObjects = new List<object>();

         
        //public object hObject;
        public Point sPoint;
        public cSelector selector = new cSelector();


        public Form1(string filename)
        {
            InitializeComponent();
            setHandlers();
            drawscale();
            OpenDocument(filename);
            
             
            currentTool = eTools.Select;
        }
        public Form1()
        {
            InitializeComponent();
            setHandlers();
        
        }

        private void setHandlers() 
        {
            this.btnNoviKabel.Click += new System.EventHandler(this.btnLine_Click);
            this.btnDemontiraniK.Click += new System.EventHandler(this.btnLine_Click);
            this.btnPostojeciK.Click += new System.EventHandler(this.btnLine_Click);
            this.btnRec.Click += new System.EventHandler(this.btnLine_Click);
            this.btnIzvod.Click += new System.EventHandler(this.btnLine_Click);
            this.btnEraser.Click += new System.EventHandler(this.btnLine_Click);
            this.btnSelect.Click += new System.EventHandler(this.btnLine_Click);
            this.btnRoad.Click += new System.EventHandler(this.btnLine_Click);
            this.btnPKDemontirani.Click += new System.EventHandler(this.btnLine_Click);
            this.btnPKNovi.Click += new System.EventHandler(this.btnLine_Click);
            this.btnPKPostojeci.Click += new System.EventHandler(this.btnLine_Click);
            this.btnEStup.Click += new System.EventHandler(this.btnLine_Click);
            this.btnStup.Click += new System.EventHandler(this.btnLine_Click);
            this.btnKutija.Click += new System.EventHandler(this.btnLine_Click);
            this.btnStupNovi.Click += new System.EventHandler(this.btnLine_Click);
            this.btnText.Click += new System.EventHandler(this.btnLine_Click);
            
        }

        private boxString _currentRoadText;
        private boxString currentRoadText{
         get{return _currentRoadText ;}

            set{_currentRoadText=value;
            if (value != null) { texttext.Visible = true; texttext.Text = _currentRoadText.value;  texttext.Focus(); texttext.SelectAll(); }
             
            }
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            //Properties.Resources.PRSICTLOGO.Save("logo.png");

            FazeRada.label1.Text = "Faze rada";
            Materijal.label1.Text = "Utrošeni materijal";
            DemMaterijal.label1.Text = "Demontirani materijal";

            FazeRada.ucitajCvs("fr.txt");
            DemMaterijal.ucitajCvs("mat.txt");
            Materijal.ucitajCvs("mat.txt");
            drawscale();
            documentInitialize();
            Slike = new cSlike(document);
            currentTool = eTools.Select;
            textsize.Text = "8";
            roadCbSize.Text = "40";

           
           
        }
        private string getHtmlTable(DataGridView dg,string naslov)
        {
            string txt = "<table border='1' cellspacing='0'>" + Environment.NewLine ;
            txt += @"<caption border='1' cellspacing='0'>" + naslov + @"</caption>
                    <tr bgcolor='FFCC66'>
                    <th width='90'>ŠIFRA</th>
                    <th width='70'>KOM</th>
                    <th width='400'>OPIS</th>";

            bool isFirstRow = true;
            string hColor1 = "FFFFCC";
            string hColor2 = "FFFFCC";
            string cColor=hColor1;
            bool dalje = false;
            foreach (DataGridViewRow dr in dg.Rows)
            {

                //if (isFirstRow) { cColor = hColor1; isFirstRow = false; } else { cColor = hColor2; isFirstRow = true; }
                txt += "<tr bgcolor='" + cColor + "'>" + Environment.NewLine ;
              

                if (dr.Cells[0].Value != null & dr.Cells[3].Value != null & dr.Cells[1].Value != null & dr.Cells[2].Value!=null)
                {
                    if (dr.Cells[3].Value.ToString() != "")
                    {
                        dalje = true;
                        txt += "<td>" + dr.Cells[0].Value.ToString() + "</td>" + Environment.NewLine;
                        txt += "<td>" + dr.Cells[3].Value.ToString() + " " + dr.Cells[2].Value.ToString() + "</td>" + Environment.NewLine;
                        txt += "<td>" + dr.Cells[1].Value.ToString() + "</td>" + Environment.NewLine;
                        txt += "</tr>" + Environment.NewLine;
                    }
                }
            }

            if (dalje == false) return "";
            txt+="</table><br>";
            return txt;
        }


        private string getHtmlDoc()
        {
            string txt = @"<!DOCTYPE html><meta http-equiv='content-type' content='text/html;charset=utf-8' /><html><head><style>table{border-collapse:collapse;}table, td, th{border:1px solid black;}</style></head><body>";

            foreach (cZaglavljeItem i in document.zaglavlje.items)
            {
                if (i.title.Trim() != "")
                    txt += i.text + "<br>";
            }

            txt += getHtmlTable(FazeRada.dg1, "FAZE RADA");
            txt += getHtmlTable(Materijal.dg1, "UTROŠENI MATERIJAL");
            txt += getHtmlTable(DemMaterijal.dg1, "DEMONTIRANI MATERIJAL");
            txt += "</body>";
            return txt;

        }

        private void setPaperSize(Size s) 
        { 
         Graphics g = this.CreateGraphics();
         plan.Width = (int)(g.DpiX / 2.54 * s.Height );
         plan.Height = (int)(g.DpiX / 2.54 * s.Width );
         g.Dispose();
              
        }
        private void drawscale()
        {
            picscale = new Bitmap(document.paperSizePix.Height , document.paperSizePix.Width );
            Graphics g = Graphics.FromImage(picscale );
            Pen pnlight=new Pen(new SolidBrush(MyColors.net  ));
            Pen pndark = new Pen(new SolidBrush(MyColors.net10 ));
            pnlight.DashStyle = System.Drawing.Drawing2D.DashStyle.Solid ;
            pndark.DashStyle = System.Drawing.Drawing2D.DashStyle.Solid ;
            Point pointx = new Point(0, 0);
            Point pointy = new Point(0, 0);
            if (plan.BackColor == Color.Black) { pnlight.Color = Color.FromArgb(20, 20, 20); }
           else{pnlight.Color=Color.FromArgb(250,250,250);}
            g.FillRectangle(new SolidBrush(plan.BackColor ) ,new Rectangle(0,0,picscale.Width,picscale.Height ));

            for(int x=0;x<=picscale.Width;x+=mill)
            {
                    g.DrawLine(pnlight, new Point(x, 0), new Point(x, picscale .Height));
                    //g.DrawLine(pnlight, new Point(0, x), new Point(picscale .Width , x ));
            }

            for (int y = 0; y <= picscale.Height ; y += mill )
            {
               //g.DrawLine(pndark, new Point(x, 0), new Point(x, picscale  .Height));
               g.DrawLine(pnlight, new Point(0,y), new Point(picscale.Width , y));
                 
                //g.DrawLine(pndark, new Point(0, x), new Point(picscale.Width, x));
            }
             
            g.Dispose();
        }

        private Color currentColor = Color.Black;

         
        private void plan_MouseMove(object sender, MouseEventArgs e)
        {
            int ey = e.Y  > -1 ? e.Y  : 0;
            int ex = e.X  > -1 ? e.X  : 0;
             
            setPointers(Convert.ToInt16(ex), Convert.ToInt16(ey));
             

            Color c=Color.Black ;
            if (pointerx > document.codeImage.Width) pointerx = document.codeImage.Width-1;
            if (pointery > document.codeImage.Height ) pointery = document.codeImage.Height-1 ;

            if (pointerx < 0) pointerx = 0;
            if (pointery < 0) pointery = 0;

            if (document.codeImage != null)

               c = document.codeImage.GetPixel(pointerx  , pointery);

                /*
            block = new BitmapLock(document.codeImage);
            block.LockBits();
            c=block.GetPixel(pointerx,pointery );
            block.UnlockBits();
                */
         

            //this.Text = c.ToString();

            currentColor = c;
             if (radnja == eRadnja.Moving  )
             {
                 selectedPointsMove(CurrentPoint());
                 //document.UpdateImage();
                
                 
             }

            if (radnja == eRadnja.MoveAll )
             {
                 selectedPointsMove(CurrentPoint());
                 //document.UpdateImage();
                
                 plan.BackgroundImage = document.getImage();
                  
             }

           

             using (Graphics g = Graphics.FromImage(bmpFocus)){
             g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
             g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
             g.Clear(Color.Transparent);
             
             if (radnja == eRadnja.Drawing)
                {
                    if (SelectedObject != null)
                    {
                        g.Clear(Color.Transparent);
 
                        eTools tool = gettype(SelectedObject) ;

                        if (tool == eTools.Rec)
                        {
                            ((cRec)SelectedObject).point3 = CurrentPoint();
                            ((cRec)SelectedObject).drawon(bmpFocus, g);
                        }
                        if (tool == eTools.Line)
                        {
                            ((cLine)SelectedObject).point2 = CurrentPoint();
                            ((cLine)SelectedObject).drawon(bmpFocus, g);
                        }
                        if (tool == eTools.LineText)
                        {
                            ((cLineText)SelectedObject).point2 = CurrentPoint();
                            ((cLineText)SelectedObject).drawon(bmpFocus, g);
                        }
                        if (tool == eTools.Road)
                        {
                            int i=((cRoad)SelectedObject).points.Count-1;

                            if (cRoadPoint != null)
                            {
                                cRoadPoint.X = pointerx;
                                cRoadPoint.Y = pointery;

                            }

                            ((cRoad)SelectedObject).drawon(bmpFocus, g);
                        }
                    }
                }

              drawfocus(bmpFocus);
             
          } 
        }

       

        private void refreshImage()
        {

            bmp = new Bitmap(plan.Width, plan.Height);
           
            //document.drawon (bmp);

           drawfocus(bmp);
           plan.Image  = bmp;

        }
        private void setPointers(Int16  x,Int16 y)
        {
            pointerx = Convert.ToInt16(x - (x % mill));
            pointery = Convert.ToInt16(y - (y % mill));
        }

        private Point _CurrentPoint;
        private Point CurrentPoint() 
        {
            _CurrentPoint.X = pointerx;
            _CurrentPoint.Y = pointery;
            return _CurrentPoint;
        }

        private void drawfocus(Bitmap b)
        {
           
            //Cursor.Hide();
            using (Graphics g = Graphics.FromImage(bmpFocus))
            {

                   Rectangle rec= new Rectangle(pointerx - (settings.SymbolSize.Width  / 2), pointery - (settings.SymbolSize.Height / 2),settings.SymbolSize.Width , settings.SymbolSize.Height );
                  if (connectedSelectedObjects.Count > 0)
                  {
                      foreach (object o in connectedSelectedObjects)
                          drawObject(o, bmpFocus);
                  }
                  else drawObject(SelectedObject, bmpFocus);


                switch (currentTool)
                {

                    case eTools.Izvod:
                        rec = new Rectangle(pointerx - (settings .IzvodSize.Width  / 2), pointery - (settings.IzvodSize.Height / 2),settings.IzvodSize.Width , settings.IzvodSize.Height );

                        g.DrawImage(Properties.Resources.izvod, rec);
                        break;
                    case eTools.Stup:
                        g.DrawImage(Properties.Resources.stup, rec);
                        break;
                    case eTools.StupNovi:
                        g.DrawImage(Properties.Resources.stupnovi, rec);
                        break;
                    case eTools.EStup:
                        g.DrawImage(Properties.Resources.EE, rec);
                        break;
                    case eTools.Kutija:
                        g.DrawImage(Properties.Resources.kutija, rec);
                        break;
                    case eTools.Text:
                        int txtHeight = TextRenderer.MeasureText(texttext.Text, textFont).Height;
                        g.DrawString(texttext.Text, textFont, new SolidBrush(textColor), CurrentPoint().X, CurrentPoint().Y - txtHeight);
                        break;
                    default:
                                SolidBrush  br = new SolidBrush(Color.LightGreen  );
                               
                                if (currentTool == eTools.Eraser) br.Color = Color.Red;

                                Pen p = new Pen(br);

                        if (currentColor.B == 6)
                        {
                             
                            Pen pen1 = new Pen(new SolidBrush(Color.Red));
                            pen1.Width = 2;
                            g.FillEllipse(br , new Rectangle(pointerx - 6, pointery - 6, 12, 12));
                            g.DrawEllipse(p, new Rectangle(pointerx - 9, pointery - 9, 18, 18));


                        }
                        else
                        {
                                   
                            if (currentColor.B != 0)
                            {
                                
                                g.FillEllipse(br   , new Rectangle(pointerx - 3, pointery - 3, 6, 6));
                                g.DrawEllipse(p, new Rectangle(pointerx - 9, pointery - 9, 18, 18));

                                //lp.EndCap  = System.Drawing.Drawing2D.LineCap.ArrowAnchor;
                               /*
                                int fw=7;
                                int fw2=4;
                                g.DrawLine(lp, new Point(pointerx - fw, pointery - fw), new Point(pointerx - fw2, pointery - fw2)); 
                                g.DrawLine(lp, new Point(pointerx + fw, pointery + fw), new Point(pointerx + fw2, pointery + fw2));
                                g.DrawLine(lp, new Point(pointerx + fw, pointery - fw), new Point(pointerx + fw2, pointery - fw2)); 
                                g.DrawLine(lp, new Point(pointerx - fw, pointery + fw), new Point(pointerx - fw2, pointery + fw2)); 
                           */
                            }

                        }
                        g.DrawEllipse(cpn, new Rectangle(pointerx - 3, pointery - 3, 6, 6));

                        break;
                }

       

                lpn.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
                g.DrawLine(lpn, new Point(pointerx, 0), new Point(pointerx, bmp.Height  ));
                g.DrawLine(lpn, new Point(0, pointery), new Point(bmp.Width , pointery));

                if (selector.enabled)
                {
                    selector.setRectangle(cpoint1, CurrentPoint());
                    selector.drawon(b, g);
                }

                
            }

          
            
            plan.Image = bmpFocus;
        }

        private List<object> SelectedObjectS = new List<object>();

        //current selected object
        private object _SelectedObject = null;
        private object SelectedObject
        {
            get { return _SelectedObject ; }
            set { _SelectedObject = value;

            if (value == null) { return; }
            if (value.GetType() == typeof(cRec)) { SetEditorTools(eTools.Rec ); texttext.Text = ((cRec)value).text.text1; texttext2.Text = ((cRec)value).text2.text1; }
            if (value.GetType() == typeof( cLine )) { 
                    SetEditorTools(eTools.Line);
                    texttext.Text = ((cLine)value).text1;
                    texttext2.Text = ((cLine)value).text2;
            }

            if (value.GetType() == typeof(cLineText)) 
            { SetEditorTools(eTools.LineText); 
                texttext.Text = ((cLineText)value).text1; 
                texttext2.Text = ((cLineText)value).text2;
                textsize.Text = ((cLineText)value).font.Size.ToString() ;
            }
            
                
            if (value.GetType() == typeof( cSymbol)) { SetEditorTools(eTools.Symbol); }
            if (value.GetType() == typeof(cText)) { SetEditorTools(eTools.Text); }
            if (value.GetType() == typeof(cRoad)) { SetEditorTools(eTools.Road ); roadCbSize.Text = ((cRoad)value).width.ToString(); }

            }
        }


        //private bool indrawing = false;

        private enum eRadnja {Drawing,Moving,MovingPoints,Selecting,Nothing,Delete,MoveAll};
        private eRadnja radnja=eRadnja.Nothing ;
        private enum eTools 
        {Nothing,Select, Line, LineText, Road, Rec,Symbol,Text,Eraser,Izvod,Stup,StupNovi,EStup,Kutija,Slike,KNovi,KPostojeci,KDemontirani,PKNovi,PKPostojeci,PKDemontirani }

        private eTools _currentTool;
        private eTools currentTool { 
            get { return _currentTool; }
            set
            {
                _currentTool = value;

                SetEditorTools(_currentTool);
            }
        }

        private void SetEditorTools(eTools tool)
        {
                    texttext.Visible = false;
                    texttext2.Visible = false;
                    textsize.Visible = false;
                    roadCbSize.Visible = false;
                    btnColor.Visible = false;
                    lbl1.Visible = false;
                    lbl2.Visible = false;
                    lbl3.Visible = false;
                    lbl4.Visible = false;


            switch (tool)
            {
                case eTools.Line:
                    lbl2.Visible = true;
                    texttext.Visible = true;
                    lbl3.Visible = true;
                    texttext2.Visible = true;
                    break;
                case eTools.LineText:
                    lbl2.Visible = true;
                    texttext.Visible = true;
                    lbl3.Visible = true;
                    texttext2.Visible = true;
                    lbl4.Visible = true;
                    textsize.Visible = true;
                    btnColor.Visible = true;
                    break;
                case eTools.Road:
                    lbl1.Visible = true;
                    roadCbSize.Visible = true;
                    if (currentRoadText != null) { texttext.Visible = true; texttext.Text = currentRoadText.value; texttext.Focus(); texttext.SelectAll(); }
                    break;
                case eTools.Rec:
                    lbl2.Visible = true;
                    texttext .Visible = true;
                    lbl3.Visible = true;
                    texttext2.Visible = true;
                    btnColor.Visible = true;
                    break;
              
                
            }

        }
        
        delegate void ThreeParam(Color c,int width,System.Drawing.Drawing2D.DashStyle ds);
        delegate void SymbParam(Bitmap b,Size ss);
        delegate void NoParam();
        delegate void delECI(elementCodeInfo i );

        private int KWidth = 1;
        private Point startPoint = new Point(0, 0);

        private cPoint  cRoadPoint=new cPoint(0,0);

      //  private cRec selector = new cRec(new Point(-1, -1), new Point (0, 0), MyColors.selector);

         
        //private cPoint SelectedPoint = null;

        //##############################################
        private void plan_MouseDown(object sender, MouseEventArgs e)
        {

             
            deselectAllObjects();
            viewUpdate();
            selectedPoints = null;cpoint1 = CurrentPoint();startPoint = CurrentPoint();
            SelectedObject = null;
            currentRoadText = null;

            if (cpoint1.X > document.paperSizePix.Height  - document.zaglavlje.width)
            {if (cpoint1.Y > document.paperSizePix.Width - document.zaglavlje.rowHeight * document.zaglavlje.items.Count)
                {btnZaglavlje.PerformClick();
                    return;}}
            
            delECI GetSelObj = (ei) =>
            {
                if (ei.elementType == 1) { SelectedObject = document.roads[ei.elementCode];
                currentRoadText = document.roads[ei.elementCode].GetNearestTextPoint(cpoint1   );
                }
                if (ei.elementType == 2) SelectedObject = document.recs[ei.elementCode];
                if (ei.elementType == 3) SelectedObject = document.lines[ei.elementCode];
                if (ei.elementType == 4) SelectedObject = document.symbols[ei.elementCode];
                if (ei.elementType == 5) SelectedObject = document.linetexts[ei.elementCode];
                if (ei.elementType == 6) SelectedObject = null;
            };

            elementCodeInfo eci = new elementCodeInfo();
            eci = MyColors.getCodeByColor(currentColor);
            //this.Text = eci.elementType.ToString() + "-" + eci.elementCode.ToString();
            
            switch (currentTool)
            {
                case eTools.Select:
                  
                  
                    selectedPoints = GetSelectedPoints();
                    GetSelObj(eci);
                    if ((Control.ModifierKeys) == Keys.Control)
                    {
                        if (SelectedObject != null)
                        {
                            using (var ms = new MemoryStream())
                            {
                                var formatter = new BinaryFormatter();
                                formatter.Serialize(ms,SelectedObject );
                                ms.Position = 0;

                                SelectedObject= formatter.Deserialize(ms);
                                setSelected(SelectedObject, true);
                                selectedPoints = GetPointsFromObject(SelectedObject);
                                //selectedPoints.AddRange(GetPointsOverObject(SelectedObject));
                                document.elementsAdd(SelectedObject);
                                radnja = eRadnja.Moving;
                                return;

                            }

                        }
                    }
                    setSelected(SelectedObject, true);
                    viewUpdate();
                    
                    if (SelectedObject != null)
                    {
                            connectedSelectedObjects.Clear();
                            connectedSelectedObjects.Add(SelectedObject);
                            
                            radnja = eRadnja.Moving;
                            selectedPoints = GetPointsFromObject(SelectedObject);
                            selectedPoints.AddRange(GetPointsOverObject(SelectedObject));
                            //elementsRemove(SelectedObject);
                            setSelected(SelectedObject, true);
                            viewUpdate();
                            drawfocus(bmpFocus);  
                    }
                    else
                    {
                        if (selectedPoints != null)
                        {
                            radnja = eRadnja.Moving;
                            //ako je simbol onda selektiraj na point selekt
                            if (gettype(SelectedObject) == eTools.Symbol) { setSelected(SelectedObject, true); }
                            viewUpdate();
                            drawfocus(bmpFocus);
                        }
                        else
                        {
                              selectedPoints = new List<cPoint>();
                              foreach (cRec r in document.recs) { selectedPoints.Add(r.pointref1); selectedPoints.Add(r.pointref3); }
                              foreach (cLine  r in document.lines ) { selectedPoints.Add(r.pointref1); selectedPoints.Add(r.pointref2); }
                              foreach (cLineText   r in document.linetexts ) { selectedPoints.Add(r.pointref1); selectedPoints.Add(r.pointref2); }
                              foreach (cSymbol r in document.symbols  ) { selectedPoints.Add(r.pointref); }
                              foreach (cRoad r in document.roads) { 
                                     foreach (cPoint p in r.points) { 
                                         selectedPoints.Add(p); };
                                      
                                 
                              }
                              radnja = eRadnja.MoveAll; 
                        }
                    }
                     
                    break;

                case eTools.Eraser:
                    GetSelObj(eci);
                    setSelected(SelectedObject, false);   
                    document.elementsRemove(SelectedObject);
                    document.updateCodeImage();
                    SelectedObject = null;
                    viewUpdate();
                    drawfocus(bmpFocus);
                    radnja = eRadnja.Delete;
                    break;

                default:

                    CreateObject(currentTool);
                    radnja = eRadnja.Drawing;
                    break;
            }

            
        }







        private void viewUpdate()
        {
            using (Graphics g = Graphics.FromImage(bmp))
            {

                g.DrawImage(picscale,new Point(0,0));
            }
            
            document.drawon(bmp);
            plan.BackgroundImage = bmp;
        }
     

       private void drawObject(object o,Bitmap b)
        {
            
           if (o == null){ return;};

           using (Graphics g=Graphics.FromImage(b))
           {
               eTools strType = gettype(o);
               if (strType == eTools.Rec) { ((cRec)o).drawon(b, g); if (((cRec)o).selected)((cRec)o).drawMarkerOn(b, g); }
               if (strType == eTools.Symbol) { ((cSymbol)o).drawon(b, g); if (((cSymbol)o).selected)((cSymbol)o).drawMarkerOn(b, g); }
               if (strType == eTools.Line) { ((cLine)o).drawon(b, g); if (((cLine)o).selected)((cLine)o).drawMarkerOn(b, g); }
               if (strType == eTools.LineText) { ((cLineText)o).drawon(b, g); if (((cLineText)o).selected)((cLineText)o).drawMarkerOn(b, g); }
               if (strType == eTools.Road) { ((cRoad)o).drawon(b, g); if (((cRoad)o).selected)((cRoad)o).drawMarkerOn(b, g); }
               if (strType == eTools.Text) { ((cText)o).drawon(b, g); if (((cText)o).selected)((cText)o).drawMarkerOn(b, g); }
           }

        }
       
           

        private void CreateObject(eTools tool)
        {

            SelectedObject = null;

            cLine line;
            ThreeParam PostaviKabel = (c, w, ds) =>
            {
                Point pkn = cpoint1;
                //foreach (cSymbol s in document.symbols)
                //{ if (isInRectangle(s.rectangle, CurrentPoint ())) { pkn = s.point; } }
                line = new cLine(pkn, cpoint1, c);
                line.width = w;
                line.dashStyle = ds;
                line.text1 = "";
                line.text2 = "";
                SelectedObject = line;
               // document.lines.Add(line);
            };

            SymbParam PostaviSymbol = (b, ss) =>
            {
                cSymbol cs = new cSymbol(b);
                cs.size = ss;
                SelectedObject  = cs;
                cs.point = CurrentPoint();
                //document.symbols.Add(cs);
                //elements.Add(cs);

            };

            switch(tool ){
                    
                case eTools.Rec: 
                        cRec rec = new cRec(CurrentPoint(), CurrentPoint(), Color.FromArgb(90, 90, 90));
                        rec.width = 1;
                        rec.color = btnColor.BackColor;
                        SelectedObject  = rec;
                        //document.recs.Add(rec);
                        //elements.Add(rec);
                        break;
                case eTools.Line: PostaviKabel(Color.Black , KWidth, System.Drawing.Drawing2D.DashStyle.Solid); break;
               
                case eTools.KNovi: PostaviKabel(Color.LightGreen , KWidth,System.Drawing.Drawing2D.DashStyle.Solid );break;
                case eTools.KPostojeci: PostaviKabel(Color.Blue, KWidth, System.Drawing.Drawing2D.DashStyle.Solid); break;
                case eTools.KDemontirani: PostaviKabel(Color.Red, KWidth, System.Drawing.Drawing2D.DashStyle.Solid); break;

                case eTools.PKNovi: PostaviKabel(Color.LightGreen , 2, System.Drawing.Drawing2D.DashStyle.Dash  ); break;
                case eTools.PKPostojeci: PostaviKabel(Color.Blue, 2, System.Drawing.Drawing2D.DashStyle.Dash ); break;
                case eTools.PKDemontirani: PostaviKabel(Color.Red, 2, System.Drawing.Drawing2D.DashStyle.Dash ); break;
                
                case eTools.Izvod:PostaviSymbol(Properties.Resources.izvod,settings.IzvodSize ); break;
                case eTools.Symbol:PostaviSymbol(Properties.Resources.kutija,settings.SymbolSize  ); break;
                case eTools.Stup:PostaviSymbol(Properties.Resources.stup,settings.StupSize  ); break;
                case eTools.StupNovi: PostaviSymbol(Properties.Resources.stupnovi,settings.StupSize); break;
                case eTools.EStup: PostaviSymbol(Properties.Resources.EE,settings.StupSize); break;
                case eTools.LineText:
                    texttext.Visible = true;
                    texttext2.Visible = true;
                    cLineText el = new cLineText(cpoint1, cpoint1, new Font("ARIAL", 7));
                    el.font = new Font("arial", Convert.ToInt16( textsize.Text));
                    el.text1 = "text";
                    el.text2 = "";
                    el.fontColor = btnColor.BackColor;
                    el.selected = true;
                    texttext.Text = "text";
                    //texttext.SelectAll();
                    //texttext.Focus();
                    //document.linetexts.Add(el);
                    SelectedObject = el;
                     
                    //csPoint = el.pointref2;
                    break;

                case eTools.Road:

                    bool pocetak=false ;
                    cRoadPoint = null;
                    SelectedObject  = null;
                    foreach (var road in document.roads)
                    {
                        if (road.points.Count != 0)
                        {
                            if (IsOverPoint(road.points[road.points.Count - 1].ToPoint(), cpoint1)) { SelectedObject = road; cRoadPoint = road.points[road.points.Count - 1]; pocetak = false; }
                            if (IsOverPoint(road.points[0].ToPoint(), cpoint1)) { SelectedObject = road; cRoadPoint = road.points[0]; pocetak = true; }
                        }
                    }
                     
                    if (SelectedObject == null)
                    {
                             //nova cesta
                            cRoad road = new cRoad();
                            road.addPoint(pointerx, pointery);
                            road.addPoint(pointerx, pointery);
                            currentRoadText = road.texts[0];
                            road.width = Convert.ToInt16(roadCbSize.Text);
                            cRoadPoint = road.points[1];
                            SelectedObject  = road;
                            document.elementsAdd(road);
                             
                            //elements.Add(road);
                    }
                    else 
                    {
                      //produzi cestu
                      var road = (cRoad)SelectedObject;
                      Point P = new Point(pointerx, pointery);

                      if (road.points.Count - 1 > 1)
                      {
                          if (pocetak)
                          {
                              road.points.Insert(0, new cPoint(pointerx, pointery));
                              road.texts.Insert(0, new boxString(""));
                              cRoadPoint = road.points[0];

                          }
                          else
                          {
                              road.addPoint(pointerx, pointery);
                              //road.points.Add(new cPoint(pointerx, pointery));
                              cRoadPoint = road.points[road.points.Count - 1];
                              currentRoadText = road.texts[road.points.Count - 2];
                          }
                      }
                      else
                      {
                              road.addPoint(pointerx, pointery);
                              cRoadPoint = road.points[road.points.Count - 1];
                              currentRoadText = road.texts[road.points.Count - 2];
                      }
                    }
                    break;


        }

          // if (SelectedObject == null)  
           // setSelected(SelectedObject, true);
        }


        private enum ElementsType { All, Symbols, Lines, Recs, Texts, Roads,LineTexts }
        private cPoint csPoint = null;

        private  Rectangle defRec = new Rectangle(-1, -1, -1, -1);
        private List<cPoint> GetPoints(Point p,ElementsType t    ) 
        {
            Point cp=CurrentPoint();

           // if (rec.X == -1) { rec.X = cp.X; rec.Y = cp.Y; rec.Width = 1; rec.Height = 1; }



            List<cPoint> ps = new List<cPoint>();

            if (t == ElementsType.All | t == ElementsType.Symbols)
            {
                foreach (cSymbol el in document.symbols)
                {
                    if (isInRectangle(el.rectangle , p))
                    { ps.Add(el.pointref); p = el.point; }

                }
            }


            if (t == ElementsType.All | t == ElementsType.LineTexts )
            {
                foreach (var el in document.linetexts)
                {
                    if (isInRectangle(el.getRectangle(), p))
                    { ps.Add(el.pointref2); p = el.point2; }

                    if (el.point1==cp)
                    { ps.Add(el.pointref1); p = el.point1; }

                }
            }

            if (t == ElementsType.All | t == ElementsType.Roads )
            {
                foreach (var el in document.roads)
                {
                    foreach (cPoint rp in el.points)
                    {
                        if (rp.ToPoint () == cp) { ps.Add(rp); }
                    }

                }
            }
            if (t == ElementsType.All | t == ElementsType.Lines )
            {
                foreach (cLine el in document.lines)
                {   bool dodano = false;
                if (p == el.point1) { ps.Add(el.pointref1); dodano = true; csPoint = el.pointref1; }
                if (p == el.point2) { ps.Add(el.pointref2); dodano = true; csPoint = el.pointref2; }
                    if (dodano != true)
                    {
                        if (IsOverLine(el.point1, el.point2, p))
                        {
                            ps.Add(el.pointref1);
                            ps.Add(el.pointref2);
                        }
                    } }} 

              if (t == ElementsType.All | t == ElementsType.Recs )
              {
                  foreach (cRec el in document.recs)
                  {

                      bool dodano = false;
                      if (p == el.point1) { ps.Add(el.pointref1); dodano = true; }
                      if (p == el.point2) { ps.Add(el.pointref2); dodano = true; }
                      if (p == el.point3) { ps.Add(el.pointref3); dodano = true; }
                      if (p == el.point4) { ps.Add(el.pointref4); dodano = true; }
                     
                      if (dodano!=true )
                      {
                          if (isInRectangle(el.GetRectangle(), p))
                          {
                              ps.Add(el.pointref1);
                            //  ps.Add(el.pointref2);
                              ps.Add(el.pointref3);
                            //  ps.Add(el.pointref4);
                          }
                      }
                  }
               }


            return ps;


        }

        private void selectedPointsMove(Point newPoint)
        {
            if (selectedPoints == null) return;
            if (selectedPoints.Count == 0) { return; }
            if (selectedPoints == null) { return; }
           // this.Text = selectedPoints.Count.ToString();

            int rX = CurrentPoint().X - startPoint.X;
            int rY = CurrentPoint().Y - startPoint.Y;
           
            for (int i=0;i<=selectedPoints.Count-1;i++)
            {
                selectedPoints[i].X  += rX;
                selectedPoints[i].Y += rY; 
            }

            startPoint = CurrentPoint();
            //refreshImage();

        }

        private eTools gettype(object obj)
        {
            if (obj == null) { return eTools.Nothing; }
             System.Type t = obj.GetType();
             if (t == typeof(cSymbol)) { return eTools.Symbol; }
             if (t == typeof(cRec )) { return eTools.Rec; }
             if (t == typeof(cLineText)) { return eTools.LineText; }
             if (t == typeof(cLine )) { return eTools.Line; }
             if (t == typeof(cText)) { return eTools.Text; }
             if (t == typeof(cRoad)) { return eTools.Road; }

             return eTools.Nothing;
        }

        private bool  IsOverElement(object element) 
        {
            System.Type t = element.GetType();
            if (t == typeof(cSymbol))
            {
                cSymbol symb = (cSymbol )element ;
                if (pointerx > symb.point.X)
                {
                    if (pointerx < symb.point.X + symb.size.Width ) 
                    {
                        if (pointery > symb.point.Y)
                        {
                            if (pointery < symb.point.Y+symb.size.Height  )
                            {

                                return true;

                            }
                        }
                    
                    
                    }
                }
            }
            return false;

        }

        private void plan_MouseUp(object sender, MouseEventArgs e)
        {

             
            if (gettype(SelectedObject) == eTools.Rec) { texttext.Focus(); }
            if (gettype(SelectedObject) == eTools.LineText) { texttext.Focus(); }
            if (gettype(SelectedObject) == eTools.Line) { texttext.Focus(); }

            if (radnja == eRadnja.Drawing)
            {
                if(gettype(SelectedObject)!= eTools.Road)
                document.elementsAdd(SelectedObject);
                 
            }

            if (radnja == eRadnja.Drawing | radnja == eRadnja.Moving | radnja == eRadnja.Delete )
            {
                changesMade(true);
                undoAddcurrentState(document); }
        
          
            document.updateCodeImage();

            viewUpdate();
             

            radnja = eRadnja.Nothing;
            //plan.Image = null;

           
        }

      
        private void drawline(Bitmap b, Point p1, Point p2)
        {
            Graphics g = Graphics.FromImage(b);
            Pen lp = new Pen(new SolidBrush(Color.FromArgb(170, 170, 170)));
            lp.Width = 2;
            g.DrawLine(lp, p1, p2);
            
            g.Dispose();
        }
        private void drawRec(Bitmap b, Rectangle  rec)
        {
            Graphics g = Graphics.FromImage(b);
            Pen lp = new Pen(new SolidBrush(Color.Blue ));
            lp.Width = 2;
            g.DrawRectangle(lp, rec );
            g.Dispose();
        }

        private void drawString(string text, Point p,Bitmap b,Graphics g)
        {
            //Graphics g = Graphics.FromImage(b);
            Font f = new Font("ARIAL", 12);
            g.DrawString(text, f, new SolidBrush(Color.DarkGray), p);

        }

        private void drawelements(Bitmap b, bool transparent=true )
        {
            /*
            using (TextureBrush brush = new TextureBrush(picscale  ,System.Drawing.Drawing2D. WrapMode.Tile))
            using (Graphics g = Graphics.FromImage(b))
            {
                // do your painting in here
                g.FillRectangle(brush, 0, 0,b.Width, b.Height);
            }
            */
            using (Graphics g = Graphics.FromImage(b))
            {
                if (transparent)
                {
                    g.Clear(Color.Transparent);
                }
                else
                {
                    g.Clear(Color.White );
                }
            
            foreach (cRoad el in document.roads) { el.drawon(b,g); }
            foreach (cLine el in document.lines) { el.drawon(b, g); if (el.selected) { el.drawMarkerOn(b, g); } }
            foreach (cRec el in document.recs) { el.drawon(b, g); if (el.selected) { el.drawMarkerOn(b, g); } }
            foreach (cSymbol el in document.symbols) { el.drawon(b, g); if (el.selected) { el.drawMarkerOn(b, g); } }
            foreach (cText el in document.texts) { el.drawon(b,g); }
            foreach (cLineText el in document.linetexts) { el.drawon(b, g); if (el.selected) { el.drawMarkerOn(b, g); } }
            document.zaglavlje.drawon(b,g);
            //drawString("Prekvršje Svjetlovod", new Point(34, plan.Height - 20), b,g);
            //g.DrawImage(Properties.Resources.PRSICTLOGO, new Point(34, plan.Height - Properties.Resources.PRSICTLOGO.Height));

    }
        }
     

        

        private void msgbox(string message) {
            MessageBox.Show(message);
        }

        

    
        private void plan_MouseLeave(object sender, EventArgs e)
        {
            setPointers(0,0);
       

           // drawelements(bmp);
            
            //plan.Image = bmp;

        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            Cursor.Show();

        }

      

        private void chSkala_CheckedChanged(object sender, EventArgs e)
        {
            if (chSkala.Checked == true)
            {
                plan.BackgroundImage = picscale;
            }
            else
            {
                plan.BackgroundImage = null;
            }
        }
        
        private void plan_Click(object sender, EventArgs e)
        {
            
        }

        private void deselectButtons() 
        {
            
            foreach (object obj in toolbar.Items)
            {
                System.Type t = obj.GetType();
                if (t == typeof(ToolStripButton ))
                {
                    ToolStripButton ts = (ToolStripButton)obj;
                    ts.Checked = false;
                }
                
            }
        
        }

       
        private void btnLine_Click(object sender, EventArgs e)
        {
            connectedSelectedObjects.Clear();
            deselectAllObjects();
         
            ToolStripButton tb = (ToolStripButton)sender;
            SelectedObject = null;
            selectedPoints = null;
            csPoint = null;
            
            viewUpdate();
            
            //hObject = null;
            deselectButtons();
            tb.Checked = true;
            currentTool = (eTools)Enum.Parse(typeof(eTools), tb.Tag.ToString() ); 


        }

        private void btnRec_Click(object sender, EventArgs e)
        {

        }

        private void btnDemontiraniK_Click(object sender, EventArgs e)
        {

        }

        private void pbIzvod2_Click(object sender, EventArgs e)
        {
             
        }


        private bool mouseIsDown() 
        {
            if ((Control.MouseButtons & MouseButtons.Left) == MouseButtons.Left)
                return true;
            else return false;

        }

         

        private void trMill_Scroll(object sender, EventArgs e)
        {
          //  mill = trMill.Value*2;
            plan.BackgroundImage = null;
            drawscale();


        }

      


        private double toler = 3;
        private bool IsOverLine(Point lPoint1, Point lPoint2, Point mousePoint)
        {
            double x1, y1, x2, y2, x, y;
            x1 = lPoint1.X;
            y1 = lPoint1.Y;
            x2 = lPoint2.X;
            y2 = lPoint2.Y;
            x = mousePoint.X;
            y = mousePoint.Y;
            if (x2 > x1)
            {
                if (x < x1) { return false; }
                if (x > x2) { return false; }
            }

            if (x2 < x1)
            {
                if (x < x2) { return false; }
                if (x > x1) { return false; }
            }

            double iY;
            iY = ((y2 - y1) / (x2 - x1)) * (x - x1) + y1;
            if (iY >= y - toler   )
            { if (iY <= y + toler  ) { return true; } }

            double iX;
            iX = (((y - y1) * (x2 - x1)) / (y2 - y1))+x1 ;
             if (iX >= x - toler)
            { if (iX <= x + toler) { return true; } }



            return false;
        }

        public bool isInRectangle(Rectangle rec, PointF p) 
        {
            if (p.X >= rec.X & p.X <= rec.X + rec.Width) 
            { if (p.Y >= rec.Y & p.Y <= rec.Y + rec.Height) { return true; }}
            return false;
        }

        private void btnZaglavlje_Click(object sender, EventArgs e)
        {
            frmZag frm = new frmZag(document.zaglavlje);
            frm.ShowDialog();
            viewUpdate();
            undoAddcurrentState(document );

        }

        private void btUndo_Click(object sender, EventArgs e)
        {

            undo(document);
            deselectAllObjects();
            viewUpdate();
             
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {

            print();

        }

        private int tolerPoint = 7;
        private bool IsOverPoint(Point p, Point MouseP)
        {
            if (MouseP.X >= p.X - tolerPoint & MouseP.X <= p.X + tolerPoint)
            {
                if (MouseP.Y >= p.Y - tolerPoint & MouseP.Y <= p.Y + tolerPoint)
                {
                    return true;
                }
            }
            return false;
        }

        private void btnExportJPG_Click(object sender, EventArgs e)
        {
            SaveFileDialog fd = new SaveFileDialog();
            fd.Filter = "PNG files (*.png)|*.png";
            if (fd.ShowDialog() == System.Windows.Forms.DialogResult.Cancel) { return; }
            saveImage(fd.FileName);
        }

        private void saveImage(string filePath)
        {
            
            Bitmap b = document.getImage();
            b.Save(filePath , System.Drawing.Imaging.ImageFormat.Png );
       
        }
        
        private void texttext_TextChanged(object sender, EventArgs e)
        {
            if (currentTool == eTools.Select | radnja==eRadnja.Nothing )
            {

                //if (currentObjType == "TEXT" | currentObjType == "SELECT" | currentObjType == "LINETEXT")
                // {

                if (currentRoadText != null) currentRoadText.value = texttext.Text;

                if (SelectedObject != null)
                {

                    System.Type t = SelectedObject.GetType();
                    if (t == typeof(cText))
                    {

                        ((cText)SelectedObject).text = texttext.Text;

                    }
                    if (t == typeof(cRec))
                    {

                        ((cRec)SelectedObject).text.text1= texttext.Text;

                    }

                    if (t == typeof(cLineText))
                    {
                        ((cLineText)SelectedObject).text1 = texttext.Text;
                    }

                    if (t == typeof(cLine))
                    {
                        ((cLine)SelectedObject).text1 = texttext.Text;
                    }
                }
                //planUpdate();
                
                //undolist[UndoIndex] = document.Clone();
                focusUpdate();
                //}


            }
        }

        private void focusUpdate()
        {
                using (Graphics g = Graphics.FromImage(bmpFocus))
                {
                    g.Clear(Color.Transparent);
                }
                drawfocus(bmpFocus);
                plan.Image = bmpFocus;
        }

        private void texttext_Click(object sender, EventArgs e)
        {

        }

        private void btnText_Click(object sender, EventArgs e)
        {
            texttext.Focus();
            texttext.SelectAll();
        }

        private Font textFont = new Font("ARIAL", 12);
        private Color textColor = Color.Black;
        private Bitmap GetTextImage(string text) 
        {

            Bitmap b = new Bitmap(2, 2);
            Graphics g = Graphics.FromImage(b);
            float x = g.MeasureString(text, textFont).Width;
            float y = g.MeasureString(text, textFont).Height;
            
            Bitmap b2 = new Bitmap((int)x, (int)y);
            Graphics g2 = Graphics.FromImage(b2);
           
            g2.DrawString(text, textFont, new SolidBrush(textColor), new PointF(0, 0));

            g.Dispose();
            g2.Dispose();
            return b;
        }

        public delegate Bitmap BitmapParam();

        private void btnSendMail_Click(object sender, EventArgs e)
        {

            string text = "";
            foreach (cZaglavljeItem i in document.zaglavlje.items)
            {

                    text += i.title + "\t"+ "\t" + i.text + Environment.NewLine;
            }

                string naslov = "";
                if (document.zaglavlje.items.Count != 0)
                {
                    naslov ="SKICA - " + document.zaglavlje.items[0].text + "  " + document.zaglavlje.items[1].text ;
                }

            /*
            string fPath = Application.StartupPath + @"\picx.png";
            Bitmap b=new Bitmap (plan.Width ,plan.Height  );
            drawelements(b);
            b.Save(fPath);
            b.Dispose();
            */

            frmMail frmw = new frmMail(document , naslov ,getHtmlDoc() );

            frmw.ShowDialog();
         

        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            newdocument();
        }

        private void EditStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void texttext2_TextChanged(object sender, EventArgs e)
        {
            if (currentTool == eTools.Select| radnja == eRadnja.Nothing)
            {
                //if (currentObjType == "TEXT" | currentObjType == "SELECT" | currentObjType == "LINETEXT")
                // {
                if (SelectedObject != null)
                {

                    System.Type t = SelectedObject.GetType();
                    if (t == typeof(cText))
                    {

                        ((cText)SelectedObject).text = texttext.Text;

                    }
                    if (t == typeof(cRec))
                    {

                        ((cRec)SelectedObject).text2.text1  = texttext2.Text;

                    }

                    if (t == typeof(cLineText))
                    {

                        ((cLineText)SelectedObject).text2 = texttext2.Text;

                    }

                    if (t == typeof(cLine))
                    {
                        ((cLine)SelectedObject).text2 = texttext2.Text;
                    }
                }

                focusUpdate();
                //}

            }
        }

        private void texttext_KeyUp(object sender, KeyEventArgs e)
        {
             
        }

        private void texttext2_Click(object sender, EventArgs e)
        {

        }

        private void texttext2_KeyUp(object sender, KeyEventArgs e)
        {
        
        }



         
      
        private List<object> hoveredObjects = new List<object>();

        private List<cPoint> SelectedPoints = new List<cPoint>();
        
        private List<object> GetObjectsIn(Rectangle rec)
        {

            return connectedSelectedObjects;

        }

        private bool IsObjectInRec(object obj, Rectangle rec)
        {
            eTools stype = gettype(obj);
            switch (stype)
            {
                case eTools.Rec:
                    var el =(cRec) obj;
                    if (isInRectangle(rec, el.point1)) { return true; }
                    if (isInRectangle(rec, el.point2)) { return true; }
                    if (isInRectangle(rec, el.point3)) { return true; }
                    if (isInRectangle(rec, el.point4)) { return true; }
                    break;


            }
            return false;
        }


        public object GetSelectedObject()
        {
             
            Point cp = CurrentPoint();

            foreach (cLineText el in document.linetexts.ToList())
            {
                if (isInRectangle(el.getRectangle(), cp)){return el;}
                if (IsOverLine(el.point1 ,el.point2,cp))return el;

            }
            /*
          IEnumerable<cSymbol> hsymbols = from s in document.symbols where isInRectangle(s.rectangle, cp) select s;
          foreach (cSymbol el in hsymbols) { hoveredObjects.Add(el); hoveredPoints.Add(el.pointref); }
          */

            foreach (cSymbol s in document.symbols.ToList())
            {
                if (isInRectangle(s.rectangle, cp))
                {
                    hoveredObjects.Add(s);
                    return s;
                }
            }

            foreach (cLine li in document.lines.ToList())
            {
                if (IsOverLine(li.point1, li.point2, new Point(pointerx, pointery)))
                {
                    return li;
                }
            }

          
            
            foreach (cRec s in document.recs.ToList())
            {
                if (isInRectangle(s.GetRectangle(), cp))
                {
                    return s;
                }
            }

            foreach (cRoad r in document.roads)
            {
                foreach (cPoint p in r.points)
                {
                    if (p.ToPoint() == cp)
                    {
                        return r;
                    }
                }
            }

            foreach (cText s in document.texts.ToList())
            {
                if (isInRectangle(s.getRectangle(), cp))
                {
                    return s;
                }
            }
            return null;


        }


        private delegate void sobj(object o);
        private delegate void delAddPoint(cPoint  p ,out bool bl);
        public List<cPoint> GetSelectedPoints()
        {

            List<cPoint> returnList = new List<cPoint>();
            Point cp = CurrentPoint();
            connectedSelectedObjects.Clear();

            bool added = false;

            sobj sos = (object o) =>
            {
                if (connectedSelectedObjects.Contains(o) == false)
                {
                    connectedSelectedObjects.Add(o);
                    setSelected(o, true);
                }
            };

            delAddPoint addPoint = ( cPoint p, out bool b) =>
            {
                if (returnList.Contains(p) == false)
                {
                    returnList.Add(p);
                }
                b = true;
            };

            foreach (cLineText el in document.linetexts.ToList())
            {
                added = false;
                if (cp == el.point2) { addPoint(el.pointref2, out added); }
                if (cp == el.point1) { addPoint(el.pointref1, out added); }
                if (added) sos(el);
            }


            foreach (cLine el in document.lines.ToList())
            {
                added = false;
                if (cp == el.point1) { addPoint(el.pointref1, out  added); }
                if(added!=true )
                    if (cp == el.point2) { addPoint(el.pointref2, out  added); };

                if (added) sos(el);
            }

            /*
           IEnumerable<cSymbol> hsymbols = from s in document.symbols where isInRectangle(s.rectangle,cp)  select s;
           foreach (cSymbol el in hsymbols) { hoveredObjects.Add(el); hoveredPoints.Add(el.pointref); }
            */

            foreach (cSymbol el in document.symbols.ToList())
            {
                added = false;
                if (cp == el.point) { addPoint(el.pointref, out  added); ; sos(el); }
                if (added) sos(el);
            }

            foreach (cRec el in document.recs.ToList())
            {
                added = false;
                if (cp == el.point1) { addPoint(el.pointref1, out  added); }
                if (cp == el.point2) { addPoint(el.pointref2, out  added); }
                if (cp == el.point3) { addPoint(el.pointref3, out  added); }
                if (cp == el.point4) { addPoint(el.pointref4, out  added); }
                if (added) sos(el);
            }


            foreach (cRoad el in document.roads)
            {
                int i = -1;
                added = false;
                foreach (cPoint p in el.points)
                {
                    i++;
                    if (p.ToPoint() == cp)  
                    { addPoint(p, out  added);
                        if (i<=el.points.Count-2)
                                currentRoadText = el.texts[i];

                    }

                }
                if (added) sos(el);
            }


            if (returnList.Count > 0) { return returnList; }
            else
            { return null; }

        }


        private List<cPoint> GetPointsFromHObjects(bool onlyFirstObject=false )
        {
            List<cPoint> l = new List<cPoint>();

            foreach (object o in hoveredObjects)
            {
                l.AddRange(GetPointsFromObject(o));
                if (onlyFirstObject) { break; }
            }
            return l;
        }

        private List<cPoint> GetPointsOverObject(object o)
        {
            
            List<cPoint> lp = new List<cPoint>();
          
            System.Type t = o.GetType();
            if (t == typeof(cRec))
            {
                cRec el = (cRec)o;
                 
                Rectangle rec = el.GetRectangle();
                
                foreach (cLine l in document.lines.ToList())
                {
                    bool added = false;
                    if (isInRectangle(rec, l.point1)) { lp.Add(l.pointref1); added=true ; }
                    if (isInRectangle(rec, l.point2)) { lp.Add(l.pointref2);added =true ; }
                    if (added==true )
                    {
                        connectedSelectedObjects.Add(l);
                        setSelected(l, true);
                    }
                }

                foreach (cSymbol l in document.symbols.ToList())
                {
                    if (isInRectangle(rec, l.point)) { lp.Add(l.pointref); l.selected = true; connectedSelectedObjects.Add(l); }
                }

                foreach (cLineText l in document.linetexts.ToList())
                {
                    if (isInRectangle(rec, l.point1)) { lp.Add(l.pointref1); lp.Add(l.pointref2); l.selected = true; connectedSelectedObjects.Add(l); }
                }

            }

            return lp;

        }


      
        private List<cPoint > GetPointsFromObject(object el)
        {
            List<cPoint> lp = new List<cPoint>();
            System.Type t = el.GetType();
            if (t == typeof(cLine))
            {
                var obj = (cLine)el;
                lp.Add(obj.pointref1);
                lp.Add(obj.pointref2);
            }

            if (t == typeof(cLineText))
            {
                var obj = (cLineText)el;
                lp.Add(obj.pointref1);
                lp.Add(obj.pointref2);
            }

            if (t == typeof(cSymbol))
            {
                var obj = (cSymbol)el;
                lp.Add(obj.pointref);    
            }

            if (t == typeof(cRec))
            {
                var obj = (cRec)el;
                lp.Add(obj.pointref1);
               // lp.Add(obj.pointref2);
                lp.Add(obj.pointref3);
               // lp.Add(obj.pointref4);
            }

            if (t == typeof(cRoad))
            {
                var obj = (cRoad)el;
                foreach (cPoint p in obj.points)
                {
                    lp.Add(p);
                }
            }

            return lp;

        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.ControlKey) { ctrlIsPressed = true; }
            
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
             

            if (e.KeyCode == Keys.ControlKey) { ctrlIsPressed = false ; }
            
        }

     

        private void btnColor_Click(object sender, EventArgs e)
        {
            ColorDialog cd = new ColorDialog();
            if (cd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

                btnColor.BackColor = cd.Color;
            }


        }


        public void OpenDocument(string filepath)
        {
            try
            {
                showTab("NACRT");
                if (connectedSelectedObjects != null) connectedSelectedObjects.Clear();
                if (selectedPoints != null) selectedPoints.Clear();
                SelectedObject = null;
                document = null;
                using (Stream stream = File.Open(filepath, FileMode.Open))
                {
                    BinaryFormatter bin = new BinaryFormatter();

                    var cm = (cDocument)bin.Deserialize(stream);
                    document = cm;
                }
                document.filePath = filepath;
                document.folder = System.IO.Path.GetDirectoryName(filepath);
                FileInfo fi = new FileInfo(filepath);
                // this.Text = fi.Name;
                setPaperSize(document.paperSize);
                //msgbox(filepath);
                bmp = new Bitmap(document.paperSizePix.Height, document.paperSizePix.Width);
                viewUpdate();

                uveziText(FazeRada.dg1, document.fazeRada);
                uveziText(Materijal.dg1, document.materijal);
                uveziText(DemMaterijal.dg1, document.demMaterijal);

               // FazeRada.Controls["txtFazeRada"].Text = document.fazeRada;
               // FazeRada.Controls["txtMaterijal"].Text = document.materijal;
               // FazeRada.Controls["txtDemMaterijal"].Text = document.demMaterijal;
                //List<string> files = new List<string>();
                //string folderPath = System.IO.Path.GetDirectoryName(filepath) + @"\slike";
                //if (System.IO.Directory.Exists(folderPath ))
                //{

                //    files.AddRange(System.IO.Directory.GetFiles(folderPath ,"*.jpg"));
                //    files.AddRange(System.IO.Directory.GetFiles(folderPath ,"*.png"));
                //    files.AddRange(System.IO.Directory.GetFiles(folderPath ,"*.bmp"));
                //    files.AddRange(System.IO.Directory.GetFiles(folderPath ,"*.gif"));

                //}

                if (document.slike == null) document.slike = new List<Bitmap>();

                Slike = new cSlike(document);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        private void uveziText(DataGridView dg, string txt)
        {
            
            string[] lines = txt.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            foreach (string line in lines)
            {
                string [] cols =line.Split(new char[]{'|'});
                //bool notfound = true;
                foreach (DataGridViewRow dr in dg.Rows)
                {
                    if (dr.Cells[0].Value != null)
                        if (cols[0] == dr.Cells[0].Value.ToString()) {   dr.Cells[3].Value = cols[1]; }
                }
                

            }

        }

        private void menubtnSave_Click(object sender, EventArgs e)
        {

            save();
        }

        private void save()
        {
            SaveFileDialog fd = new SaveFileDialog();
            fd.Filter = "PRS Files (*.prs)|*.prs";
            if (fd.ShowDialog() == System.Windows.Forms.DialogResult.Cancel) { return; }

            document.fazeRada = izveziIzDg(FazeRada.dg1);
            document.materijal = izveziIzDg(Materijal.dg1);
            document.demMaterijal = izveziIzDg(DemMaterijal.dg1);

            document.save(fd.FileName);
             
            
            changesMade(false);


        }
        private void menubtnOpen_Click(object sender, EventArgs e)
        {
            open();
          
        }

        private void newdocument()
        {
            frmNewDoc frm = new frmNewDoc();
            frm.ShowDialog();

            if (frm.DialogResult == System.Windows.Forms.DialogResult.Cancel)
            {
                return;
            }

            Size docSize = new Size(Convert.ToInt16( frm.txtVisina.Text) ,Convert.ToInt16( frm.txtSirina.Text)); 

            document = null;
            document = new cDocument( docSize );
            undolist.Clear();
            drawscale();
           
            bmpFocus = new Bitmap(document.paperSizePix.Height  , document.paperSizePix.Width,PixelFormat.Format32bppArgb );
            
            bmp = new Bitmap(document.paperSizePix.Height , document.paperSizePix.Width);
            //viewUpdate();

            clearDG(FazeRada.dg1);
            clearDG(Materijal.dg1);
            clearDG(DemMaterijal.dg1);
            cZaglavljeItem[] itm=new cZaglavljeItem [4] ;

            for (int i = 0; i < itm.Length; ++i)
                itm[i] = new cZaglavljeItem ();

            itm[0].title= "WWMS"; itm[0].text = frm.txtWWMS.Text ;
            itm[1].title= "ADRESA"; itm[1].text = frm.txtAdresa.Text  ;
            itm[2].title= "IZVOD"; itm[2].text = frm.txtIzvod.Text  ;
            itm[3].title= "DATUM"; itm[3].text = frm.txtDatum.Text  ;

            document.zaglavlje.items.AddRange(itm);

            document.zaglavlje.width = Properties.Settings.Default.zSirina;
            document.zaglavlje.rowHeight = 25;
            document.zaglavlje.width = 350;

            undolist.Add(document.Clone());
            UndoIndex = 0;
            setPaperSize(document.paperSize);
           
            Slike.lstSlike.Items.Clear();
            Slike.slika.BackgroundImage = null;

            viewUpdate();

        }
        private void clearDG(DataGridView dg)
        {
            foreach (DataGridViewRow dr in dg.Rows)
            {
                dr.Cells[3].Value = "";
            }

        }
        private void documentInitialize()
        {
            try { 
            


            document = null;
            document = new cDocument(new Size(20, 30));
            undolist.Clear();
            drawscale();

            bmpFocus = new Bitmap(document.paperSizePix.Height, document.paperSizePix.Width);
            bmp = new Bitmap(document.paperSizePix.Height, document.paperSizePix.Width);
            viewUpdate();

            foreach (string s in Properties.Settings.Default.zSadrzaj)
            {
                document.zaglavlje.AddRow(s);
            }

            document.zaglavlje.width = Properties.Settings.Default.zSirina;
            document.zaglavlje.rowHeight = 25;
            document.zaglavlje.width = 350;
             
            undolist.Add(document.Clone());
            UndoIndex = 0;
            setPaperSize(document.paperSize);
            }
             catch { }
            viewUpdate();

        }


        private void open()
        {
            OpenFileDialog fd = new OpenFileDialog();
             
            fd.InitialDirectory  = Properties.Settings.Default.openDir;
            fd.Filter = "PRS Files (*.prs)|*.prs";
            
            if (fd.ShowDialog() == System.Windows.Forms.DialogResult.Cancel) { return; }
            OpenDocument(fd.FileName);
            this.Text = getFileName(fd.FileName);
            changesMade(false);

        }

        private string getFileName(string fPath)
        {
            int i=fPath.LastIndexOf(@"\");
            if (i > 0)
            {
                return (fPath.Substring(i+1));
            }
            else
            {
                return fPath;
            }
        }

        private void changesMade(bool areMade)
        {
            if (areMade)
            {
                if (this.Text.EndsWith("*") == false)
                    this.Text += "*";
            }
            else
            {
                if (this.Text.EndsWith("*"))
                    this.Text = this.Text.Substring(0, this.Text.Length - 1);
            }

        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            open();
            
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            save();

        }

        private void print()
        {

            try
            {
                Bitmap b = document.getImage();
                PrintDocument pd = new PrintDocument();
                pd.PrintPage += (object printSender, PrintPageEventArgs printE) =>
                {
                    printE.Graphics.DrawImageUnscaled(b, new Point(0, 0));
                };
                 
                PrintDialog dialog = new PrintDialog();
                dialog.ShowDialog();
                pd.PrinterSettings = dialog.PrinterSettings;
                pd.Print();
            }
            catch (Exception ex) { msgbox(ex.Message); }

        }

        private void deselectAllObjects()
        {
            foreach (var el in document.roads) { el.selected = false; }
            foreach (var el in document.lines) { el.selected = false; }
            foreach (var el in document.linetexts) { el.selected = false; }
            foreach (var el in document.recs) { el.selected = false; }
            foreach (var el in document.symbols) { el.selected = false; }
            foreach (var el in document.texts) { el.selected = false; }
            refreshImage();
        }


        private int UndoIndex = -1;
        public List<cDocument> undolist = new List<cDocument>();

        public void undo(cDocument doc)
        {
            if (UndoIndex - 1 == -1) { return; }
            UndoIndex--;
            document = null;
            connectedSelectedObjects.Clear();
            SelectedPoints.Clear();
            SelectedObject = null;
            document = CloneDocument(undolist[UndoIndex]);
            printdebug();

        }


        public void redo(cDocument doc)
        {
            if (UndoIndex +1> undolist.Count-1) { return; }
            UndoIndex += 1;
            document = null;
            document = CloneDocument(undolist[UndoIndex]);
            printdebug();
        }

        public void undoAddcurrentState(cDocument doc)
        {
            if (UndoIndex < undolist.Count - 1)
            {
                for (int i = undolist.Count - 1; i >= UndoIndex+1  ; i--)
                {
                    undolist.RemoveAt(i);
                }
            }
            undolist.Add(CloneDocument(document));
             
            UndoIndex = undolist.Count - 1;
            
        }

        private void printdebug()
        {
            int i = -1;
            System.Diagnostics.Debug.Print("INDEX: " + UndoIndex.ToString());

            foreach (cDocument doc in undolist)
            {
                i++;
                System.Diagnostics.Debug.Print("IMG "+ i.ToString() );

            }

        }

        public cDocument CloneDocument(cDocument originalDoc)
        {
            cDocument doc = new cDocument(originalDoc.paperSize );
            using (MemoryStream stream = new MemoryStream())
            {

                BinaryFormatter bin = new BinaryFormatter();
                bin.Serialize(stream, originalDoc);
                stream.Position = 0;
                doc = (cDocument)bin.Deserialize(stream);
            }
            return doc;
        }

        private void btnRedo_Click(object sender, EventArgs e)
        {
            redo(document );
            deselectAllObjects();
            viewUpdate();

        }

        private void textsize_Click(object sender, EventArgs e)
        {
           
    
        }

        private void textsize_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SelectedObject != null)
            {

                System.Type t = SelectedObject.GetType();



                if (t == typeof(cLineText))
                {

                    ((cLineText)SelectedObject).font = new Font("ARIAL", Convert.ToInt16(textsize.Text));

                }


                if (t == typeof(cRec))
                {


                    ((cRec)SelectedObject).text.font = new Font("ARIAL", Convert.ToInt16(textsize.Text));
                    ((cRec)SelectedObject).text2.font = new Font("ARIAL", Convert.ToInt16(textsize.Text));

                }

                //planUpdate();

                viewUpdate();
                if (UndoIndex > -1)
                {
                    undolist[UndoIndex] = document.Clone();
                }
            }
        }

        private void roadCbSize_Click(object sender, EventArgs e)
        {
         
        }

        private void roadCbSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SelectedObject != null)
            {

                System.Type t = SelectedObject.GetType();



                if (t == typeof(cRoad))
                {
                    undolist.Add(document.Clone());
                    ((cRoad)SelectedObject).width = Convert.ToInt16(roadCbSize.Text);
                    viewUpdate();
                    
                }




                //planUpdate();

                
            }
        }


        private void selectObjects()
        {
             

        }
        private PointF GetIntersectPoint(cLine L1,cLine L2)
        {
            PointF L1P1 = new PointF(), L1P2 = new PointF(), L2P1 = new PointF(), L2P2 = new PointF();
            L1P1.X = L1.point1.X;
            L1P1.Y = L1.point1.Y;
            L1P2.X = L1.point2.X;
            L1P2.Y = L1.point2.Y;

            L2P1.X = L2.point1.X;
            L2P1.Y = L2.point1.Y;
            L2P2.X = L2.point2.X;
            L2P2.Y = L2.point2.Y;

            PointF functionReturnValue = new System.Drawing.PointF(0, 0); ;
            float Slope1 = 0;
            float Slope2 = 0;
            float YInt1 = 0;
            float YInt2 = 0;

            //Get the slopes
            if ((L1P1.X - L1P2.X) != 0)
            {
                Slope1 = (L1P2.Y - L1P1.Y) / (L1P2.X - L1P1.X);
                //The slope is undefined
            }
            else
            {
                //Both the lines are vertical and don't cross
                if ((L2P2.X - L2P1.X) == 0)
                {
                    functionReturnValue.X = 0;
                    functionReturnValue.Y = 0;
                }
                else
                {
                    //Get Slope2 and YInt2
                    Slope2 = (L2P2.Y - L2P1.Y) / (L2P2.X - L2P1.X);
                    YInt2 = (Slope2 * L2P1.X - L2P1.Y) * -1;
                    //Claculate X and Y
                    functionReturnValue.X = L1P1.X;
                    functionReturnValue.Y = Slope2 * L1P1.X - (Slope2 * L2P1.X - L2P1.Y);
                }
                return functionReturnValue;
            }
            if ((L2P2.X - L2P1.X) != 0)
            {
                Slope2 = (L2P2.Y - L2P1.Y) / (L2P2.X - L2P1.X);
                //The slope is undefined
            }
            else
            {
                //Claculate X and Y
                functionReturnValue.X = L2P1.X;
                functionReturnValue.Y = Slope1 * L2P1.X - (Slope1 * L1P1.X - L1P1.Y);
                return functionReturnValue;
            }

            //Get the Y intercepts
            YInt1 = -(Slope1 * L1P1.X - L1P1.Y);
            YInt2 = -(Slope2 * L2P1.X - L2P1.Y);

            //-B = M * X -Y

            //Check if the lines cross
            //The lines are parallel and don't cross
            if (Slope1 == Slope2 & YInt1 != YInt2)
            {
                functionReturnValue.X = 0;
                functionReturnValue.Y = 0;
                //It is the same line and they cross at every point on that line
            }
            else if (Math.Round(Slope1, 2) == Math.Round(Slope2, 2) & YInt1 == YInt2)
            {
                functionReturnValue.X = L1P1.X;
                functionReturnValue.Y = L1P1.Y;
                //The lines cross some ware
            }
            else
            {
                //Line one is horizontal
                if (Slope1 == 0)
                {
                    functionReturnValue.Y = L1P1.Y;
                    functionReturnValue.X = (YInt2 - L1P1.Y) / Slope2;
                    //Line two is horizontal
                }
                else if (Slope2 == 0)
                {
                    functionReturnValue.Y = L2P1.Y;
                    functionReturnValue.X = (YInt2 - L2P1.Y) / Slope1;
                }
                else
                {
                    functionReturnValue.Y = (YInt1 * (Slope2 / Slope1) - YInt2) / ((Slope2 / Slope1) - 1);
                    functionReturnValue.X = (functionReturnValue.Y - YInt1) / Slope1;
                }
            }
            return functionReturnValue;
        }
        private PointF GetIntersectPoint(PointF L1P1, PointF L1P2, PointF L2P1, PointF L2P2)
        {
            PointF functionReturnValue = new System.Drawing.PointF(0, 0); ;
            float Slope1 = 0;
            float Slope2 = 0;
            float YInt1 = 0;
            float YInt2 = 0;

            //Get the slopes
            if ((L1P1.X - L1P2.X) != 0)
            {
                Slope1 = (L1P2.Y - L1P1.Y) / (L1P2.X - L1P1.X);
                //The slope is undefined
            }
            else
            {
                //Both the lines are vertical and don't cross
                if ((L2P2.X - L2P1.X) == 0)
                {
                    functionReturnValue.X = 0;
                    functionReturnValue.Y = 0;
                }
                else
                {
                    //Get Slope2 and YInt2
                    Slope2 = (L2P2.Y - L2P1.Y) / (L2P2.X - L2P1.X);
                    YInt2 = (Slope2 * L2P1.X - L2P1.Y) * -1;
                    //Claculate X and Y
                    functionReturnValue.X = L1P1.X;
                    functionReturnValue.Y = Slope2 * L1P1.X - (Slope2 * L2P1.X - L2P1.Y);
                }
                return functionReturnValue;
            }
            if ((L2P2.X - L2P1.X) != 0)
            {
                Slope2 = (L2P2.Y - L2P1.Y) / (L2P2.X - L2P1.X);
                //The slope is undefined
            }
            else
            {
                //Claculate X and Y
                functionReturnValue.X = L2P1.X;
                functionReturnValue.Y = Slope1 * L2P1.X - (Slope1 * L1P1.X - L1P1.Y);
                return functionReturnValue;
            }

            //Get the Y intercepts
            YInt1 = -(Slope1 * L1P1.X - L1P1.Y);
            YInt2 = -(Slope2 * L2P1.X - L2P1.Y);

            //-B = M * X -Y

            //Check if the lines cross
            //The lines are parallel and don't cross
            if (Slope1 == Slope2 & YInt1 != YInt2)
            {
                functionReturnValue.X = 0;
                functionReturnValue.Y = 0;
                //It is the same line and they cross at every point on that line
            }
            else if (Math.Round(Slope1, 2) == Math.Round(Slope2, 2) & YInt1 == YInt2)
            {
                functionReturnValue.X = L1P1.X;
                functionReturnValue.Y = L1P1.Y;
                //The lines cross some ware
            }
            else
            {
                //Line one is horizontal
                if (Slope1 == 0)
                {
                    functionReturnValue.Y = L1P1.Y;
                    functionReturnValue.X = (YInt2 - L1P1.Y) / Slope2;
                    //Line two is horizontal
                }
                else if (Slope2 == 0)
                {
                    functionReturnValue.Y = L2P1.Y;
                    functionReturnValue.X = (YInt2 - L2P1.Y) / Slope1;
                }
                else
                {
                    functionReturnValue.Y = (YInt1 * (Slope2 / Slope1) - YInt2) / ((Slope2 / Slope1) - 1);
                    functionReturnValue.X = (functionReturnValue.Y - YInt1) / Slope1;
                }
            }
            return functionReturnValue;
        }


        private bool RecOverRec(cRec srec, cRec rec2)
        {
            
            Rectangle r1 = srec.GetRectangle();
            Rectangle r2 = rec2.GetRectangle();
          
            
            if (isInRectangle(r2, srec.point1)) { return true; }
            if (isInRectangle(r2, srec.point2)) { return true; }
            if (isInRectangle(r2, srec.point3)) { return true; }
            if (isInRectangle(r2, srec.point4)) { return true; }
            

            cLine d1 = new cLine(rec2.point1, rec2.point3);
            cLine d2 = new cLine(rec2.point2, rec2.point4);

            cLine v1 = new cLine(srec.point1, srec.point4);
            cLine v2 = new cLine(srec.point2, srec.point3);
            cLine h1 = new cLine(srec.point1, srec.point2);
            cLine h2 = new cLine(srec.point4, srec.point3);

           

            PointF ip;
            ip = GetIntersectPoint(d2.point1, d2.point2, v1.point1, v1.point2);

          //  this.Text = r2.ToString() + " ----- " + d2.point1.ToString() + "---" + d2.point2.ToString() + " ---ip= " + ip.ToString();

            if (isInRectangle(r2, ip))
            
            {
               // this.Text = r2.ToString() + " ----- " + ip.ToString();

                return true;}
          
            
 /*
            ip = GetIntersectPoint(d1, v2);
            if (isInRectangle(r2, ip)) { return true; }
           
            ip = GetIntersectPoint(d1, h1);
            if (isInRectangle(r1, ip)) { return true; }

            ip = GetIntersectPoint(d1, h2);
            if (isInRectangle(r1, ip)) { return true; }

            ip = GetIntersectPoint(d2, v1);
            if (isInRectangle(r1, ip)) { return true; }

            ip = GetIntersectPoint(d2, v2);
            if (isInRectangle(r1, ip)) { return true; }

            ip = GetIntersectPoint(d2, h1);
            if (isInRectangle(r1, ip)) { return true; }

            ip = GetIntersectPoint(d2, h2);
            if (isInRectangle(r1, ip)) { return true; }
            */
            return false ;
        }

        private void SelectSwitch(object o)
        {

            /*
            if (gettype(o) == "REC") {  oppositeBoolean (ref ((cRec)o).selected ); }
            if (gettype(o) == "SYMBOL") { oppositeBoolean(ref ((cSymbol)o).selected); }
            if (gettype(o) == "LINE") { oppositeBoolean(ref ((cLine)o).selected ); }
            if (gettype(o) == "LINETEXT") {  oppositeBoolean (ref ((cLineText)o).selected ); }
            if (gettype(o) == "ROAD") {  oppositeBoolean (ref ((cRoad)o).selected ); }
            if (gettype(o) == "TEXT") {  oppositeBoolean (ref ((cText)o).selected ); }
            */

        }

        private void oppositeBoolean(ref bool value)
        {
            if (value == true) { value = false; } else { value = true; }


        }

        private void setSelected(object o,bool value)
        {

            if (gettype(o) == eTools.Rec) {  ((cRec)o).selected=value; }
            if (gettype(o) == eTools.Symbol) { ((cSymbol )o).selected = value; }
            if (gettype(o) == eTools.Line) { ((cLine)o).selected = value; }
            if (gettype(o) == eTools.LineText) { ((cLineText)o).selected = value; }
            if (gettype(o) == eTools.Road) { ((cRoad)o).selected = value; }
            if (gettype(o) == eTools.Text) { ((cText)o).selected = value; }

        }

        public System.Drawing.Rectangle ZaglavljeGetRectangle()
        {

            int w =document.zaglavlje.width;
            int h =document.zaglavlje.items.Count * document.zaglavlje.rowHeight;


            int x1 = plan.Width - document.zaglavlje.width;
            int y1 =plan.Height - h;

            return new System.Drawing.Rectangle(x1, y1, w, h);

        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Properties.Settings.Default.Save();

        }

        private cOpisRadoca FazeRada = new cOpisRadoca();
        private cOpisRadoca Materijal = new cOpisRadoca();
        private cOpisRadoca DemMaterijal = new cOpisRadoca();

        private cSlike Slike;

        private void btnFazeRada_Click(object sender, EventArgs e)
        {

            string stab = ((ToolStripButton)sender).Tag.ToString();
            btnFazeRada.Checked = false;
            btnMaterijal.Checked = false;
            btnDemMaterijal.Checked = false;
            btnSlike.Checked = false;
            btnNacrt.Checked = false;

            ((ToolStripButton)sender).Checked = true;
            showTab(stab);
            //return;

            //panel1.Controls.Remove(Nacrt);
            //panel1.Controls.Remove(Slike);
            //panel1.Controls.Add(FazeRada);

            //FazeRada.Width = panel1.Width;
            //FazeRada.Height = panel1.Height;
            //FazeRada.Dock = DockStyle.Fill;
            //btnNacrt.Checked = false ;
            //btnSlike.Checked = false;
            //btnFazeRada.Checked = true;
            //SetEditorTools("FAZERADA");

        }

        private void showTab(string sTab)
        {
            panel1.Controls.Remove(Nacrt);
            panel1.Controls.Remove(DemMaterijal );
            panel1.Controls.Remove(Materijal );
            panel1.Controls.Remove(FazeRada);
            panel1.Controls.Remove(Slike);
           
            switch (sTab)
            {
                case "FAZERADA":
                    panel1.Controls.Add(FazeRada);FazeRada.Dock=DockStyle.Fill;
                break;
                case "MATERIJAL":
                panel1.Controls.Add(Materijal);Materijal.Dock=DockStyle.Fill;
                break;
                case "DEMMATERIJAL":
                panel1.Controls.Add(DemMaterijal );DemMaterijal.Dock=DockStyle.Fill ;
                break;
                case "SLIKE":
                panel1.Controls.Add(Slike);Slike.Dock=DockStyle.Fill ;
                break;
                case "NACRT":
                panel1.Controls.Add(Nacrt); Nacrt.Dock = DockStyle.Fill;
                break;

            }

        }
        private void btnNacrt_Click(object sender, EventArgs e)
        {
             
        }

        private void opisRadovaChanged(object sender, EventArgs e)
        {
            TextBox TB =(TextBox ) sender;

            if (TB.Tag.ToString() == "FAZERADA")
            {
                document.fazeRada = TB.Text;
            }

            if (TB.Tag.ToString() == "MATERIJAL")
            {
                document.materijal = TB.Text;
            }

            if (TB.Tag.ToString() == "DEMMATERIJAL")
            {
                document.demMaterijal = TB.Text;
            }
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            FazeRada.Width = panel1.Width;
            FazeRada.Height = panel1.Height;
            FazeRada.dg1.Columns[0].Width = 70;
            FazeRada.dg1.Columns[1].Width  = this.Width / 2;

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
        
        }

        private void toolbar_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
        
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            document.updateCodeImage();
            plan.BackgroundImage = document.codeImage;

        }

        private void datotekaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
        
        FolderBrowserDialog fb = new FolderBrowserDialog();
           
        private void btnSaveAll_Click(object sender, EventArgs e)
        {

            if (document.zaglavlje.items[0].text == "" | document.zaglavlje.items[0].text == null)
            {
                MessageBox.Show("Nedostaje broj radnog naloga");
                return;

            }
            if (document.zaglavlje.items[1].text == "" | document.zaglavlje.items[0].text == null)
            {
                MessageBox.Show("Potrebno je unjeti adresu");
                return;

            }
            fb.SelectedPath  = Properties.Settings.Default.openDir;
            if (fb.ShowDialog() == System.Windows.Forms.DialogResult.Cancel) return;
            string newFolder=fb.SelectedPath + @"\"+ document.zaglavlje.items[0].text + "  " + document.zaglavlje.items[1].text.Trim().ToUpper();
            System.IO.Directory.CreateDirectory(newFolder);
          
            string projectPath =System.IO.Path.Combine(newFolder  , (document.zaglavlje.items[0].text + "  " + document.zaglavlje.items[1].text).Trim().ToUpper() + ".prs");
            string imagePath = System.IO.Path.Combine(newFolder , (document.zaglavlje.items[0].text + "  " + document.zaglavlje.items[1].text).Trim().ToUpper() + " (SKICA).png");
            string textPath = System.IO.Path.Combine(newFolder , (document.zaglavlje.items[0].text + "  " + document.zaglavlje.items[1].text).Trim().ToUpper() + " (FAZE RADA).txt");

            document.fazeRada = izveziIzDg(FazeRada.dg1);
            document.materijal = izveziIzDg(Materijal.dg1);
            document.demMaterijal = izveziIzDg(DemMaterijal.dg1);

            document.save(projectPath  );
            Properties.Settings.Default.openDir =fb.SelectedPath ;
           
            saveImage(imagePath);

             
            string folderPath = System.IO.Path.GetDirectoryName(projectPath ) + @"\slike";
            int i=0;
            foreach(Bitmap b in document.slike ){
                i++;
                b.Save(folderPath + @"\slika " + i.ToString() + ".png");
            }

            StreamWriter SW = new StreamWriter(textPath);
            SW.Write(izveziText());
            SW.Dispose();


        }
        private string izveziIzDg(DataGridView dg)
        {
            string rstring="";
            foreach(DataGridViewRow dr in dg.Rows ){
                if (dr.Cells[3].Value != null)
                {
                    if (dr.Cells[3].Value.ToString() != "")
                    {
                        rstring += dr.Cells[0].Value.ToString() + "|" + dr.Cells[3].Value.ToString() + Environment.NewLine;

                    }
                }

            }
            return rstring;

        }


        private string izveziText(DataGridView dg,string naslov)
        {

            string text = naslov +Environment.NewLine  ;
            foreach (DataGridViewRow dr in dg.Rows)
            {
                if (dr.Cells[3].Value != null)
                {
                    if (dr.Cells[3].Value.ToString() != "")
                    {
                        text += dr.Cells[0].Value.ToString() + "   " + dr.Cells[2].Value.ToString() + "=" + dr.Cells[3].Value.ToString() + "    (" + dr.Cells[1].Value.ToString() + ")" + Environment.NewLine;
                    }
                }
            }
            if (text == naslov + Environment.NewLine) return "";
            text += Environment.NewLine;
            return text;
        }

        private string izveziText()
        {
            string text = "";
            text += izveziText(FazeRada.dg1, "FAZE RADA");
            text += izveziText(Materijal.dg1, "UTROŠENI MATERIJAL");
            text += izveziText(DemMaterijal.dg1, "DEMONTIRANI MATERIJAL");
            return text;
        }

        private void btnSlike_Click(object sender, EventArgs e)
        {
            panel1.Controls.Remove(Nacrt );
            panel1.Controls.Remove(FazeRada);
           
           
            panel1.Controls.Add(Slike);
             
            Slike.Width = panel1.Width;
            Slike.Height = panel1.Height;

            btnNacrt.Checked = false;
            btnFazeRada.Checked = false;
            btnSlike.Checked = true;
            

            SetEditorTools(eTools.Slike);
        }

        private void velicinaDokumentaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DokSize f = new DokSize();
            f.txtWidth.Text = document.paperSize.Width.ToString();
            f.txtHeight.Text = document.paperSize.Height.ToString();
            if (f.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //document.ChangeSize(f.docWidth, f.docHeight);

                Size docSize = new Size(Convert.ToInt16(f.txtWidth.Text), Convert.ToInt16(f.txtHeight.Text));

                

                document.paperSize = docSize;
                document.image = new Bitmap(document.paperSizePix.Height, document.paperSizePix.Width);
                document.codeImage = new Bitmap(document.paperSizePix.Height, document.paperSizePix.Width);

                drawscale();

                bmpFocus = new Bitmap(document.paperSizePix.Height, document.paperSizePix.Width, PixelFormat.Format32bppArgb);

                bmp = new Bitmap(document.paperSizePix.Height, document.paperSizePix.Width);
                //viewUpdate();

                setPaperSize(document.paperSize);

                viewUpdate();
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {

        }

        private void btnEraser_Click(object sender, EventArgs e)
        {

        }
    }
}
