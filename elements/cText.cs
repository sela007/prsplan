﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing ;
namespace PRSPLAN
{

    [Serializable]
    public class cText
    {

       public string text;

       public  Point point = new Point(0, 0);
       public bool selected = false;
       public string ObjType = "TEXT";
       public  Font font = new Font("ARIAL", 12);
       public Color color = Color.Black;
       public cText(string stext)
        {
            text = stext;
        }
        public cText(string stext,Point p)
        {
            text = stext;
            point = p;
        }
        public cText(string stext, Rectangle rec)
        {
            text = stext;
            point = new Point(rec.X ,rec.Y);
        }
          public void  drawon(Bitmap b,Graphics g)
          {
              //Graphics g = Graphics.FromImage(b);
              g.DrawString(text, font, new SolidBrush(color), point);
              //g.Dispose();


          }

          public void drawMarkerOn(Bitmap b, Graphics g)
          {

           
          }

          public void drawCode(Graphics g,int code)
          {
              Color color = MyColors.getColorByCode(code, MyColors.codeRec);
               
              g.FillRectangle( new SolidBrush(color) , getRectangle());
          }

          public Rectangle getRectangle()
          {
               
              Bitmap b=new Bitmap(2,2);
              using (Graphics g = Graphics.FromImage(b))
              {
                  float x = g.MeasureString(text, font).Width;
                  float y = g.MeasureString(text, font).Height;
                  return new Rectangle(point, new Size((int)x, (int)y));
              }
          }


    }
}
