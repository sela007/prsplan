﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace PRSPLAN
{

    [Serializable]
    public class cSymbol
    {
        public bool selected = false;
        public string ObjType = "SYMBOL";
        public Image image;
        public cPoint pointref = new cPoint(0, 0);
        public Point point {
            get { return new Point(pointref.X, pointref.Y); }
            set { pointref.X = value.X; pointref.Y = value.Y; setRectangle(); }
        }

        private Size _size = new Size(24, 24);
        public Size size{
            get{return _size ;}
            set { _size = value; setRectangle(); }
        }
        
        public cSymbol(Image i)
        {
            image = i;
        }

        public void drawon(Bitmap b,Graphics g) 
        {
            //using (Graphics g = Graphics.FromImage(b))
            //{
                Rectangle rec = new Rectangle(point.X - size.Width / 2, point.Y - size.Height / 2, size.Width, size.Height);
                rectangle = rec;
                g.DrawImage (image, rec);
                // g.DrawRectangle(Pens.AliceBlue, rectangle );

            //}
        }
        public void drawCode(Graphics g, int code)
        {
            Color color = MyColors.getColorByCode(code, MyColors.codeSymbol );
             
            g.FillRectangle (new SolidBrush(color),rectangle  );
        }

        public void drawMarkerOn(Bitmap b, Graphics g)
        {
            //using (Graphics g = Graphics.FromImage(b))
            //{
            Rectangle rec = new Rectangle(point.X-2-  size.Width / 2, point.Y-2 - size.Height / 2, size.Width+2, size.Height+2);
            rectangle = rec;
            
           // g.DrawEllipse(new Pen(new SolidBrush(MyColors.selectedColor)), rec);

            g.DrawRectangle(new Pen(new SolidBrush(MyColors.selectedColor)), rectangle);

            //}
        }

        public Rectangle rectangle = new Rectangle(-1, -1, 0, 0);
        public void setRectangle() {
            return;
            Point p = new Point();
            p.X = point.X - size.Width / 2;
            p.Y = point.Y - size.Height / 2;
            rectangle.X = p.X;
            rectangle.Y = p.Y;
            rectangle.Width = size.Width;
            rectangle.Height = size.Height;

        }

    }


}
