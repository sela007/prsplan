﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing ;

namespace PRSPLAN
{
    [Serializable]
    public  class cLine
    {
         
        public int width = 2;
        public bool onlyText = false;
        [NonSerialized ]
        private bool _selected = false;
        
        public bool selected { get { return _selected; }
            set { if (value != _selected) {
                if (value == false) settings.selectedObjects.Remove(this);
                if (value == true) settings.selectedObjects.Add(this);
                _selected = value; } }
        }

        public string text1 { get; set; }
        public string text2 { get; set; }

        public string ObjType = "LINE";
        public cPoint pointref1 = new cPoint(0, 0);
        public Point point1
        {
            get { return new Point(pointref1.X, pointref1.Y); }
            set { pointref1.X = value.X; pointref1.Y = value.Y; }
        }

        public cPoint pointref2 = new cPoint(0, 0);
        public Point point2
        {
            get { return new Point(pointref2.X, pointref2.Y); }
            set { pointref2.X = value.X; pointref2.Y = value.Y; }
        }

        public Color color;
        public System.Drawing.Drawing2D.DashStyle dashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
  
        public cLine(Point p1,Point p2)
        {
            point1 = p1;
            point2 = p2;
            color = Color.Black;
        }
        public cLine(Point p1, Point p2,Color c)
        {
            point1 = p1;
            point2 = p2;
            color = c;
        }

          public void drawon(Bitmap b,Graphics g) 
        {

           // using (Graphics g = Graphics.FromImage(b))
            //{
                Pen lp = new Pen(new SolidBrush(color));
                lp.Width = width;
                lp.DashStyle = dashStyle;
           if (onlyText != true) g.DrawLine(lp, point1, point2);
                Pen lp2 = new Pen(Brushes.Black );

                int rs = 4;

              Rectangle r=new Rectangle(point1.X-2, point1.Y-2,rs,rs);
              Rectangle r2=new Rectangle(point2.X-2,point2.Y-2,rs,rs);

          if (onlyText != true)   g.FillEllipse(Brushes.DimGray , r);
          if (onlyText != true)   g.FillEllipse(Brushes.DimGray, r2);

              drawOnLine(point1, point2, b);
            //}

        }
          public void drawMarkerOn(Bitmap b, Graphics g)
          {

              // using (Graphics g = Graphics.FromImage(b))
              //{
              Pen lp = new Pen(new SolidBrush(MyColors.selectedColor ));
              lp.Width = width;
              lp.DashStyle = dashStyle;
              g.DrawLine(lp, point1, point2);
              //}

          }

          public void drawCode(Graphics g, int code)
          {
              Color color = MyColors.getColorByCode(code, MyColors.codeLine);
              Pen lp = new Pen(new SolidBrush(color));
              lp.Width = MyColors.koefLine;
              g.DrawLine(lp, point1, point2);
          }




          private void drawOnLine(Point iP1, Point iP2, Bitmap b)
          {
              if (text1 ==null | text2 ==null ) { return; }
              if (text1 =="" && text2 =="" ) { return; }


              Point P1;
              Point P2;

              if (iP1.X <= iP2.X) { P1 = iP1; P2 = iP2; }
              else
              {
                  P1 = iP2; P2 = iP1;
              }

              float ang = getLineAngle(P1, P2);
               
              Font f = new Font("TAHOMA", 7);
              Brush br = new SolidBrush(Color.Gray );
              SizeF size1;
              SizeF size2;

              int x1, y1, x2, y2;
              x1 = P1.X;
              y1 = P1.Y;
              x2 = P2.X;
              y2 = P2.Y;

              int recW, recH, recX, recY;
              if (x1 >= x2) { recW = x1 - x2; recX = x2; } else { recW = x2 - x1; recX = x1; }
              if (y1 >= y2) { recH = y1 - y2; recY = y2; } else { recH = y2 - y1; recY = y1; }


              if (recW == 0) recW = 1;
              if (recH == 0) recH = 1;


              using (Graphics g = Graphics.FromImage(b))
              {
                  size1 = g.MeasureString(text1, f);
                  size2 = g.MeasureString(text2, f);
              }
              float txtWidth;

              if (size1.Width >= size2.Width) { txtWidth = size1.Width; }
              else { txtWidth = size2.Width; }

              float txtHeight = size1.Height;

              // Bitmap bx=new Bitmap(recW*2,recH *2);
              Bitmap bx = new Bitmap((int)txtWidth * 2, (int)txtWidth * 2);

              using (Graphics g = Graphics.FromImage(bx))
              {

                  float textX = (bx.Width / 2) - (txtWidth / 2);
                  float textY = (bx.Height / 2) - (txtHeight / 2);

                  //move rotation point to center of image
                  g.TranslateTransform((float)bx.Width / 2, (float)bx.Height / 2);
                  //rotate
                  g.RotateTransform(ang);
                  //move image back
                  g.TranslateTransform(-(float)bx.Width / 2, -(float)bx.Height / 2);

                  g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                  g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                  g.DrawString(text1, f, br, new PointF(textX, textY));
                  g.DrawString(text2, f, br, new PointF(textX + txtWidth / 2 - size2.Width / 2, textY + txtHeight + 3));
              }

              int RAZ = (int)txtHeight / 2;
              int drawX, drawY;
              if (x2 >= x1)
              {
                  drawX = ((recX + recW) - recW / 2) - bx.Width / 2 - RAZ;
              }
              else
              {
                  drawX = ((recX + recW) - recW / 2) - bx.Width / 2 + RAZ;
              }


              if (y1 >= y2)
              {
                  drawY = ((recY + recH) - recH / 2) - bx.Height / 2 - RAZ;
              }
              else
              {
                  drawY = ((recY + recH) - recH / 2) - bx.Height / 2 - RAZ;
                  drawX = ((recX + recW) - recW / 2) - bx.Width / 2 + RAZ;
              }

              using (Graphics g = Graphics.FromImage(b))
              {
                  g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

                  //g.DrawImage(bx, new Point((int)-size.Height / 2, (int)-size.Height / 2));
                  g.DrawImage(bx, new Point(drawX, drawY));
              }

          }

          private float getLineAngle(Point p1, Point p2)
          {
              float Angle = (float)(Math.Atan2(p2.Y - p1.Y, p2.X - p1.X) * 180 / Math.PI);
              return Angle;


          }
    }
}
