﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace PRSPLAN
{
    [Serializable]
    public  class cLineText
    {
        public cLineText(Point p1,Point p2,Font f)
        {
            point1 = p1;
            point2 = p2;
            font = f;
        }

         public string ObjType = "LINETEXT";
        private System.Drawing.Drawing2D.DashStyle dashStyle = System.Drawing.Drawing2D.DashStyle.Solid;

        private Font _font= new Font("ARIAL", 8);

        public Font font{get{return _font; }
            set{_font=value ;
            text1Size = System.Windows.Forms.TextRenderer.MeasureText(text1 , font);
             text2Size = System.Windows.Forms.TextRenderer.MeasureText(text2, font);
        }}

        
        public Color color = Color.Gray  ;
        public Color fontColor = Color.FromArgb(70,70,200) ;  
        public cPoint pointref1 = new cPoint(0, 0);
        public Point point1
        {
            get { return new Point(pointref1.X, pointref1.Y); }
            set { pointref1.X = value.X; pointref1.Y = value.Y; }
        }

        public cPoint pointref2 = new cPoint(0, 0);
        public Point point2
        {
            get { return new Point(pointref2.X, pointref2.Y); }
            set { pointref2.X = value.X; pointref2.Y = value.Y; }
        }

        private string _text1;
        private string _text2;
        public Size text1Size=new Size (0,0);
        public Size text2Size = new Size(0, 0);

        public string text1{get{return _text1 ;}
            set { _text1 = value;
            text1Size = System.Windows.Forms.TextRenderer.MeasureText(value , font);
            }
        }

        public string text2
        {
            get { return _text2; }
            set
            {
                _text2 = value;
                text2Size = System.Windows.Forms.TextRenderer.MeasureText(value, font);
            }
        }
        public bool selected=false ;

   
        public void drawon(Bitmap b,Graphics g)
        {
                Pen lp = new Pen(new SolidBrush(color));
                lp.Width = 1;   
                lp.StartCap = System.Drawing.Drawing2D.LineCap.ArrowAnchor;
                Size txtSize;
                if (text1Size.Width > text2Size.Width) { txtSize = text1Size; } else { txtSize = text2Size; }
                Color c =Color.FromArgb (color.A,color.R,color.G ,color.B );
                
                Pen lp2 = new Pen(new SolidBrush(c));
                lp2.Width = 1;
                lp.DashStyle = dashStyle;
                g.DrawLine(lp, point1, point2);
                if (text1 != "" | text2 != "")
                {

                    if (point2.X >= point1.X)
                    {

                        if (point1 != point2) { g.DrawLine(lp2, point2, new Point(point2.X + txtSize.Width, point2.Y)); }

                        g.DrawString(text1, font, new SolidBrush(fontColor), new PointF(point2.X, point2.Y - txtSize.Height));
                        g.DrawString(text2, font, new SolidBrush(fontColor), new PointF(point2.X, point2.Y + 2));

                    }
                    else
                    {
                        if (point1 != point2) { g.DrawLine(lp2, point2, new Point(point2.X - txtSize.Width, point2.Y)); }

                        g.DrawString(text1, font, new SolidBrush(fontColor), new PointF(point2.X - txtSize.Width, point2.Y - txtSize.Height));
                        g.DrawString(text2, font, new SolidBrush(fontColor), new PointF(point2.X - txtSize.Width, point2.Y + 2));

                    }
                }

        }

        public Rectangle getRectangle()
        {
            int W=0;
            int X = 0;
            if (text1Size.Width >= text2Size.Width) { W = text1Size.Width; }else{W=text2Size.Width;}

            if (text1Size.Width >= text2Size.Width) { W = text1Size.Width; } else { W = text2Size.Width; }
            if (point1.X <= point2.X) { X = point2.X; } else { X = point2.X - W; }
            
            return new Rectangle(new Point(X, point2.Y - text1Size.Height),new Size( W, text1Size.Height * 2));
        
        }
        

        public void drawMarkerOn(Bitmap b, Graphics g)
        {
            System.Diagnostics.Debug.Print(DateTime.Now.ToString());
            
            Pen lp = new Pen(new SolidBrush(MyColors.selectedColor));
            lp.Width = 1;
            lp.StartCap = System.Drawing.Drawing2D.LineCap.ArrowAnchor;
            Size txtSize;
            if (text1Size.Width > text2Size.Width) { txtSize = text1Size; } else { txtSize = text2Size; }
            Pen lp2 = new Pen(new SolidBrush(MyColors.selectedColor));
            lp2.Width = 1;
            lp.DashStyle = dashStyle;
            g.DrawLine(lp, point1, point2);
            if (text1 != "" | text2 != "")
            {

                if (point2.X >= point1.X)
                {

                    if (point1 != point2) { g.DrawLine(lp2, point2, new Point(point2.X + txtSize.Width, point2.Y)); }

                    g.DrawString(text1, font, new SolidBrush(fontColor), new PointF(point2.X, point2.Y - txtSize.Height));
                    g.DrawString(text2, font, new SolidBrush(fontColor), new PointF(point2.X, point2.Y + 2));

                }
                else
                {
                    if (point1 != point2) { g.DrawLine(lp2, point2, new Point(point2.X - txtSize.Width, point2.Y)); }

                    g.DrawString(text1, font, new SolidBrush(fontColor), new PointF(point2.X - txtSize.Width, point2.Y - txtSize.Height));
                    g.DrawString(text2, font, new SolidBrush(fontColor), new PointF(point2.X - txtSize.Width, point2.Y + 2));

                }
            }
             
        }

        public void drawCode(Graphics g, int  code)
        {
            Color color = MyColors.getColorByCode( code, MyColors.codeLineText);
            Pen lp = new Pen(new SolidBrush(color));
            lp.Width = MyColors.koefLine;
            g.FillRectangle ( new SolidBrush(color) , getRectangle());
            g.DrawLine(lp, point1, point2);
        }
        
    }
}
