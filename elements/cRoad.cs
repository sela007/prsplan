﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing ;
namespace PRSPLAN
{

    [Serializable]
    public class cRoad
    {
        public bool selected = false;
        public string ObjType = "ROAD";
        public  List<cPoint> points = new List<cPoint>();
        public List<boxString> texts;
        public cPoint currentPoint = null;
        public int width = 90;
        public void drawon(Bitmap b,Graphics g,bool drawDots =true )
        {

            Point[] ps = GetPoints();
            if (points.Count < 2) { return; }
           // using (Graphics g = Graphics.FromImage(b))
           // {
                Pen lp = new Pen(new SolidBrush(MyColors.road));
                lp.Width = width;
                g.DrawLines(lp, ps);
                //g.DrawClosedCurve (lp,ps);
              
                if (texts == null) createTexts();
                Pen cp = new Pen(new SolidBrush(Color.DarkGray));
                int i = -1;
                foreach (Point p in ps)
                {
                    i++;
                    if (i<points.Count -1){
                        cRotatedText rt = new cRotatedText(points[i].ToPoint(), points[i + 1].ToPoint());
                        rt.text.value  = texts[i].value ;
                        rt.drawon(b, g);
                        }
                   // if (i != 1) { 
                  if (drawDots)   g.DrawEllipse(cp, p.X - 3, p.Y - 3, 6, 6); 

                    //g.FillEllipse( new SolidBrush(Color.White) ,p.X-3,p.Y-3,6,6);
                }
     
           // }
        }

        public void addPoint(int x, int y)
        {
            if (texts == null) createTexts();
 
            points.Add(new cPoint(x, y));
            if (points.Count > 1)
            {
                boxString  txt =new boxString ( "");
                texts.Add(txt);

            }
        }
        public void drawCode(Graphics g, int code)
        {
            Color color = MyColors.getColorByCode(code, MyColors.codeRoad);
            Point[] ps = GetPoints();
            if (points.Count < 2) { return; }
          
            Pen lp = new Pen(new SolidBrush(color));
            lp.Width = width;
            g.DrawLines(lp, ps);
             
        }

        public void drawMarkerOn(Bitmap b, Graphics g)
        {

            Point[] ps = GetPoints();
            if (points.Count < 2) { return; }
            // using (Graphics g = Graphics.FromImage(b))
            // {
            Pen lp = new Pen(new SolidBrush(MyColors.road));
            lp.Width = width;
            g.DrawLines(lp, ps);
            //g.DrawCurve(lp, points.ToArray());

            Pen cp = new Pen(new SolidBrush(MyColors.selectedColor ));
            int i = -1;
            foreach (Point p in ps)
            {
                 
                // if (i != 1) { 
                i++;
                if (i < points.Count - 1)
                {
                    cRotatedText rt = new cRotatedText(points[i].ToPoint(), points[i + 1].ToPoint());
                    rt.text.value  = texts[i].value ;
                    rt.drawon(b, g);
                }
                g.DrawEllipse(cp, p.X - 3, p.Y - 3, 6, 6);
                //g.FillEllipse( Brushes.Red   ,p.X-3,p.Y-3,6,6);
            }
            if (currentPoint != null) g.FillEllipse(Brushes.Red, currentPoint.X-3, currentPoint.Y  - 3, 6, 6);

            // }
        }
        private Point[] GetPoints() 
        {
            Point[] ps = new Point[ points.Count  ];
            int i = -1;
            foreach (cPoint p in points) 
            {
                i++;
                ps[i] = new Point(p.X, p.Y);
            }

            return ps;
        }
        public boxString GetNearestTextPoint(Point pp)
        {
            cPoint p = new cPoint(pp.X, pp.Y);
            boxString b = new boxString("");
            int y = 0;
            int x = 0;
            int sum = 10000;
            int i = -1;
            foreach (cPoint pi in points )
            {
                i++;
                y = Math.Abs(pi.Y - p.Y);
                x = Math.Abs(pi.X - p.X);

                if (i < points.Count - 1)
                {
                    if (x + y < sum)
                    {
                        b = texts[i];
                        sum = x + y;
                        currentPoint = points[i];
                    }
                }
            }

            return b;

        }

        private void createTexts()
        {
            int i = -1;
            texts = new List<boxString>();
            foreach (cPoint p in points)
            {
                i++;
                if (i<=points.Count-2)
                texts.Add(new boxString(""));
            }
        }

    }


}
