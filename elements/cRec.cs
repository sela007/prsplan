﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
namespace PRSPLAN
{
    [Serializable]
    public class cRec
    {

        public cLineText text;

        public cLineText text2;

        public Color color=Color.Black;
        public int width=1;
        public string ObjType = "REC";
        public Font font = new Font("ARIAL", 10);
         
        public System.Drawing.Drawing2D.DashStyle dashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
        
        [NonSerialized]
        private bool _selected = false;

        public bool selected
        {
            get { return _selected; }
            set
            {
                if (value != _selected)
                {
                    if (value == false) settings.selectedObjects.Remove(this);
                    if (value == true) settings.selectedObjects.Add(this);
                    _selected = value;
                }
            }
        }
        public cRec(Point p1, Point p3, Color c)
        {
            pointref1.PointChanged += new cPoint.PointChangedEventHandler(Point1Changed);
            pointref2.PointChanged += new cPoint.PointChangedEventHandler(Point2Changed);
            pointref3.PointChanged += new cPoint.PointChangedEventHandler(Point3Changed);
            pointref4.PointChanged += new cPoint.PointChangedEventHandler(Point4Changed);
            
            point1 = p1;
            point3 = p3;
            color = c;
            text = new cLineText(point1, point1, new Font("ARIAL", 10));
            text.fontColor = color;
            text.text1 = "";
            text2= new cLineText(p1, p1, new Font("ARIAL", 10));
            text.color = color;
            text2.text1 = "";
        }

         

        public cPoint pointref1 = new cPoint(0, 0);
        public Point point1 {
            get { return new Point(pointref1.X, pointref1.Y); }
            set { pointref1.X = value.X; pointref1.Y = value.Y;
            pointref2.Y = value.Y;
            pointref4.X = value.X;
            }
        }

        public  cPoint pointref2 = new cPoint(0, 0);
        public Point point2
        {
            get { return new Point(pointref2.X, pointref2.Y); }
            set { pointref2.X = value.X; pointref2.Y = value.Y;
            pointref1.Y = value.Y;
            pointref3.X = value.X;
            }
        }

        public cPoint pointref3 = new cPoint(0, 0);
        public Point point3
        {
            get { return new Point(pointref3.X, pointref3.Y); }
            set { pointref3.X = value.X; pointref3.Y = value.Y;
            pointref2.X = value.X;
            pointref4.Y = value.Y;
            }
        }

        public cPoint pointref4 = new cPoint(0, 0);
        public Point point4
        {
            get { return new Point(pointref4.X, pointref4.Y); }
            set { pointref4.X = value.X; pointref4.Y = value.Y;
            pointref1.X = value.X;
            pointref3.Y = value.Y;
            }
        }


        private void Point1Changed() { point1 = new Point(pointref1.X, pointref1.Y); }
        private void Point2Changed() { point2= new Point(pointref2.X, pointref2.Y); }
        private void Point3Changed() { point3 = new Point(pointref3.X, pointref3.Y); }
        private void Point4Changed() { point4 = new Point(pointref4.X, pointref4.Y); }
       
        public void drawon(Bitmap b,Graphics g) 
        {
         //using(Graphics  g = Graphics.FromImage(b))
          //{
            Pen lp = new Pen(new SolidBrush(color ));
            if (selected) { lp.Color = MyColors.selectedColor; lp.Brush = Brushes.Pink; }

            lp.Width = width ;
            lp.DashStyle = dashStyle;
            Point[] pss={point1,point2 ,point3,point4 };
            g.DrawPolygon(lp,pss  );

            Rectangle rec=GetRectangle();

            int x=0;
            x=rec.X + rec.Width / 2;
            x=x-text.text1Size.Width /2;

            int y = 0;
            y = rec.Y+ rec.Height / 2;
            y = y - text.text1Size.Height / 2;

            g.DrawString(text.text1, text.font,new SolidBrush( text.fontColor), x, y);
            g.DrawString(text2.text1, text2.font,new SolidBrush( text2.fontColor),rec.X , rec.Y );
           

            //g.DrawRectangle(lp, rectangle);
          //}
        }
        public void drawMarkerOn(Bitmap b, Graphics g)
        {
        
            g.DrawRectangle(new Pen(new SolidBrush(MyColors.selectedColor)),GetRectangle () );

       
        }

        public void drawCode(Graphics g, int code)
        {
            Color color = MyColors.getColorByCode(code, MyColors.codeRec );
            Rectangle rec = GetRectangle();
            
            g.FillRectangle ( new SolidBrush(color ) , GetRectangle());
        }


        public Rectangle GetRectangle()
        {

            Point p1=point1 ;
            Point p2=point1 ;
            if (point1.Y <= point4.Y)
            {
                if (point1.X <= point2.X)
                {
                    p1 = point1;
                }
                else { p1 = point2; }
            }
            else
            {
                if (point4.X <= point3.X) { p1 = point4; } else { p1 = point3; }
            }


            if (p1 == point1) { p2 = point3; }
            if (p1 == point2) { p2 = point4; }
            if (p1 == point3) { p2 = point1; }
            if (p1 == point4) { p2 = point2; }


            return new Rectangle(p1.X, p1.Y, p2.X - p1.X, p2.Y - p1.Y);

        }


    }
}
