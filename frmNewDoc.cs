﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PRSPLAN
{
    public partial class frmNewDoc : Form
    {
        public frmNewDoc()
        {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            int res=0;

            if (Int32.TryParse(txtSirina.Text, out res) == false | Int32.TryParse(txtVisina.Text, out res) == false)
            {
                MessageBox.Show("Pogrešan unos (veličina dokumenta)", "");
                return;
            }

            if(txtWWMS.Text !=""){
                if (Int32.TryParse(txtWWMS.Text, out res) == false | txtWWMS.Text.Length != 8 )
                {
                    MessageBox.Show("Pogrešan unos (WWMS)", "");
                    return;
                }
            }

            Properties.Settings.Default.docSirina = txtSirina.Text;
            Properties.Settings.Default.docVisina= txtVisina.Text;

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel ;
            this.Close();
        }

        private void txtSirina_TextChanged(object sender, EventArgs e)
        {

        }

        private void frmNewDoc_Load(object sender, EventArgs e)
        {
            txtSirina.Text = Properties.Settings.Default.docSirina;
            txtVisina.Text = Properties.Settings.Default.docVisina;
            txtDatum.Text = DateTime.Now.ToString("dd.MM.yyyy");
           
        }
    }
}
