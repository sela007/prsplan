﻿namespace PRSPLAN
{
    partial class frmZag
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TXT1 = new System.Windows.Forms.TextBox();
            this.TXT2 = new System.Windows.Forms.TextBox();
            this.TXT3 = new System.Windows.Forms.TextBox();
            this.TXT4 = new System.Windows.Forms.TextBox();
            this.TXT5 = new System.Windows.Forms.TextBox();
            this.TXT6 = new System.Windows.Forms.TextBox();
            this.TXT7 = new System.Windows.Forms.TextBox();
            this.TXT8 = new System.Windows.Forms.TextBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.T8 = new System.Windows.Forms.TextBox();
            this.T7 = new System.Windows.Forms.TextBox();
            this.T6 = new System.Windows.Forms.TextBox();
            this.T5 = new System.Windows.Forms.TextBox();
            this.T4 = new System.Windows.Forms.TextBox();
            this.T3 = new System.Windows.Forms.TextBox();
            this.T2 = new System.Windows.Forms.TextBox();
            this.T1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // TXT1
            // 
            this.TXT1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXT1.Location = new System.Drawing.Point(175, 12);
            this.TXT1.Name = "TXT1";
            this.TXT1.Size = new System.Drawing.Size(331, 26);
            this.TXT1.TabIndex = 0;
            // 
            // TXT2
            // 
            this.TXT2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXT2.Location = new System.Drawing.Point(175, 44);
            this.TXT2.Name = "TXT2";
            this.TXT2.Size = new System.Drawing.Size(331, 26);
            this.TXT2.TabIndex = 1;
            // 
            // TXT3
            // 
            this.TXT3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXT3.Location = new System.Drawing.Point(175, 76);
            this.TXT3.Name = "TXT3";
            this.TXT3.Size = new System.Drawing.Size(331, 26);
            this.TXT3.TabIndex = 3;
            // 
            // TXT4
            // 
            this.TXT4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXT4.Location = new System.Drawing.Point(175, 108);
            this.TXT4.Name = "TXT4";
            this.TXT4.Size = new System.Drawing.Size(331, 26);
            this.TXT4.TabIndex = 0;
            // 
            // TXT5
            // 
            this.TXT5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXT5.Location = new System.Drawing.Point(175, 140);
            this.TXT5.Name = "TXT5";
            this.TXT5.Size = new System.Drawing.Size(331, 26);
            this.TXT5.TabIndex = 4;
            // 
            // TXT6
            // 
            this.TXT6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXT6.Location = new System.Drawing.Point(175, 172);
            this.TXT6.Name = "TXT6";
            this.TXT6.Size = new System.Drawing.Size(331, 26);
            this.TXT6.TabIndex = 5;
            // 
            // TXT7
            // 
            this.TXT7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXT7.Location = new System.Drawing.Point(175, 204);
            this.TXT7.Name = "TXT7";
            this.TXT7.Size = new System.Drawing.Size(331, 26);
            this.TXT7.TabIndex = 6;
            // 
            // TXT8
            // 
            this.TXT8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXT8.Location = new System.Drawing.Point(175, 236);
            this.TXT8.Name = "TXT8";
            this.TXT8.Size = new System.Drawing.Size(331, 26);
            this.TXT8.TabIndex = 7;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(146, 272);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(114, 41);
            this.btnOK.TabIndex = 8;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click_1);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(266, 272);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(114, 41);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // T8
            // 
            this.T8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.T8.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::PRSPLAN.Properties.Settings.Default, "zagTXT8", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.T8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.T8.Location = new System.Drawing.Point(12, 236);
            this.T8.Name = "T8";
            this.T8.Size = new System.Drawing.Size(157, 26);
            this.T8.TabIndex = 0;
            this.T8.Text = global::PRSPLAN.Properties.Settings.Default.zagTXT8;
            // 
            // T7
            // 
            this.T7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.T7.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::PRSPLAN.Properties.Settings.Default, "zagTXT7", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.T7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.T7.Location = new System.Drawing.Point(12, 204);
            this.T7.Name = "T7";
            this.T7.Size = new System.Drawing.Size(157, 26);
            this.T7.TabIndex = 0;
            this.T7.Text = global::PRSPLAN.Properties.Settings.Default.zagTXT7;
            // 
            // T6
            // 
            this.T6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.T6.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::PRSPLAN.Properties.Settings.Default, "zagTXT6", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.T6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.T6.Location = new System.Drawing.Point(12, 172);
            this.T6.Name = "T6";
            this.T6.Size = new System.Drawing.Size(157, 26);
            this.T6.TabIndex = 0;
            this.T6.Text = global::PRSPLAN.Properties.Settings.Default.zagTXT6;
            // 
            // T5
            // 
            this.T5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.T5.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::PRSPLAN.Properties.Settings.Default, "zagTXT5", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.T5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.T5.Location = new System.Drawing.Point(12, 140);
            this.T5.Name = "T5";
            this.T5.Size = new System.Drawing.Size(157, 26);
            this.T5.TabIndex = 0;
            this.T5.Text = global::PRSPLAN.Properties.Settings.Default.zagTXT5;
            // 
            // T4
            // 
            this.T4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.T4.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::PRSPLAN.Properties.Settings.Default, "zagTXT4", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.T4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.T4.Location = new System.Drawing.Point(12, 108);
            this.T4.Name = "T4";
            this.T4.Size = new System.Drawing.Size(157, 26);
            this.T4.TabIndex = 0;
            this.T4.Text = global::PRSPLAN.Properties.Settings.Default.zagTXT4;
            // 
            // T3
            // 
            this.T3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.T3.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::PRSPLAN.Properties.Settings.Default, "zagTXT3", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.T3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.T3.Location = new System.Drawing.Point(12, 76);
            this.T3.Name = "T3";
            this.T3.Size = new System.Drawing.Size(157, 26);
            this.T3.TabIndex = 0;
            this.T3.Text = global::PRSPLAN.Properties.Settings.Default.zagTXT3;
            // 
            // T2
            // 
            this.T2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.T2.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::PRSPLAN.Properties.Settings.Default, "zagTXT2", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.T2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.T2.Location = new System.Drawing.Point(12, 44);
            this.T2.Name = "T2";
            this.T2.Size = new System.Drawing.Size(157, 26);
            this.T2.TabIndex = 0;
            this.T2.Text = global::PRSPLAN.Properties.Settings.Default.zagTXT2;
            // 
            // T1
            // 
            this.T1.BackColor = System.Drawing.SystemColors.Control;
            this.T1.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::PRSPLAN.Properties.Settings.Default, "zagTXT1", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.T1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.T1.Location = new System.Drawing.Point(12, 12);
            this.T1.Name = "T1";
            this.T1.ReadOnly = true;
            this.T1.Size = new System.Drawing.Size(157, 26);
            this.T1.TabIndex = 0;
            this.T1.Text = global::PRSPLAN.Properties.Settings.Default.zagTXT1;
            // 
            // frmZag
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(518, 321);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.TXT8);
            this.Controls.Add(this.TXT7);
            this.Controls.Add(this.TXT6);
            this.Controls.Add(this.TXT5);
            this.Controls.Add(this.TXT4);
            this.Controls.Add(this.TXT3);
            this.Controls.Add(this.TXT2);
            this.Controls.Add(this.TXT1);
            this.Controls.Add(this.T8);
            this.Controls.Add(this.T7);
            this.Controls.Add(this.T6);
            this.Controls.Add(this.T5);
            this.Controls.Add(this.T4);
            this.Controls.Add(this.T3);
            this.Controls.Add(this.T2);
            this.Controls.Add(this.T1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmZag";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Zaglavlje";
            this.Load += new System.EventHandler(this.frmZag_Load);
            this.Shown += new System.EventHandler(this.frmZag_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox T1;
        private System.Windows.Forms.TextBox TXT1;
        private System.Windows.Forms.TextBox T2;
        private System.Windows.Forms.TextBox TXT2;
        private System.Windows.Forms.TextBox T3;
        private System.Windows.Forms.TextBox TXT3;
        private System.Windows.Forms.TextBox T4;
        private System.Windows.Forms.TextBox TXT4;
        private System.Windows.Forms.TextBox T5;
        private System.Windows.Forms.TextBox TXT5;
        private System.Windows.Forms.TextBox T6;
        private System.Windows.Forms.TextBox TXT6;
        private System.Windows.Forms.TextBox T7;
        private System.Windows.Forms.TextBox TXT7;
        private System.Windows.Forms.TextBox T8;
        private System.Windows.Forms.TextBox TXT8;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
    }
}